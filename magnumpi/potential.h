/** \file
 *
 * Copyright (C) 2016-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_POTENTIAL_H
#define H_MAGNUMPI_POTENTIAL_H

#include "magnumpi/json.h"
#include "magnumpi/data_set.h"
#include <memory>

namespace magnumpi {

/** A base class for potential objects.
 *
 *  A potential object describes the potential (energy) of a system of
 *  two interacting particles. It provides interfaces for retrieving the
 *  value of the potential as a function of the separation of the particles,
 *  as well as the first and second derivatives of this function. These
 *  can be retrieved by invoking the members value(r), dVdr(r) and dV2dr2(r),
 *  respectively.
 *
 *  A potential object can be created by calling static member create,
 *  passing a config_data object that describes the requested potential
 *  type and configuration parameters.
 *
 *  The calculation of the potential value and derivatives is delegated
 *  to pure virtual members get_value, get_dVdr and get_d2Vdr2, which need
 *  to be provided by derived potential classes.
 *
 *  \author Jesper Janssen and Jan van Dijk
 *  \date   September 2016
 */
class potential
{
  public:
	/** Creates and returns a derived potential object, based on the
	 *  configuration data in \a cnf.
	 *
	 *  If \a cnf contains a "query" section, the information contained
	 *  therein is used to fetch a JSON section that describe a potential
	 *  from an external source. In this case, create is called recursively,
	 *  passing this new section as argument, and the potential that is
	 *  created by that call is returned.
	 *
	 *  In the normal case (no "query"), the type of the potential object
	 *  is determined by the argument of element 'type' in \a cnf, and
	 *  depending on its value additional configuration data are extracted
	 *  from \a cnf.
	 *
	 *  The caller is responsible for deleting the potential object pointer
	 *  that is returned by this function.
	 */
	static std::unique_ptr<const potential> create(const json_type& cnf);
	/** virtual destructor. This is needed for 'delete p' to destroy
	 *  a potential object of the actual derived type when p is a pointer
	 *  to the potential base class.
	 */
	virtual ~potential();
	/** Returns the value of the potential as a function of the particle
	 *  separation \a r.
	 */
	double value(double r) const { return get_value(r); }
	/** Returns the first derivative of the potential as a function
	 *  of the particle separation \a r.
	 */
	double dVdr(double r) const { return get_dVdr(r); }
	/** Returns the second derivative of the potential as a function
	 *  of the particle separation \a r.
	 */
	double d2Vdr2(double r) const { return get_d2Vdr2(r); }
	/** Populates a data_set with samples of the potential values.
	 *  The potential is sampled in the range [\a rmin, \a rmax]
	 *  at intervals \a rdel.
	 */
	data_set write_xny(double rmin, double rmax, std::size_t npoints) const;
	/** Populates a json object with samples of the potential values.
	 *  The potential is sampled in the range [\a rmin, \a rmax]
	 *  at intervals \a rdel.
	 */
	json_type write_json(double rmin, double rmax, std::size_t npoints) const;
  protected:
	/** Must be overridden by concrete derived potential classes to return
	 *  the value of the potential as a function of the particle
	 *  separation \a r.
	 */
	virtual double get_value(double r) const=0;
	/** Must be overridden by concrete derived potential classes to return
	 *  the first derivative of the potential as a function of the particle
	 *  separation \a r.
	 */
	virtual double get_dVdr(double r) const=0;
	/** Must be overridden by concrete derived potential classes to return
	 *  the second derivative of the potential as a function of the particle
	 *  separation \a r.
	 */
	virtual double get_d2Vdr2(double r) const=0;
};

/** Starting with the provided initial value, keep reducing r by a factor 0.9
 *  until the absolute value of the potential \a p at r is above the threshold
 *  \a Vmin. The loop also terminates if no such r was found within 760
 *  iterations.
 *
 *  \todo Should we not consider the absolute value of V(r)? At present this
 *        will not work for purely attractive potentials (e.g. Coulomb for
 *        particles with opposite charges).
 *  \todo explain and document the hardcoded number 760 in this code.
 *
 *  \author Jesper Janssen
 *  \date   September 2016
 */
double rmin_input_evaluation(double rmin, const magnumpi::potential* p, double vlimrmin);

/** Starting with the provided initial value, keep increasing r with a factor 1.1
 *  until the absolute value of the potential \a p at r is below the threshold
 *  \a Vmax. The loop also terminates if no such r was found within 760
 *  iterations. That usually happens only for ill-defined potential functions
 *  that do not tend to 0 for large r-values.
 *
 *  \todo At present, the value is clipped to 1e-8 when that values is exceeded.
 *        Should we not throw a std::runtime_error instead in that case?
 *  \todo explain and document the hardcoded numbers 750 and 100e-10 in this
 *        function. Also use something. more realistic than the Planck length
 *        in the choice for the former.
 *
 *  \author Jesper Janssen
 *  \date   September 2016
 */
double rmax_input_evaluation(double rmax, const magnumpi::potential* p, double vlimrmax);

} // namespace magnumpi

#endif // H_MAGNUMPI_POTENTIAL_H
