/** \file
 *
 * Copyright (C) 2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_GAUSS_KRONROD_H
#define H_MAGNUMPI_GAUSS_KRONROD_H

#include <cmath>
#include <cstdlib>
#include <array>

namespace magnumpi {

/** \todo Document me, add references, explain interface and purpose.
 *
 *  \author Chris Schoutrop
 *  \date   April 2021
 */
template<typename IntegrationMethod>
class Integrator
{
public:
	Integrator()
	 : m_x_min(0), m_x_max(0), m_tolerance(1e-12)
	{
	}
	Integrator(double x_min, double x_max, double tolerance)
	 : m_x_min(x_min), m_x_max(x_max), m_tolerance(tolerance)
	{
		m_length = m_x_max - m_x_min;
	}
	void set_range(double x_min, double x_max)
	{
		m_x_min = x_min;
		m_x_max = x_max;
		m_length = m_x_max - m_x_min;
	}
	void set_tolerance(double tolerance)
	{
		m_tolerance = tolerance;
	}
	template<typename L>
	double integrate(L fn) const
	{
		if (m_x_min==m_x_max)
		{
			return 0.0;
		}
		else
		{
			return integrate_recursively_embedded(fn, m_x_min, m_x_max);
		}
	}
private:
	/** These methods have a built-in error estimate.
	 *  The most accurate estimate should be given by S2, S1 the least
	 *  accurate estimate. The error estimate for the integral
	 *  approximated by S2 on [a,b] is then |S1-S2|.
	 *
	 *  An example of this method is the 15-Point Gauss-Kronrod method,
	 *  which is a 15-Point Kronrod method with embedded 7-Point Gaussian
	 *  quadrature.
	 */
	template<typename L>
	double integrate_recursively_embedded(L fn, double a, double b) const
	{
		double S1, S2;

		// Integrate from a to b
		IntegrationMethod::integrate(fn, a, b, S1, S2);

		// Estimate the error in the integral on [a,b]
		const double error = estimate_error(S1, S2, a, b);
		if (error > m_tolerance)
		{
			// Subdivide the interval if the error is too big.
			const double mid = (a + b) / 2.0;
			const double lower_subdivision = integrate_recursively_embedded(fn, a, mid);
			const double upper_subdivision = integrate_recursively_embedded(fn, mid, b);
			return lower_subdivision + upper_subdivision;
		}
		else
		{
			// If the error is small enough, return the most accurate estimate.
			return S2;
		}
	};

	double estimate_error(double S1, double S2, double a, double b) const
	{
		double error = std::abs(S2 - S1);
		if (S2!=0.0)
		{
			error=error/S2;
		}
		else if (S1!=0.0)
		{
			error=error/S1;
		}
		else
		{
			// It is implied that both S2 and S1 are 0.0 here
			error=0.0;
		}
		// Error estimate in this specific interval
		return error;
	};

	double m_x_min;
	double m_x_max;
	double m_length;
	double m_tolerance;
	/// \todo use_local_tolerance is not used anywhere. Remove?
	// const double use_local_tolerance = true;
};

/** \todo Document me, add references, explain interface and purpose.
 *
 *  \author Chris Schoutrop
 *  \date   April 2021
 */
class GK15
{
public:
	using size_type = std::size_t;
	template<typename L>
	static void integrate(L fn, double a, double b, double& S1, double& S2)
	{
		double rm = (b - a) / 2.0;
		double rp = (b + a) / 2.0;
		const size_type N_points = GK15::nodes.size();
		std::array<double, 15> fn_vals;

		S2 = 0.0;
		// Kronrod quadrature
		for (size_type i = 0; i < N_points; ++i)
		{
			fn_vals[i] = fn(rm * GK15::nodes[i] + rp);
			S2 += fn_vals[i] * GK15::weights_Kronrod[i];
		}
		S2 = S2 * rm;

		// Embedded Gaussian quadrature
		size_type i_G = 0;
		S1 = 0.0;
		for (size_type i = 1; i < N_points; i += 2)
		{
			S1 += fn_vals[i] * GK15::weights_Gauss[i_G];
			++i_G;
		}
		S1 = S1 * rm;

	}
  private:
	/// \todo The following members are not used anywhere. Remove?
	// static const bool is_embedded = true;
	// static const bool is_recycling = false;
	// static const size_type scheme_order = 7 * 3 + 1;
	static const std::array<double, 15> nodes;
	static const std::array<double, 15> weights_Kronrod;
	static const std::array<double, 7> weights_Gauss;
};

} // namespace magnumpi

#endif // H_MAGNUMPI_GAUSS_KRONROD_H
