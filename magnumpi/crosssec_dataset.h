/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_CROSSSEC_DATASET_H
#define H_MAGNUMPI_CROSSSEC_DATASET_H

#include "magnumpi/crosssec.h"
#include "magnumpi/data_set.h"
#include "magnumpi/interpolation.h"
#include <cmath>

namespace magnumpi {

/** A table-based cross section that does double-logarithmic interpolation.
 *
 *  The cross section values are stored in a table that contains the 10-log
 *  SI-values as a function of the 10-log SI-value of the energy. The object can
 *  be constructed from a data_set or JSON object. The input tables are assumed
 *  to contain the values of Q^(l)(eps) for l=1,...lmax.
 *
 *  \author Jan van Dijk
 *  \date   February 2019
 *
 *  \todo This functionality has been extracted from calc_omega.cpp. The
 *        reason for doing double-logarihmic interpolation should be explained
 *        and perhaps this should be made optional (that is: also support
 *        linear interpolation of energy or cross section values (or both).
 *
 */
class cross_section_from_dataset_log : public cross_section
{
  public:
	/** Construct a cross section object from \a Q by reading the energy
	 *  values and the cross section data and initializing member m_l_max.
	 *  The data are of type data_set. That set must contain a column
	 *  representing the energy values and a collection of l_max y-columns,
	 *  each representing the cross section values Q^(l)(eps) for one
	 *  l value. These l-values must be 1,...,l_max (in that order).
	 *  The class converts the energy and cross section values to their
	 *  values in SI units J and m^2, then stores the 10-logs of these
	 *  values in private data members for later usage.
	 */
	cross_section_from_dataset_log(const data_set& Q);
	/** Construct a cross section object from \a Q by reading the energy
	 *  values and the cross section data and initializing member m_l_max.
	 *
	 *  \todo Document the structure of JSON cross section documents once
	 *  a final decision has been made, provide JSON Schema to define that
	 *  choice.
	 *
	 *  The class converts the energy and cross section values to their
	 *  values in SI units J and m^2, then stores the 10-logs of these
	 *  values in private data members for later usage.
	 */
	cross_section_from_dataset_log(const json_type& Q);
  protected:
	/** Calculate the log10 of the energy value \a eps (which must be in J)
	 *  and use that to construct a cross section value for the indicated
	 *  \a l by interpolation of table values.
	 *
	 *  \todo Dpcument the interpolation strategy, document what happens if
	 *  \a eps is outside the table range (extrapolation or clipping).
	 *  \todo See if the interpolation can be done more precisely and see if
	 *  the action when energies are out-of-range should be configurable.
	 */
	virtual double get_value(unsigned l, double eps) const;
	/** Return true if l is positive and not larger than lmax,
	 *  the highest l-value for which cross section data were provided
	 *  when the object was constructed.
	 */
	virtual bool get_supports_l(unsigned l) const;
  private:
	/** dvector is the type of the double-vector that is used to store
	 *  energy and cross section values Q^(l)(eps) (for a given eps).
	 */
	using dvector = std::vector<double>;
	dvector m_log_energy;
	/** Element i of m_log_Q is a vector that has the cross section
	 *  values for log10(energy)==m_log_energy[i]. Those values are
	 *  stored in a vector of size m_l_max that has the values for
	 *  l=1,...,lmax (in that order).
	 */
	std::vector<dvector> m_log_Q;
	/** The highest l-value for which data were provided when the object
	 *  was constructed.
	 */
	const unsigned m_l_max;
};


} // namespace magnumpi

#endif // H_MAGNUMPI_CROSSSEC_DATASET_H
