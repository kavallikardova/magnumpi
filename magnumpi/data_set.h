/** \file
 *
 * Copyright (C) 2017-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_DATA_SET_H
#define H_MAGNUMPI_DATA_SET_H

#include "magnumpi/units.h"
#include <string>
#include <utility>
#include <vector>
#include <iostream>

namespace magnumpi {

/** A data_info object describes one of the elements of a PLASIMO dataset
 *  (an axis or the data itself). An object can be created by passing strings
 *  that represent the name and unit of the data to its constructor.
 *  The unit is that of the numbers that are stored internally by instances of
 *  derived classes. The string "1" can be passed as unit if the data are
 *  scalar.
 *
 *  The object also manages a 'display unit'; after construction this is equal
 *  to the unit itself. The display unit can be changed after construction by
 *  calling member set_display_unit. The display unit must be compatible with
 *  the unit of the object. Whenever a display unit is installed, the scalar
 *  conversion factor unit/display_unit is calculated and stored. The display
 *  unit and conversion factor allow derived classes to stored data internally
 *  in SI units, and facilitate the task of presenting the data to users in
 *  more usual units, depending on the context.
 *
 *  As an example, let us assume that the object describes energy values.
 *  Internally we want to work with the SI unit J exclusively, but a table that
 *  is written to a file may be more easy to understand if the energies are
 *  expressed in electron-volt (eV). This can be achieved by creating the object
 *  with unit "J" and calling set_display_unit("eV") on it afterwards. The
 *  conversion factor (here J/eV) can be retrieved with member conversion_factor().
 *  Summarizing, in pseudo-code, the user can present a stored value 'x' as "x unit()"
 *  or as  "x*conversion_factor() display_unit()".
 *
 *  The class also manages the precision with which numeric data must be written
 *  to output files (the number of digits that are used in output operations).
 *  The default value after construction is 0, which must be understood as:
 *  `use the default precision'. The precision can be modified and queried with
 *  members 'set_precision' and 'precision'.
 *
 *  Finally, member full_name write returns a string of the form "name [unit]",
 *  or just "name" if the object's unit is equal to "1". Member display_unit
 *  does the same, but uses the display unit instead of the unit itself.
 *
 *  \author Jan van Dijk
 *  \date   December 2017
 */
class data_info
{
  public:
	data_info(std::string name, units::dimension d, std::string unit);
	virtual ~data_info();
	const std::string& name() const { return m_name; }
	/// \todo Only support SI internally?
	const std::string& unit() const { return m_unit; }
	const std::string& display_unit() const { return m_display_unit; }
	double conversion_factor() const { return m_conversion_factor; }
	void set_name(const std::string& name) { m_name = name; }
	void set_display_unit(std::string dunit);
	void set_precision(unsigned precision) { m_precision=precision; }
	unsigned precision() const { return m_precision; }
	std::string full_name() const;
	std::string display_full_name() const;
  private:
	std::string m_name;
	units::dimension m_dimension;
	const std::string m_unit;
	std::string m_display_unit;
	double m_conversion_factor;
	unsigned m_precision;
};

/** Class data_set supports data of the form y_j(x) and z(x,y),
 *
 *  \todo elaborate
 *
 *  \author Jan van Dijk
 *  \date   December 2017
 */
class data_set : public data_info
{
  public:
	using value_type = double;
	struct axis_type : public data_info
	{
		// label, numeric value to label the x and y axis
		using item_type = std::pair<std::string,double>;
		using data_type = std::vector<item_type>;
		using size_type = data_type::size_type;
		enum label_type { labeled, numeric };
		/// \todo Only support SI internally (that is: omit unit, assume base SI unit)?
		axis_type(const std::string& name,units::dimension d, const std::string& unit);
		axis_type(const std::string& name);
		size_type size() const { return m_data.size(); }
		value_type value(size_type ndx) const { return get_value(ndx); }
		value_type display_value(size_type ndx) const { return get_value(ndx)*conversion_factor(); }
		axis_type& operator<<(const std::string& value);
		axis_type& operator<<(double value);
		std::string label_str(size_type ndx) const;
		bool is_numeric() const { return m_type==numeric; }
		json_type write_json() const;
	  private:
		value_type get_value(size_type ndx) const { return m_type==numeric ? m_data[ndx].second : value_type(ndx); }
		label_type m_type;
		data_type m_data;
	};
	using array_type = std::vector<double>;
	using size_type = array_type::size_type;
	data_set(
		const std::string& dname, units::dimension dd, const std::string& dunit,
		const std::string& rname, units::dimension rd, const std::string& runit,
		const std::string& cname, units::dimension cd, const std::string& cunit);
	data_set(
		const std::string& dname, units::dimension dd, const std::string& dunit,
		const axis_type& row_axis,
		const axis_type& col_axis);
	const axis_type& row_axis() const { return m_row_axis; }
	const axis_type& col_axis() const { return m_col_axis; }
	axis_type& row_axis() { return m_row_axis; }
	axis_type& col_axis() { return m_col_axis; }
	void add_row(const std::string& value, const array_type& values=array_type{});
	void add_row(double value, const array_type& values=array_type{});
	void add_column(const std::string& value, const array_type& values=array_type{});
	void add_column(double value, const array_type& values=array_type{});
	double operator()(size_type r, size_type c) const { return m_data[c][r]; }
	double& operator()(size_type r, size_type c) { return m_data[c][r]; }
	double cell(size_type r, size_type c) { return m_data[c][r]; }

	void write_legend(std::ostream& os) const;
	void write_row(std::ostream& os, size_type r) const;
	void write_2d(std::ostream& os, const std::string& header=std::string()) const;
	void write_3d(std::ostream& os, const std::string& header=std::string()) const;
	void write(std::ostream& os, const std::string& header=std::string()) const;
	void write(const std::string& fname, const std::string& header=std::string()) const;
	json_type write_json(const std::string& header=std::string()) const;
	/** If a non-zero precision() is active, this class emits a std::setprecision
	 *  command to the output stream \a os. It then calls the pure virtual
	 *  member do_write_display_value to write the number in row \a r and
	 *  column \a c itself.
	 */
	void write_display_value(std::ostream& os, size_type r, size_type c) const;
	void SetWriteGnu3d(bool b) { m_write_gnu3d = b; }
	bool GetWriteGnu3d() const { return m_write_gnu3d; }
  private:
	axis_type m_row_axis;
	axis_type m_col_axis;
	std::vector<array_type> m_data;
	bool m_write_gnu3d;
};

} // namespace magnumpi

#endif // H_MAGNUMPI_DATA_SET_H

