/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_SCAT_COLONNA_H
#define H_MAGNUMPI_SCAT_COLONNA_H

#include "magnumpi/potential.h"
#include "magnumpi/data_set.h"
#include "magnumpi/json.h"

namespace magnumpi {

/** Calculate the deflection angle Theta(b,eps) for an interaction potential
 *  \a V with a specific \a tolerance. The number \a n is ...
 *  The expression for Theta(b,eps) is given in equation 1 of \cite Colonna2008.
 *
 *  \todo Explain \a n and suggest a proper choice.
 *
 *  \todo See if we can avoid the pi-pi problem by returning the scattering angle
 *   chi(b,eps) instead (also defined in equation 1 of \cite Colonna2008).
 *
 *  \author Jesper Janssen, Jan van Dijk and CHris Schoutrop
 *  \date   May 2015
 */
double scat_Colonna_original(const potential *V, double b, double eps, double tolerance, int n);

/** An alternative for scat_Colonna_original that uses RK45 for the integration.
 *  This feature is EXPERIMENTAL and its usage is NOT recommended at this moment.
 *
 *  Integrate to find the scattering angle using the Dormand-Prince 5(4) scheme
 *  embedded Runga-Kutta method. This is Theta in equation (1) of Colonna.
 *
 *  The Runge-Kutta scheme is adapted from Numerical Recipes -
 *  The Art of Scientific Computing 3rd edition. Section 17.2
 *  Adaptive Stepsize Control for Runge-Kutta.
 *
 *  \todo integrand(r+dr) is the same as integrand(r) in the next iteration
 *  if the step is accepted. This could save one evaluation of the integrand.
 *  \todo find a cleaner way to check if any of the sqrt_terms is negative
 *  \todo add a regtest for this integrator
 *  \todo this integrator can be made more generic
 *  \todo a test with fixed stepsize to check if the order of convergence is ok
 *
 *  \author Chris Schoutrop, Jesper Janssen and Jan van Dijk
 *  \date   November 2021
 *
 */
double scat_Colonna_rk45(const potential *V, double b, double eps, double tolerance);

/** Switch function to change between original and Runge-Kutta integration
 *  methods for the scattering angle.
 */
inline double scat_Colonna(const potential *V, double b, double eps, double tolerance, int n)
{
//#define MAGNUMPI_USE_SCAT_COLONNA_RK45
#ifdef MAGNUMPI_USE_SCAT_COLONNA_RK45
	return scat_Colonna_rk45(V, b, eps, tolerance);
#else
	return scat_Colonna_original(V, b, eps, tolerance, n);
#endif
}

/** \todo this function is UNTESTED, not used anywhere at present
 */
data_set get_scat_vs_b_vs_E(
	const std::vector<double>& b_vals,
	const std::vector<double>& energy_vals,
	const potential* pot,
	int interpol,
	double tol);

data_set get_scat_vs_b_vs_E(
	double b_low,
	double b_high,
	int num_b,
	double logE_low,
	double logE_high,
	int logE_pdecade,
	int interpol,
	double tol,
	const potential* pot);

data_set get_scat_vs_b_vs_E(const json_type& cnf, const json_type& pot);

/// Constructs the potential from cnf/interaction/potential
data_set get_scat_vs_b_vs_E(const json_type& cnf);

json_type get_scattering_angles(const json_type& cnf);

} // namespace magnumpi

#endif // H_MAGNUMPI_SCAT_COLONNA_H
