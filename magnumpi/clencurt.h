/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef clencurt_H
#define clencurt_H

#include <cmath>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iostream>

#include "magnumpi/clencurt_coefs.h"

namespace plasimo {
namespace clencurt {

/** Clenshaw-Curtis integration.
 *
 *  \author Jesper Janssen and Jan van Dijk
 *  \date   April 2015
 */
class integrator
{
  public:
	using index_type = unsigned;
	using scalar_type = double;
	using coef_vector_type = coef_vector;
	//make clenshaw curtis quadratures
	integrator(scalar_type a, scalar_type b, unsigned lower, unsigned upper);
	//integration algorithm ensuring tolerance
	template <typename UF>
	typename UF::result_type integrate(
			const UF& f_in,
			scalar_type tolerance) const;
	//integration algorithm ensuring tolerance for multi-dimensional functions
	template <typename UF>
	void integrate_md(const UF& f_in,
			unsigned int dim,
			scalar_type tolerance,
			std::vector<typename UF::result_type>& f_out) const;
	scalar_type a() const { return m_a; }
	scalar_type b() const { return m_b; }
	unsigned lower() const { return m_lower; }
	unsigned upper() const { return m_upper; }
	//provide access to clenshaw curtis coefficients
	scalar_type cc_position(int order, unsigned i) const { return m_coeff[order-m_lower][i].pos; }
  private:
	index_type pow2(index_type p) const { return index_type{1} << p; }
	//integration of a specific clenshaw curtis index
	template <typename UF>
	typename UF::result_type integrate(
			const UF& f_in,
			unsigned index_cc,
			typename UF::result_type* h) const;
	//integration of a specific clenshaw curtis index for multi-dimensional functions
	template <typename UF>
	void integrate_md(const UF& f_in,
			unsigned dim,
			unsigned index_cc,
			std::vector<typename UF::result_type>& f_out,
			std::vector<std::vector<typename UF::result_type>>& h) const;
	/** Create a vector of object that will contain the positions
	 *  and weights for selected orders.
	 */
	std::vector<coef_vector_type> m_coeff;
	const scalar_type m_a;
	const scalar_type m_b;
	const unsigned m_lower;
	const unsigned m_upper;
};

template<typename UF>
typename UF::result_type integrator::integrate(const UF& f_in, scalar_type tolerance) const
{
	typedef typename UF::result_type result_type;
	unsigned index_cc=m_lower;
	scalar_type res=1.0;
	result_type f_out,f_old;
	//declare vector that is large enough for the integrand of index_cc=m_upper
	const unsigned n=pow2(m_upper)+1;
	std::vector<result_type> h(n);
	//initial estimate of integral
	f_out=integrate(f_in,index_cc,h.data());
	//continue until desired accuracy is reached
	while (res>tolerance)
	{
		//increase order of approximation
		++index_cc;
		//stop if order exceeds m_upper
		if (index_cc>m_upper)
		{
			std::cout << "WARNING:Integration algorithm reached res="
				<< res << " while tolerance="<< tolerance << std::endl;
			std::cout << "Not converged Integral value is "
				<< f_out << std::endl;
			break;
		}
		//store old integral result
		f_old=f_out;
		//calculate new integral result
		f_out=integrate(f_in,index_cc,h.data());
		//calculate new residue
		res=std::abs((f_out-f_old)/f_out);
		//use this to evaluate the convergence of the integral
		//std::cout << index_cc << "\t" << f_out << "\t" << res << std::endl;
	}
	return f_out;
}

template<typename UF>
typename UF::result_type integrator::integrate(
				const UF& f_in,
				unsigned index_cc,
				typename UF::result_type* h) const
{
	typedef typename UF::result_type result_type;
	const unsigned step  = (index_cc==m_lower) ? 1 : 2;
	const unsigned start = (index_cc==m_lower) ? 0 : 1;
	const unsigned mult=pow2(m_upper-index_cc);
	//calculate h integrand values
	const unsigned end = pow2(index_cc)+1;
	const coef_vector_type& cf = m_coeff[index_cc-m_lower];
	result_type f_out=result_type();
	for (unsigned i=start;i<end;i+=step)
	{
		const result_type f_i = f_in(cf[i].pos);
		h[i*mult]=f_i;
		f_out += f_i * cf[i].weight;
	}
	//add contribution from previously calculated h values
	if (start==1)
	{
		for (unsigned i=1;i<end;i+=step)
		{
			f_out += h[i*mult] * cf[i].weight;
		}
	}
	return f_out;
}

template<typename UF>
void integrator::integrate_md(
			const UF& f_in,
			unsigned int dim,
			scalar_type tolerance,
			std::vector<typename UF::result_type>& f_out) const
{
	typedef typename UF::result_type result_type;
	unsigned index_cc=m_lower;
	scalar_type res=1.0;
	std::vector<result_type> f_old(dim);
	//declare vector that is large enough for the integrand of index_cc=m_upper
	const unsigned n=pow2(m_upper)+1;
	std::vector<std::vector<result_type>> h(n,std::vector<result_type>(dim,0.0));

	//initial estimate of integral
	integrate_md(f_in,dim,index_cc,f_out,h);
	//continue until desired accuracy is reached
	while (res>tolerance)
	{
		//increase order of approximation
		++index_cc;
		//stop if order exceeds m_upper
		if (index_cc>m_upper)
		{
			std::cout << "WARNING:Integration algorithm reached res="
				<< res << " while tolerance="<< tolerance << std::endl;
			std::cout << "Not converged Integral value is " ;
			for (unsigned i=0;i<=f_out.size();++i)
			{
				std::cout << f_out[i] << "\t";
			}
			std::cout << std::endl;
			break;
		}
		//store old integral result
		for (unsigned i=0; i<f_out.size();++i)
		{
			f_old[i] = f_out[i];
			f_out[i] = 0.0; 
		}
		
		//calculate new integral result
		integrate_md(f_in,dim,index_cc,f_out,h);

		//calculate new residue
		res=0;
		for (unsigned i=0;i!=dim;++i)
		{
			res=std::max(res,std::abs((f_out[i]-f_old[i])/ f_out[i] ));
		}
		//use this to evaluate the convergence of the integral
		//std::cout << index_cc << "\t" << res << std::endl;
	}
	//std::cout << index_cc << "\t" << res << std::endl;
}

template<typename UF>
void integrator::integrate_md(const UF& f_in,
			unsigned dim,
			unsigned index_cc,
			std::vector<typename UF::result_type>& f_out,
			std::vector<std::vector<typename UF::result_type>>& h) const
{
	//define stepsize in h vector
	const unsigned mult=pow2(m_upper-index_cc);
	const unsigned step  = (index_cc==m_lower) ? 1 : 2;
	const unsigned start = (index_cc==m_lower) ? 0 : 1;
	//calculate h integrand values
	const coef_vector_type& cf = m_coeff[index_cc-m_lower];
	for (unsigned int i=start;i*mult<h.size();i+=step)
	{
		const scalar_type p=cf[i].pos;
		const scalar_type w=cf[i].weight;
		f_in(p,h[i*mult]);
		for (unsigned int j=0;j<h[i*mult].size();++j)
		{
			f_out[j] += w* h[i*mult][j];
		}
	}
	//add contribution from previously calculated h values
	if (start==1)
	{
		for (unsigned int i=start;i*mult<h.size();i+=step)
		{
			for (unsigned int j=0;j<h[i*mult].size();++j)
			{
				f_out[j] += h[i*mult][j] * cf[i].weight;
			}
		}
	}
}

} // namespace clencurt
} // namespace plasimo

#endif

