/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_JSON_H
#define H_MAGNUMPI_JSON_H

#include "nlohmann/json.hpp"
#include <iostream>
#include <string>

namespace magnumpi {

/** Make type json_type available in namespace magnumpi. We use the 'nlohmann'
 *  JSON implementation.
 */
using json_type = nlohmann::json;

/** Helper function for creating a json object from stream \a is.
 *
 *  \author Jan van Dijk
 *  \date   February 2019
 */
json_type read_json_from_stream(std::istream& is);

/** Helper function for creating a json object from file \a fname.
 *  Environment variables (of the form $VAR) are substituted in the string
 *  \a fname. A std::runtime_error is thrown if the file cannot be opened
 *  for reading (it does not exist or the user has insufficient permissions).
 *
 *  \author Jan van Dijk
 *  \date   February 2019
 */
json_type read_json_from_file(const std::string& fname);

/** Helper function for writing json object \a data to file \a fname.
 *  Environment variables (of the form $VAR) are substituted in the string
 *  \a fname. A std::runtime_error is thrown if the file cannot be opened
 *  for writing. Optional argument \a indent controls the indentation that
 *  is used in the output file. The default value -1 means that the data will
 *  be written in a single-line compact format.
 *
 *  \author Jan van Dijk
 *  \date   February 2019
 */
void write_json_to_file(const json_type& data, const std::string& fname, int indent=-1);

}; // namespace magnumpi

#endif // H_MAGNUMPI_JSON_H
