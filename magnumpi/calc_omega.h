/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_CALC_OMEGA_H
#define H_MAGNUMPI_CALC_OMEGA_H

#include "magnumpi/clencurt.h"
#include "magnumpi/data_set.h"
#include "magnumpi/crosssec.h"
#include <string>
#include <vector>

namespace magnumpi {

/** A collision integral calculator.
 *
 *  This class provides interfaces for calculating collision integrals
 *  for one or more temperature values.
 *
 *  \author Jesper Janssen and Jan van Dijk
 *  \date   April 2015
 */
class calc_omega
{
public:
	struct ls_pair
	{
		ls_pair(unsigned ll,unsigned ss):l(ll),s(ss) {}
		unsigned l,s;
	};
	using ls_pairs_type = std::vector<ls_pair>;
	/** An Omega calculator is constructed with a reference to a cross
	 *  section \a Q, the \a reduced_mass of the interaction pair, with
	 *  a list \a ls_pairs of (l,s) pairs for which the calculation the
	 *  calculation is requested. The other parameters are the lower
	 *  and upper limit of the order of the Clenshaw-Curtis integrator
	 *  and the \a tolerance of the calculation.
	 */
	calc_omega(
		const cross_section& Q,
		double reduced_mass,
		const ls_pairs_type& ls_pairs,
		int lower_cc,
		int upper_cc,
		double tol);
	/** Calculate the requested collision integrals for temperature \a T
	 *  and return a vector that contains the results. The elements of the
	 *  vector are in the same order that matches the order that is
	 *  returned by member get_ls_pairs().
	 */
	std::vector<double> integrate(double T) const;
	//Integrate all cross sections to collision integrals
	data_set integrate(
		double Tlow,
		double Tmax,
		double Tdel) const;
	const ls_pairs_type& get_ls_pairs() const { return m_ls_pairs; }
	double tolerance() const { return m_tolerance; }
private:
	using integrator_type = plasimo::clencurt::integrator;
	const cross_section& m_Q;
	double m_reduced_mass;
	const ls_pairs_type m_ls_pairs;
	const integrator_type m_integrator;
	const double m_tolerance;
};

/** Construct and return a vector containing the relevant (l,s) pairs
 *  for a given lmax and smax. These are the pairs with l-values in the
 *  range [1,lmax] and, for every l, s-values in the range [s,smax].
 *
 *  \author Jesper Janssen
 *  \date   April 2015
 */
calc_omega::ls_pairs_type construct_ls_pairs(unsigned lmax,unsigned smax);

/** Calculate and return a collision integral table from the provided parameters.
 */
data_set calculate_ci(
		int lower_cc,
		int upper_cc,
		double reduced_mass,
		double Tlow,
		double Tstep,
		double Tupper,
		int l_max,
		int s_max,
		double tol,
		const cross_section& Q);

/** Calculate and return a collision integral table from the provided parameters.
 *
 *  \todo Consider where to specify reduced_mass, tolerance. Should this task have its own tolerance?
 */
data_set calculate_ci(
		const cross_section& Q,
		const json_type& cnf,
		unsigned l_max,
		double reduced_mass,
		double tolerance);

} // namespace magnumpi

#endif // H_MAGNUMPI_CALC_OMEGA_H
