/** \file
 *
 * Copyright (C) 2018-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_UNITS_H
#define H_MAGNUMPI_UNITS_H

#include "magnumpi/json.h"
#include <string>
#include <typeinfo>
#include <stdexcept>

namespace magnumpi {

/** Each of the convert_to family of functions converts string \a value into a
 *  numerical value of type T and returns the result. The functions throw a
 *  runtime error in case the conversion fails or is incomplete (trailing
 *  characters). Instantiation of the primary template generates a compile-time
 *  assert: the function template must be specialized for each supported type T.
 *
 *  \author Jan van Dijk
 *  \date   November 2018
 */
template <class T>
T convert_to(const std::string& value)
{
	// Make the static_assert depend on T, so this is only triggered when
	// this function gets instantiated.
	static_assert(static_cast<T*>(nullptr),"Conversion to this type is not supported.");
}

template <> long convert_to<long>(const std::string& value);
template <> unsigned long convert_to<unsigned long>(const std::string& value);
template <> int convert_to<int>(const std::string& value);
template <> double convert_to<double>(const std::string& value);
template <> bool convert_to<bool>(const std::string& value);
template <>
inline std::string convert_to<std::string>(const std::string& value)
{
	return value;
}

/** Returns true if \a value can be parsed to an int or double value, false
 *  otherwise. No exception is thrown.
 *
 *  \author Jan van Dijk
 *  \date   January 2019
 */
bool is_number(const std::string& value);

/** Returns true if \a value can be parsed into a boolean. This is the case
 *  if the string is equal to 'true' or 'false'. Note: no other values (like
 *  '0', '1', 'yes' or 'no' are recognized as boolean values.
 *
 *  \author Jan van Dijk
 *  \date   January 2019
 */
bool is_bool(const std::string& value);

namespace units {

/** The supported dimensions.
 *  \todo Define semantics for scalar quantities.
 */
enum dimension { scalar, mass, length, time, temperature, energy, angle, area, m3_s, Jm6 };

/** Returns the numerical value of unit \a unit_symbol which has dimension \a d in SI units.
 *  An error is thrown in case of a syntax error or a unit mismatch.
 *
 *  \author Jan van Dijk
 *  \date   November 2018
 */
double unit_in_si(const std::string& unit_symbol, dimension d);

/** Returns the numerical value of quantity \a q which has dimension \a d in SI units.
 *  Argument \a q must be of the form value*unit, as in "1*eV"
 *  An error is thrown in case of a syntax error or a unit mismatch.
 *
 *  \author Jan van Dijk
 *  \date   November 2018
 */
double quantity_in_si(const std::string& q, dimension d);

/** Argument \a si_value is the numerical value of a quantity with dimension
 *  \a d with respect to SI units. This code expresses that quantity in
 *  terms of units \a unit and returns the result as a string.
 *
 *  \author Jan van Dijk
 *  \date   November 2018
 */
std::string in_units(double si_value, const std::string& unit, dimension d);

/** Returns the value of quantity \a q which has dimension \a d in SI units.
 *  Argument \a q must be a JSON object with elements "value" (double)
 *  and "unit" (string).
 *  An error is thrown in case of a syntax error, missing element  or
 *  a unit mismatch.
 *  As an example, a call with arguments { "value:" 3, "unit:" eV }
 *  and magnumpi::energy will return the value 3 * Constant::eV (approximately
 *  4.81...e-19).
 *
 *  \author Jan van Dijk
 *  \date   February 2019
 */
double in_si(const json_type& q, dimension d);

} // namespace units

} // namespace magnumpi

#endif // H_MAGNUMPI_UNITS_H
