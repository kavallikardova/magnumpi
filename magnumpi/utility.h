/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_UTILITY_H
#define H_MAGNUMPI_UTILITY_H

#include <vector>
#include "magnumpi/json.h"
#include "magnumpi/units.h"

namespace magnumpi {

/** Create and return a vector of \a n equidistant samples in the range
 *  [\a low - \a high].
 *
 *  \author Jan van Dijk
 *  \date   February 2020
 */
std::vector<double> make_samples_lin(double low, double high, unsigned n);

/** Create and return a vector of samples in the range 10^log_low - 10^log_high
 *  that are equally distributed on a logarithmic scale. Argument \a n_decade
 *  inidcates the requested number of samples per decade.
 *
 *  \author Jan van Dijk
 *  \date   February 2020
 */
std::vector<double> make_samples_log(double log_low, double log_high, unsigned n_decade);

/** Create and return a vector of \a n samples in the range [\a first - \a last].
 *  If the number of samples \a n is smaller than 2, a runtime_error is thrown.
 *  When \a log_range is false (the default), the sampling will be equidistant,
 *  the increments are then given by (last-first)/(n-1). When \a log_spacing is
 *  true, the range is produced such that the logarithms of the values are
 *  equidistant. In that case it is required that \a first and \a last are
 *  non-zero and have the same sign.
 *
 *  Examples of valid invocations and their results are:
 *  \verbatim
      make_range(-1,2,4,false) -> { -1, 0, 1, 2 }
      make_range(2,-1,4,false) -> { 2, 1, 0, -1 }
      make_range(1e-1,1e2,4,true) -> { 1e-1, 1e0, 1e1, 1e2 }
      make_range(1e2,1e-1,4,true) -> { 1e2, 1e1, 1e0, 1e-1 }
      make_range(-1e-1,-1e2,4,true) -> { -1e-1, -1e0, -1e1, -1e2 } \endverbatim
 *
 *  \todo Check and document error bounds, in particular for the log case
 *
 *  \author Jan van Dijk
 *  \date   May 2021
 */
std::vector<double> make_range(
	double first,
	double last,
	std::vector<double>::size_type n,
	bool log_spacing=false);

/** This overload of make_range extracts the range description from
 *  json object \a cnf. That must contain elements "first", "last",
 *  "size" and "spacing". The first two must be quantities with dimension
 *  \a d, "size" is an integer that indicates the number of samples.
 *  Element "spacing" can be 'linear' or 'logarithmic'. See the other
 *  make_range overload for an exaplenation.
 *
 *  As an example, a distane range (with dimension units::length) can
 *  be set up with a JSON object like:
 *  \verbatim
      {
        "first": { "value": 1e-12, "unit": "m" },
        "last":  { "value": 1e-9, "unit": "m" },
        "size":  4,
        "spacing": 'logarithmic'
      }, \endverbatim
 *  which will result in the values { 1e-12, 1e-11, 1e-10, 1e-9 }.
 *
 *  \sa units
 *  \sa units::in_si
 *
 *  \author Jan van Dijk
 *  \date   May 2021
 */
std::vector<double> make_range(const json_type& cnf, units::dimension d);

} // namespace magnumpi

#endif // H_MAGNUMPI_UTILITY_H
