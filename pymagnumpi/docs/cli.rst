Command line interface
======================

.. argparse::
   :module: magnumpi.cli
   :func: make_parser
   :prog: magnumpi
