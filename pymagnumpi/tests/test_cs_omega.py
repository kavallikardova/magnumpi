import json
import pytest
from magnumpi.calc_cs_omega import calc_cs_omega


def test_calc_cross_section():
    request = {
        "cc": {
            "lower": 6,
            "upper": 14
        },
        "ci": {
            "cc": {
                "lower": 6,
                "upper": 14
            },
            "s_max": 5,
            "temperature_range": {
                "max": {
                    "value": 5000,
                    "unit": "K"
                },
                "min": {
                    "value": 1000,
                    "unit": "K"
                },
                "step": {
                    "value": 1000,
                    "unit": "K"
                }
            }
        },
        "energy_range": {
            "max": {
                "value": 1e+5,
                "unit": "eV"
            },
            "min": {
                "value": 1e-4,
                "unit": "eV"
            },
            "points_per_decade": 1
        },
        "interaction": {
            "potential": {
                "epsilon": {
                    "value": 0.1,
                    "unit": "eV"
                },
                "sigma": {
                    "value": 2.5e-10,
                    "unit": "m"
                },
                "type": "LennardJones"
            },
            "reduced_mass": {
                "value": 2.0013,
                "unit": "amu"
            }
        },
        "l_max": 3,
        "r": {
            "Vmax": {
                "value": 20,
                "unit": "eV"
            },
            "Vmin": {
                "value": 1e-3,
                "unit": "eV"
            },
            "del": {
                "value": 1e-14,
                "unit": "m"
            },
            "max": {
                "value": 1e-9,
                "unit": "m"
            },
            "min": {
                "value": 2e-10,
                "unit": "m"
            }
        },
        "tolerance": 1e-3
    }
    response = calc_cs_omega(request)
    cs_row_axis_values = response['cs']['row_axis']['data']['values']
    response['cs']['row_axis']['data']['values'] = []
    expected_cs_row_axis_values = [
                        1e-4,
                        1e-3,
                        1e-2,
                        1e-1,
                        1,
                        1e+1,
                        1e+2,
                        1e+3,
                        1e+4,
                        1e+5
                    ]
    assert cs_row_axis_values == pytest.approx(expected_cs_row_axis_values)
    assert response['cs']['row_axis']['data']['unit'] == 'eV'

    cs_columns = response['cs']['columns']
    response['cs']['columns'] = []
    expected_cs_columns = [[
                4.90620101384102e-18, 3.713520279847789e-18,
                5.654907105485948e-18
            ],
                        [
                            2.271810058988696e-18, 1.7251255060154793e-18,
                            2.620378800486137e-18
                        ],
                        [
                            1.0520676209959495e-18, 8.022682353900286e-19,
                            1.2139101398324567e-18
                        ],
                        [
                            4.713080203875713e-19, 3.692105566002201e-19,
                            5.514546387094996e-19
                        ],
                        [
                            1.6792773065185235e-19, 1.3241400189809442e-19,
                            1.884397314061889e-19
                        ],
                        [
                            1.1663208134300963e-19, 9.217160295530175e-20,
                            1.3007212454200035e-19
                        ],
                        [
                            8.0987919917013e-20, 6.484771633700879e-20,
                            9.128435542224883e-20
                        ],
                        [
                            5.555176675808563e-20, 4.4684572653158556e-20,
                            6.285131002362364e-20
                        ],
                        [
                            3.793042588182527e-20, 3.055495154582053e-20,
                            4.2966342452352924e-20
                        ],
                        [
                            2.5859867006326387e-20, 2.08411898830419e-20,
                            2.9304513606731575e-20
                        ]]
    for i, expected_row in enumerate(expected_cs_columns):
        assert cs_columns[i] == pytest.approx(expected_row)

    omega_columns = response['omega']['columns']
    response['omega']['columns'] = []
    expected_omega_columns = [[
                2.8012279275726573e-16, 7.198488387006527e-16,
                2.5610676928559597e-15, 1.1665783337038917e-14,
                6.483084024146265e-14, 5.653852908373165e-16,
                2.0133755218362375e-15, 9.176947789545456e-15,
                5.102559853165208e-14, 2.929047991813347e-15,
                1.3290002564348213e-14, 7.361946903538073e-14
            ],
                        [
                            2.9304388314215246e-16, 7.529956673047088e-16,
                            2.702576515231718e-15, 1.2503435549299643e-14,
                            7.094925748532118e-14, 5.926471502094654e-16,
                            2.128692216600076e-15, 9.853617250375123e-15,
                            5.593439863614171e-14, 3.054769966189795e-15,
                            1.4087735512722146e-14, 7.976681582362264e-14
                        ],
                        [
                            3.0346279346575073e-16, 7.918220152792767e-16,
                            2.9020306833985834e-15, 1.3713684752470039e-14,
                            7.919740767378131e-14, 6.238421082710994e-16,
                            2.287792351257262e-15, 1.081525276338807e-14,
                            6.247465582394017e-14, 3.2640183738175855e-15,
                            1.5393259906786493e-14, 8.879516795593943e-14
                        ],
                        [
                            3.1526519416811755e-16, 8.379510015050585e-16,
                            3.1236971683922955e-15, 1.4938789698663737e-14,
                            8.687227717646377e-14, 6.605737582505365e-16,
                            2.4636851569171703e-15, 1.1785851359987206e-14,
                            6.855109073970272e-14, 3.5054866501862866e-15,
                            1.6742793545709685e-14, 9.729145859271757e-14
                        ],
                        [
                            3.281020816831835e-16, 8.858142818239536e-16,
                            3.338106047293241e-15, 1.605754046589578e-14,
                            9.361963191430243e-14, 6.985676052503996e-16,
                            2.6335617613312756e-15, 1.2671618001131028e-14,
                            7.389223883769459e-14, 3.741634617699483e-15,
                            1.7981515905386978e-14, 1.0477623090378977e-13
                        ]]
    assert len(omega_columns) == len(expected_omega_columns)
    for i, expected_row in enumerate(expected_omega_columns):
        assert omega_columns[i] == pytest.approx(expected_row)

    expected = {
        'cs': {
            'title':
            'sigma_l(eps)',
            'unit':
            'm^2',
            'column_axis': {
                'title': 'l',
                'labels': ['1', '2', '3']
            },
            'row_axis': {
                'title': 'epsilon',
                'data': {
                    'unit':
                    'eV',
                    'values': []
                }
            },
            'columns': []
        },
        'omega': {
            'title':
            'Omega_ls(T)',
            'unit':
            'm^3/s',
            'column_axis': {
                'title':
                '(l,s)',
                'labels': [
                    '(1,1)', '(1,2)', '(1,3)', '(1,4)', '(1,5)', '(2,2)',
                    '(2,3)', '(2,4)', '(2,5)', '(3,3)', '(3,4)', '(3,5)'
                ]
            },
            'row_axis': {
                'title': 'T',
                'data': {
                    'unit': 'K',
                    'values': [1000.0, 2000.0, 3000.0, 4000.0, 5000.0]
                }
            },
            'columns': []
        }
    }

    assert response == expected
