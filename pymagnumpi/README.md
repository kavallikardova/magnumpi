# MagnumPI Python package

Python package that makes C++ MagnumPI library available in Python.

## Install

```shell
pip install magnumpi
```

## Command line tool

The `magnumpi` command line tool can be used to perform calculations. See below for some examples.

The example input files can be found in [https://gitlab.com/magnumpi/magnumpi](https://gitlab.com/magnumpi/magnumpi). The following commands are run in root of that repo.

The example input files include env vars that are expanded in C++, and those env vars needs to be set

```shell
export MAGNUMPI_INPUTDATA_DIR=$PWD
```

### Calculate cross section

```shell
magnumpi cs input/cs.json
```

It will output a JSON formatted string with the cross section at the requested energy values.

```json
{
  "title": "sigma_l(eps)",
  "unit": "m^2",
  "column_axis": {
    "title": "l",
    "labels": [
      "1",
      "2",
      "3"
    ]
  },
  "row_axis": {
    "title": "epsilon",
    "data": {
      "unit": "eV",
      "values": [
        9.999999999999961e-05,
...
        99999.9999999996
      ]
    }
  },
  "columns": [
    [
      8.092877898598035e-18,
      5.906052452597951e-18,
      9.479830033419527e-18
    ],
...
  ]
}
```

### Calculate scattering angle

```shell
magnumpi scat input/scat_rscp_colonna.json
```

It will output a JSON formatted string with the scattering angle at the requested impact and energy ranges.

```json
{
  "column_axis": {
    "data": {
      "unit": "eV",
      "values": [
        9.999999999999961e-05,
...
        99999.9999999996
      ]
    },
    "title": "E"
  },
  "columns": [
    [
      3.141592653589793,
...
      0.0002028565858154252
    ]
  ],
  "row_axis": {
    "data": {
      "unit": "m",
      "values": [
        0.0,
        1e-11,
...
        3e-10
      ]
    },
    "title": "b"
  },
  "title": "chi(b,E)",
  "unit": "rad"
}
```

### Calculate derivates of potential

```shell
magnumpi pot --rmin 2e-10 --rmax 3e-10 --rdel 1e-11 input/HH.json
#distance [m],V [V],dV/dr [V/m],d2V/dr2 [V/m^2]
2e-10,-1.0541933465153312e-19,4.225791224186558e-09,-104.47263840488723
2.1e-10,-6.843852916251855e-20,3.1738317744082248e-09,-102.61821539091953
2.2e-10,-4.160587069680212e-20,2.2191271972441033e-09,-86.91630626111781
2.2999999999999998e-10,-2.342850334973455e-20,1.450262373088819e-09,-66.65514992303468
2.4e-10,-1.1925072469313695e-20,8.828186881170004e-10,-47.2578142103886
2.5e-10,-5.176180186826646e-21,4.937041134467853e-10,-31.22042921959424
2.6000000000000003e-10,-1.5831173447527182e-21,2.4494767170692424e-10,-19.19034553797456
2.7000000000000005e-10,6.012825711190221e-23,9.760433642444005e-11,-10.835896548407579
2.8000000000000007e-10,5.952064614391767e-22,1.8400813738867518e-11,-5.428755491601337
2.900000000000001e-10,5.69477929651543e-22,-1.8149521987992936e-11,-2.1789629148688308
```

## Programmatic access

```python
from magnumpi import get_scattering_angles
reqeust = ...
response = get_scattering_angles(request)
print(response)
```

## Develop

For development of this package see [DEVELOP.md](DEVELOP.md) document.
