def ds2columns(ds):
    columns = []
    for r in range(ds.row_axis().size()):
        column = []
        for c in range(ds.col_axis().size()):
            v = ds.cell(r, c)
            column.append(v)
        columns.append(column)
    return columns


def axis2dict(axis):
    res = {"title": axis.name()}
    if axis.is_numeric():
        res["data"] = {
            "unit": axis.display_unit(),
            "values": [axis.display_value(i) for i in range(axis.size())],
        }
    else:
        res["labels"] = [axis.label_str(i) for i in range(axis.size())]
    return res


def ds2json(ds):
    columns = ds2columns(ds)
    return {
        "title": ds.name(),
        "unit": ds.unit(),
        "column_axis": axis2dict(ds.col_axis()),
        "row_axis": axis2dict(ds.row_axis()),
        "columns": columns,
    }
