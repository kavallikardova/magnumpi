#include <iostream>
#include <chrono>
#include "magnumpi/json.h"
#include "magnumpi/consts.h"
#include "magnumpi/potential.h"
#include "magnumpi/calc_cs.h"
#include "magnumpi/calc_omega.h"
#include "magnumpi/scat_colonna.h"
#include "magnumpi/data_set.h"
#include "magnumpi/crosssec_dataset.h"

#include "pybind11_json/pybind11_json.hpp"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace py = pybind11;

#ifndef MAGNUMPI_MODULE_NAME
#define MAGNUMPI_MODULE_NAME _libmagnumpi
#endif

PYBIND11_MODULE(MAGNUMPI_MODULE_NAME, m) {
    m.doc() = R"pbdoc(
        Bindings for MagnumPI library
    )pbdoc";

    m.attr("eV") = Constant::eV;
    m.def("in_si", &magnumpi::units::in_si, R"pdoc(
        Convert quantity to SI unit

        Args:
          quantity: Quantity to convert to a value in SI unit.
              Should be a JSON object with value and unit keys.
          dimension (Dimension): The dimension of the quantity.
    )pdoc", py::arg("quantity"), py::arg("dimension"));
    py::enum_<magnumpi::units::dimension>(m, "Dimension", "The supported dimensions")
        .value("SCALAR", magnumpi::units::dimension::scalar)
        .value("MASS", magnumpi::units::dimension::mass)
        .value("LENGTH", magnumpi::units::dimension::length)
        .value("TEMPERATURE", magnumpi::units::dimension::temperature)
        .value("ENERGY", magnumpi::units::dimension::energy)
        .value("ANGLE", magnumpi::units::dimension::angle)
        .value("AREA", magnumpi::units::dimension::area)
        .value("M3_S", magnumpi::units::dimension::m3_s);

    py::class_<magnumpi::potential>(m, "Potential", "A potential object describes the potential (energy) of a system of two interacting particles.")
	.def_static("create", &magnumpi::potential::create)
	.def("value", &magnumpi::potential::value)
	.def("dVdr", &magnumpi::potential::dVdr)
	.def("d2Vdr2", &magnumpi::potential::d2Vdr2);
    m.def("rmin_input_evaluation", &magnumpi::rmin_input_evaluation);
    m.def("rmax_input_evaluation", &magnumpi::rmax_input_evaluation);

    py::class_<magnumpi::data_info>(m, "DataInfo", "Unit aware data container")
        .def("name", &magnumpi::data_set::axis_type::name)
        .def("unit", &magnumpi::data_set::axis_type::unit)
        .def("display_unit", &magnumpi::data_set::axis_type::display_unit)
    ;
    py::class_<magnumpi::data_set::axis_type, magnumpi::data_info>(m, "AxisType", "Description of axis")
        .def("size", &magnumpi::data_set::axis_type::size)
        .def("value", &magnumpi::data_set::axis_type::value, py::arg("index"), "Value in 'unit' at index")
        .def("display_value", &magnumpi::data_set::axis_type::display_value, py::arg("index"), "Value in 'display_unit' at index")
        .def("label_str", &magnumpi::data_set::axis_type::label_str, py::arg("index"), "Label at index")
        .def("is_numeric", &magnumpi::data_set::axis_type::is_numeric)
    ;
    py::class_<magnumpi::data_set, magnumpi::data_info>(m, "DataSet", "Dataset container")
        .def("cell", &magnumpi::data_set::cell, py::arg("row"), py::arg("column"), "Value of cell at row and column index")
        .def("col_axis", (magnumpi::data_set::axis_type&(magnumpi::data_set::*)()) &magnumpi::data_set::col_axis)
        .def("row_axis", (magnumpi::data_set::axis_type&(magnumpi::data_set::*)()) &magnumpi::data_set::row_axis)
    ;

    m.def("get_scat_vs_b_vs_E",
        py::overload_cast<double, double, int, double, double, int, int, double, const magnumpi::potential*>(&magnumpi::get_scat_vs_b_vs_E),
        py::arg("b_low"), py::arg("b_high"), py::arg("num_b"),
        py::arg("logE_low"), py::arg("logE_high"), py::arg("logE_pdecade"),
        py::arg("interpol"), py::arg("tol"), py::arg("pot"),
        "Calculate scattering angle"
    );
    m.def("get_scattering_angles",
        &magnumpi::get_scattering_angles,
        "Calculate scattering angles, using json as input and output"
    );

    py::class_<magnumpi::cross_section>(m, "CrossSection");
    py::class_<magnumpi::cross_section_from_dataset_log, magnumpi::cross_section>(m, "CrossSectionFromDatasetLog", "Convert dataset to cross section")
        .def(py::init<const magnumpi::json_type&>());
    ;

    py::class_<magnumpi::calc_cs>(m, "CrossSectionCalculator", "Calculator for cross section")
	.def_static("create", &magnumpi::calc_cs::create)
        .def(
            py::init<int, int, double, int, double, double, double, int, int, double, const magnumpi::potential*>(),
            "Construct calculator",
            py::arg("lower"), py::arg("upper"),
            py::arg("E_low"), py::arg("l_max"),
            py::arg("rmin"), py::arg("rmax"), py::arg("dr"),
            py::arg("use_scat_Col"), py::arg("interpol"),
            py::arg("tol"), py::arg("pot")
        )
        .def("integrate",
            py::overload_cast<double, double, unsigned>(&magnumpi::calc_cs::integrate, py::const_),
            R"pdoc(Integrate)pdoc",
            py::arg("logE_low"), py::arg("logE_high"), py::arg("logE_pdecade"))
    ;
    m.def("calc_cross_sec",
        py::overload_cast<const magnumpi::json_type&>(&magnumpi::calc_cross_sec),
        py::arg("cnf"),
        "Calculate and return a cross section table");

    py::class_<magnumpi::calc_omega::ls_pair>(m, "ls_pair")
        .def(py::init<unsigned, unsigned>(), py::arg("l"), py::arg("s"));
    m.def("construct_ls_pairs", &magnumpi::construct_ls_pairs, py::arg("lmax"), py::arg("smax"), "Construct ls pairs");

    py::class_<magnumpi::calc_omega>(m, "OmegaCalculator", "Calculator of omega of cross section")
        .def(py::init<const magnumpi::cross_section&, double, const magnumpi::calc_omega::ls_pairs_type, int, int, double>(),
            py::arg("cross_section"),
            py::arg("reduced_mass"),
            py::arg("ls_pairs"),
            py::arg("lower_cc"),
            py::arg("upper_cc"),
            py::arg("tol")
        )
        .def("integrate",
                py::overload_cast<double, double, double>(&magnumpi::calc_omega::integrate, py::const_),
                R"pdoc(Integrate)pdoc", py::arg("Tlow"), py::arg("Tmax"), py::arg("Tdel"))
    ;


#ifdef VERSION_INFO
    m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
    m.attr("__version__") = "dev";
#endif
}
