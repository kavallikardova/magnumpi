# usage: python3 get_scat <input file>

import json
import sys
import magnumpi.backend

if len(sys.argv) != 2:
    print('usage: python3 get_scat <input file>')
    exit(1)

with open(sys.argv[1]) as f:
    request = json.load(f)
result = magnumpi.backend.get_scattering_angles(request)
print(json.dumps(result))
