# usage: python3 get_pot <input file>

import json
import sys
from magnumpi.potential_builder import build_potential

if len(sys.argv) != 5:
    print('usage: python3 get_pot <input file> <rmin> <rmax> <rstep>')
    exit(1)

with open(sys.argv[1]) as f:
    request = json.load(f)

V = build_potential(request['potential'])

rmin = float(sys.argv[2])
rmax = float(sys.argv[3])
rdel = float(sys.argv[4])

print('#distance [m]  V [V] dV/dr [V/m] d2V/dr2 [V/m^2]')
r = rmin
while (r <= rmax):
    print(r, V.value(r), V.dVdr(r), V.d2Vdr2(r))
    r += rdel
