# To build Python with cmake and conda

## Install deps in conda env

(Boost should not be installed in OS. Otherwise import problems)

```shell
. ~/miniconda39/bin/activate
conda install -n base -c conda-forge mamba
mamba create -y -n magnumpi -c conda-forge pybind11 pytest cmake compilers boost-cpp
conda activate magnumpi
```

## Compile

```shell
# In root
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -Dwith-doc=0  ..
make -j 4
```

## Run Python scripts

```shell
. set-magnumpi-local
python3 python/app/get_cs.py ../input/cs.json
```
