#include <string>
#include <iostream>
#include <stdexcept>
#include "magnumpi/scat_colonna.h"

using namespace magnumpi;

void run_case(const json_type& cnf, const json_type& pot)
{
	data_set chi = get_scat_vs_b_vs_E(cnf,pot);
	chi.write("scat_vs_b_vs_E.dat");
	write_json_to_file(chi.write_json(),"scat_vs_b_vs_E.json",2);
}

int main(int argc, char *argv[])
try
{
	if (argc==2)
	{
		const json_type cnf = read_json_from_file(argv[1]);
		run_case(cnf,cnf.at("interaction").at("potential"));
	}
	else if (argc==3)
	{
		const json_type cnf = read_json_from_file(argv[1]);
		const json_type pot = read_json_from_file(argv[2]).at("potential");
		run_case(cnf,pot);
	}
	else
	{
		throw std::runtime_error("Usage: calc_scat_colonna <config file> [potential file]");
	}
	return 0;
}
catch (const std::exception& exc)
{
	std::cerr << "Error calculating chi(b,E): " << exc.what() << std::endl;
	return 1;
}
