add_executable(calc_cs calc_cs.cpp)
target_link_libraries(calc_cs magnumpi)

add_executable(calc_scat_colonna calc_scat_colonna.cpp)
target_link_libraries(calc_scat_colonna magnumpi)

add_executable(config_to_json config_to_json.cpp)
target_link_libraries(config_to_json magnumpi)

add_executable(extract_element extract_element.cpp)
target_link_libraries(extract_element magnumpi)

add_executable(write_potential write_potential.cpp)
target_link_libraries(write_potential magnumpi)
