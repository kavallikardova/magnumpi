#ifndef H_PLUTIL_STRING_UTIL_H
#define H_PLUTIL_STRING_UTIL_H

#include <string>
#include <stdexcept>

namespace plasimo {

/** Replace all occurrences of \a key in \a str with \a repl.
 *  The function correctly handles the case that \a key is a
 *  substring of \a repl. As an example, if in the string
 *  "AA" all "A" are to be replaced with "AB, the result will be
 *  "ABAB": the 'produced' characters 'A' will not be replaced
 *  recursively (which would result in an endless loop).
 *
 *  If the search string \a key is empty, a std::runtime_error is thrown.
 *
 *  \author Jan van Dijk
 *  \date   May 2013
 */
inline std::string& search_and_replace(
				std::string& str,
				const std::string& key,
				const std::string& repl)
{
	const std::string::size_type sz = key.length();
	if (!sz)
	{
		throw std::runtime_error("search and replace: empty search string provided.");
	}
	for (std::string::size_type pos=0;                    // start looking at position 0
	     (pos = str.find(key,pos)) != std::string::npos;  // update pos, stop if there is no more match
	     pos += repl.length() )                           // advance to the end of the replaced substring
	{
		str.replace(pos, sz, repl);
	}
	return str;
}

/** This function is similar to search_and_replace, but accepts a
 *  constant string reference. The function makes a copy of \a str,
 *  calls search_and_replace to do the substitutions in that copy,
 *  and returns the result.
 *
 *  \sa     search_and_replace
 *  \author Jan van Dijk
 *  \date   January 2014
 */
inline std::string search_and_replace_copy(
				const std::string& str,
				const std::string& key,
				const std::string& repl)
{
	std::string copy(str);
	return search_and_replace(copy,key,repl);
}

} // namespace plasimo

#endif // H_PLUTIL_STRING_UTIL_H

