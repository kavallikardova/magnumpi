#ifndef H_PLUTIL_TEST_UTILS_H
#define H_PLUTIL_TEST_UTILS_H

#include <iostream>
#include <exception>

inline unsigned long& test_ntests() { static unsigned long ntests=0; return ntests; }
inline unsigned long& test_nerrors() { static unsigned long nerrors=0; return nerrors; }
inline unsigned long& test_nxfails() { static unsigned long nxfails=0; return nxfails; }


#define begin_test \
try { \
  ++test_ntests();	\
  {

#define end_test(expr,should_throw,xfails) \
  }	\
  if (should_throw)	\
  {	\
	if (xfails) \
	{	\
		++test_nxfails();	\
		std::cout << "<test> XFAIL (known failure): test should generate an exception. Expression was: '" << #expr << "'." << std::endl;	\
	}	\
	else	\
	{	\
		++test_nerrors();	\
		std::cout << "<test> ERROR, test should generate an exception. Expression was: '" << #expr << "'." << std::endl;	\
	}	\
  }	\
  else	\
  {	\
  }	\
} catch (std::exception& exc)	\
{	\
  if (should_throw)	\
  {	\
	if (xfails) \
	{	\
		++test_nxfails();	\
		std::cout << "<test> ERROR (This test passed, but was marked XFAIL): an exception was expected: '" \
			     << exc.what() << "'" << std::endl;	\
	}	\
	else \
	{	\
		std::cout << "<test> OK: an exception was expected: '" \
			     << exc.what() << "'" << std::endl;	\
	}	\
  }	\
  else	\
  {	\
	if (xfails) \
	{	\
		++test_nxfails();	\
		std::cout << "<test> XFAIL (known failure): test generated an unexpected exception. Expression was: '" << #expr \
				<< "', it raised: '" << exc.what() << "'." << std::endl;	\
	}	\
	else \
	{	\
		++test_nerrors();	\
		std::cout << "<test> ERROR: test generated an unexpected exception. Expression was: '" << #expr \
				<< "', it raised: '" << exc.what() << "'." << std::endl;	\
	} \
  }	\
}

#define do_test_expr( expr, xfails) \
  if ( (xfails) ^ ((expr)) )	\
  {	\
	if (xfails) \
	{	\
		std::cout << "<test> XFAIL (known failure): Expression '" << #expr << "'." << std::endl;	\
		++test_nxfails();	\
	}	\
	else \
		std::cout << "<test> OK: Expression '" << #expr << "'." << std::endl;	\
  }	\
  else	\
  {	\
	std::cout << "<test> ERROR, expression failed: '" << #expr << "'" << std::endl;	\
	++test_nerrors();	\
  }

/** Test that \a expr can be executed without throwing an exception.
 *  If it does anyway, an error is flagged.
 */
#define test_code(expr)	\
begin_test	\
expr;	\
std::cout << "<test> OK: Code '" << #expr << "'." << std::endl;	\
end_test(expr,false,false)

/** Execute the code \a expr and verify that it throws an exception.
 *  If it does not, an error is flagged.
 */
#define test_code_throws(expr)	\
begin_test	\
expr;	\
end_test(expr,true,false)

/** Execute the code \a expr and verify that it throws an exception.
 *  If it does not, an xfail is flagged.
 *  This is for code that is known to have a defficiency.
 */
#define test_code_throws_xfail(expr)	\
begin_test	\
expr;	\
end_test(expr,true,true)

#define test_expr_xfail(expr)	\
begin_test	\
do_test_expr(expr,true)	\
end_test(expr,false,false)

#define test_expr_throws(expr)	\
begin_test	\
do_test_expr(expr,false)	\
end_test(expr,true,false)

#define test_expr_throws_xfail(expr)	\
begin_test	\
do_test_expr(expr,false)	\
end_test(expr,true,true)

#define test_expr(expr)	\
begin_test	\
do_test_expr(expr,false)	\
end_test(expr,false,false)

#define test_static_assert(expr)	\
begin_test	\
static_assert(expr,#expr); \
do_test_expr(expr,false)	\
end_test(expr,false,false)

#define test_expr_else(expr,code, penalty) \
  if (! (expr))	\
  {	\
	std::cout << "<test> ERROR, aborting: expression failed: '" << #expr << "'" << std::endl;	\
	test_nerrors()+=penalty;	\
	code;	\
  }	\
  else	\
  {	\
	std::cout << "<test> OK: Expression '" << #expr << "'." << std::endl;	\
  }

#define test_report \
std::cout << "TEST REPORT: " << std::endl;	\
std::cout << " - " << test_ntests()  << " tests were done" << std::endl;	\
std::cout << " - " << test_nxfails() << " known failures occurred." << std::endl;		\
std::cout << " - " << test_nerrors() << " errors occured" << std::endl;


#endif // H_PLUTIL_TEST_UTILS_H

