#ifndef H_PLUTIL_ENVIRONMENT_H
#define H_PLUTIL_ENVIRONMENT_H

#include <cctype>
#include <cstdlib>
#include <stdexcept>
#include "plutil/string_util.h"

namespace plasimo {

/** If the environment variable \a envvar exists, return its
 *  value as a std::string. If the variable is not set, and
 *  \a required is false, return an empty string is. If the
 *  variable is not set and \a required is true, throw a
 *  std::runtime_error.
 *   
 *  \author Jan van Dijk
 *  \date   May 2013
 */
inline std::string get_env_var(const std::string& envvar, bool required)
{
	const char* value = std::getenv(envvar.c_str());
	if (required && !value)
	{
		throw std::runtime_error( "Environment variable '"
			+ envvar + "' required, but not set.");
	}
	return value ? value : "";
}

/** Resolves environment variables in \a name. If \a env_required
 *  is true, a runtime_error is thrown if an environment variables that
 *  is used in \a name is not set. The function returns true if any
 *  substiutions were made, false otehrwise.
 *
 *  An environment variable is announced by the character $, and ends
 *  at the first character that is not an alphanumeric character or
 *  the underscore. A runtime_error is thrown if an empty variable
 *  name is encountered, as in "$:".
 *
 *  Note that this function correctly handles nested environment
 *  assignments. As an example, given the environment variables
	\verbatim
	A=$B
	B=$C
	C=value, \endverbatim
 *  this function changes a string which is equal to "$A" into "value",
 *  not "$B".
 *
 *  If an error occurs, partial substitution may result.
 *
 *  \author Bart Hartgers and Jan van Dijk
 *  \date   May 2013
 */
inline bool substitute_env_vars(std::string& name, bool env_required)
{
	std::string::size_type ndx = name.find('$');
	if (ndx==name.npos)
	{
		// no '$' found.
		return false;
	}
	const std::string::size_type n1=ndx;
	// OK, there is one. See where it ends (non A-z or _)
	while (++ndx != name.size())
	{
		if ( (!std::isalnum(name[ndx])) && !(name[ndx]=='_') ) 
		{
			break;
		}
	}
	// also remove the $
	const std::string::size_type len=ndx-n1-1;
	const std::string envvar(name.substr(n1+1,len));
	if (envvar.empty())
	{
		throw std::runtime_error(
			"While substituting environment variables in '" + name +
			"': '$' must be followed by an identifier.");
	}
	const char* envval(std::getenv(envvar.c_str()));
	if (env_required && !envval)
	{
		throw std::runtime_error(
			"While substituting environment variables in '" + name +
			"': variable '" + envvar + "' required, but not set.");
	}
	search_and_replace(name, '$'+envvar, envval ? envval : "");
	// recurse: needed in case multiple environment variables are used in
	//          the string, or the environment variables are defined in terms
	//          of other environment variables.
	substitute_env_vars(name,env_required);
	return true;
}

/** This function is similar to substitute_env_vars, but modifies and returns
 *  a copy of the string \a name, rather than \a name itself.
 */
inline std::string substitute_env_vars_copy(const std::string& name, bool env_required)
{
	std::string copy(name);
	substitute_env_vars(copy,env_required);
	return copy;
}

} // namespace plasimo

#endif // H_PLUTIL_ENVIRONMENT_H
