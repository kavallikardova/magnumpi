FROM ubuntu:18.04 as builder

RUN \
    apt update && \
    DEBIAN_FRONTEND=noninteractive apt install -y build-essential autoconf libtool libboost-filesystem-dev gnuplot-nox doxygen-latex graphviz texlive-science dvipng

WORKDIR /app

COPY . /app

RUN \
    make -f Makefile.dist && \
    mkdir build && \ 
    cd build && \
    ../configure --with-doc && \
    make && \
    make install && \
    ldconfig

FROM ubuntu:18.04

COPY --from=builder /usr/local/lib/libmagnumpi.so.0 /usr/local/lib/
COPY --from=builder /app/build/app/.libs/* /usr/local/bin/
COPY --from=builder /usr/local/magnumpi/*.h /usr/local/include/magnumpi/

RUN ldconfig

CMD ["calc_cs"]
