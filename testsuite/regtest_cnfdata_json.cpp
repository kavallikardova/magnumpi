#include "magnumpi/json.h"
#include "plutil/test_utils.h"

void input_parser_tests()
{
	using namespace magnumpi;

	test_code_throws(
		const std::string str("#no hash-comments in JSON files.");
		json_type cnf(json_type::parse(str));
	);
	test_code_throws(
		const std::string str("{ \"value_without_key\" }");
		json_type cnf(json_type::parse(str));
	);
	test_code_throws(
		const std::string str("{ \"key_without_value\": }");
		json_type cnf(json_type::parse(str));
	);
	test_code_throws(
		const std::string str("=");
		json_type cnf(json_type::parse(str));
	);
}

void access_tests()
{
	using namespace magnumpi;

	// check that an exception is thrown when the ipnut file does not exist
	test_code_throws( read_json_from_file("non_existing_file.txt") );

	// construct a cnf with test input data.
	json_type cnf;
	
	try {
		cnf = read_json_from_file("$MAGNUMPI_INPUTDATA_DIR/testsuite/regtest_cnfdata_json.in");
	}
	catch(std::exception& exc)
	{
		std::cerr << exc.what() << std::endl;
		test_nerrors() += 100;
		return;
	}

	// key-value tests (also for whitespace handling)
	test_expr( cnf.at("test_key_1")=="test_value_1" );
	// whitespace handling. Leading and trailing whitespace in values is kept as-is
	test_expr( cnf.at("test_key_2")=="\ttest value 2  " );

	// section access
	test_code_throws( cnf.at("non_existing_subsection") );
	// subsection access
	test_code( cnf.at("subsection1").at("subsection1a") );
	test_expr( cnf.at("subsection1").at("subsection1a").at("test_key_1a")=="test_value_1a" );

	// integer parsing tests
	test_expr( json_type::parse("2")==2 );
	// NOTE: get<int> etc. allow double -> int conversions, just like C++ does.
	// so even while:
	test_expr( json_type::parse("3.5").is_number_integer()==false );
	// ... the following will pass:
	test_expr( json_type::parse("3.5").get<int64_t>()==3 );

	// double parsing tests
	test_expr( json_type::parse("3.0")==3.0 );
	test_expr( json_type::parse("3.0e0")==3.0 );
	// bad double, will give a parse error
	test_expr_throws( json_type::parse("3.0.0")==3.0 );

	// comparison
	test_expr( cnf.at("subsection1").at("subsection1a")==json_type::parse("{ \"test_key_1a\": \"test_value_1a\" }") );
}

int main()
{
	input_parser_tests();
	access_tests();

	// report test results and return the numer of errors.
        test_report;
        return test_nerrors();
}
