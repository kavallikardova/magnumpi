#include "magnumpi/data_set.h"
#include "magnumpi/consts.h"
#include "plutil/test_utils.h"
#include <memory>
#include <sstream>

void test_xny()
{
	using namespace magnumpi;

	std::unique_ptr<data_set> ds;
	test_code(ds.reset(new data_set(
		"chi(b,E_0+)",units::angle,"rad",{"b",units::length,"m"}, {"species pair"}));)
	test_expr_else( ds.get(), return;, 100)

	test_expr( ds->row_axis().name()=="b" )
	test_expr( ds->row_axis().unit()=="m" )
	test_expr( ds->row_axis().size()==0 )

	test_expr( ds->col_axis().name()=="species pair" )
	test_expr( ds->col_axis().unit()=="1" )
	test_expr( ds->col_axis().size()==0 )

	test_code( ds->add_column("He_He"); );
	test_code( ds->add_column("He_He+"); );
	test_code( ds->add_column("He+_He+"); );
	test_expr( ds->col_axis().size()==3 )
	test_code_throws( ds->add_column(1.0) );
	test_expr( ds->col_axis().size()==3 )

	test_expr( ds->col_axis().value(0)==0 )
	test_expr( ds->col_axis().value(1)==1 )
	test_expr( ds->col_axis().value(2)==2 )
	test_expr( ds->col_axis().label_str(0)=="He_He" )
	test_expr( ds->col_axis().label_str(1)=="He_He+" )
	test_expr( ds->col_axis().label_str(2)=="He+_He+" )

	// add a row, x=3, default-initialize y[] data
	test_code( ds->add_row(3) )
	test_expr( ds->row_axis().size()==1 )
	test_expr( ds->row_axis().value(0)==3 )
	test_expr( (*ds)(0,0)==0 )
	test_expr( (*ds)(0,1)==0 )
	test_expr( (*ds)(0,2)==0 )

	test_code( (*ds)(0,0)=1 )
	test_code( (*ds)(0,1)=2 )
	test_code( (*ds)(0,2)=3 )

	test_expr( (*ds)(0,0)==1 )
	test_expr( (*ds)(0,1)==2 )
	test_expr( (*ds)(0,2)==3 )

	// add a row, x=4, use provided y[] data
	// wrong number of y[] data (expected 3)
	test_code_throws( ds->add_row(4, {11}) )
	test_expr( ds->row_axis().size()==1 )
	// try again with the correct number of data:
	test_code( ds->add_row(4, {11, 12, 13}) )
	test_expr( ds->row_axis().size()==2 )
	test_expr( ds->row_axis().value(1)==4 )
	test_expr( (*ds)(1,0)==11 )
	test_expr( (*ds)(1,1)==12 )
	test_expr( (*ds)(1,2)==13 )

	// wrong number of data, expected 2
	test_code_throws( ds->add_column("Ar-Ar+", {14}) )
	test_expr( ds->col_axis().size()==3 )
	test_code( ds->add_column("Ar-Ar+", {4,14}) )
	test_expr( ds->col_axis().size()==4 )
	test_expr( (*ds)(0,3)== 4 )
	test_expr( (*ds)(1,3)==14 )

	/// \todo Test more interfaces
}

void test_xyz()
{
	using namespace magnumpi;

	std::unique_ptr<data_set> ds;
	test_code(ds.reset(new data_set(
		"chi (He He+)",units::angle,"rad",{"b",units::length,"m"}, {"E",units::energy,"J"}));)
	test_expr_else( ds.get(), return;, 100)
	test_code( ds->add_column(1*Constant::eV); );
	test_code( ds->add_column(2*Constant::eV); );
	test_code_throws( ds->add_column("label"); );
	test_expr( ds->col_axis().name()=="E" )
	test_expr( ds->col_axis().unit()=="J" )
	test_expr( ds->col_axis().size()==2 )
	test_expr( ds->col_axis().value(0)==1*Constant::eV )
	test_expr( ds->col_axis().value(1)==2*Constant::eV )

	test_expr( ds->row_axis().size()==0 )
	test_code( ds->add_row(3) )
	test_code( ds->add_row(4) )
	test_expr( ds->row_axis().size()==2 )
	test_expr( ds->row_axis().value(0)==3 )
	test_expr( ds->row_axis().value(1)==4 )

	/// \todo Test more interfaces
}

int main()
{
	test_xny();
	test_xyz();

	// report test results and return the numer of errors.
        test_report;
        return test_nerrors();
}
