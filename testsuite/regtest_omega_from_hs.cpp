#include "plutil/test_utils.h"
#include "magnumpi/calc_omega.h"
#include "magnumpi/crosssec_hs.h"
#include <cmath>

void omega_from_hs_tests()
{
	using namespace magnumpi;

	// 1. Set up a hard-sphere cross section
	const double r_hs = 1e-10;
	cross_section_hard_sphere cs(2*r_hs);
	const double mu = 2.0013*Constant::AMU;

	// 2. Set up a collision integral integrator object

	const unsigned l_max = 1;
	const unsigned s_max = 4;
	const unsigned lower_cc=6;
	const unsigned upper_cc=14;
	const double tolerance = 1e-7;

	calc_omega::ls_pairs_type ls(construct_ls_pairs(l_max,s_max));
	calc_omega co(cs, mu, ls, lower_cc, upper_cc, tolerance);
	const calc_omega::ls_pairs_type& ls_pairs = co.get_ls_pairs();

	// integration is done for one fixed T
	const double T=300;
	const std::vector<double> res = co.integrate(T);
	test_expr_else( res.size()==ls_pairs.size(), return, 100)

	const double v_th=std::sqrt(8*Constant::Boltzmann*T/(Constant::pi*mu));
	// test the calculated Omega_ls values against the analytical
	// result for hard-sphere cross sections.
	/// \todo Add a reference for the analytical expression
	for (unsigned p=0; p!=res.size(); ++p)
	{
		const unsigned l = ls_pairs[p].l;
		const unsigned s = ls_pairs[p].s;
		// note: tgamma(s+2) = (s+1)!
		const double Omega_ls_an = 0.25*v_th*cs.Q_hs(l)*std::tgamma(s+2)/2;
		std::cout << "l = " << l << ", s = " << s << std::endl;
		test_expr( std::abs(1-res[p]/Omega_ls_an) < co.tolerance() );
	}
}

int main()
{
	try {
		omega_from_hs_tests();
	}
	catch (const std::exception& exc)
	{
		std::cerr << "Unexpected error: " << exc.what() << std::endl;
		test_nerrors() += 1000;
	}

	// report test results and return the numer of errors.
        test_report;
        return test_nerrors();
}
