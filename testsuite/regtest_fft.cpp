/** \file
 *
 * Copyright (C) 2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * This file test Clenshaw-Curtis integration over the interval (-1,1)
 * for various test functions, and check the results against analytical
 * expressions.
 *
 * \author Jan van Dijk
 * \date   April 2021
 */

#include "magnumpi/fft.h"
#include "magnumpi/consts.h"
#include <complex>
#include <vector>
#include <cmath>
#include <limits>

#include "plutil/test_utils.h"

template <typename T>
void test_delta()
{
	using scalar_type = T;
	using C=std::complex<scalar_type>;
	const scalar_type tol = 20*std::numeric_limits<scalar_type>::epsilon();
	constexpr const C imag = C{0,1};
	std::size_t N=8;
	for (std::size_t k=0; k!=N; ++k)
	{
		std::vector<C> f(N);
		f[k] = 1;
		std::vector<C> F(f);
		Numerics::fft_complex(F);
		for (std::size_t m=0; m!=N; ++m)
		{
			test_expr(std::abs(F[m]-std::exp(imag*double(2*Constant::pi*k*m)/double(N)))<tol);
		}
		std::vector<C> Finv(F);
		Numerics::ifft_complex(Finv);
		for (std::size_t m=0; m!=N; ++m)
		{
			test_expr(std::abs(Finv[m]-(m==k?1.0:0.0))<tol);
		}
	}
}

int main()
{
	test_delta<double>();

	// report test results and return the numer of errors.
        test_report;
        return test_nerrors();
}
