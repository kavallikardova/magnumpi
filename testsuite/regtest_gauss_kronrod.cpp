#include "magnumpi/gauss_kronrod.h"
#include "magnumpi/consts.h"
#include <cmath>
#include <iostream>
#include "plutil/test_utils.h"

template <class F>
bool calculate_integral(const F& f, double xmin, double xmax, double expected, double tolerance=1e-15)
{
	magnumpi::Integrator<magnumpi::GK15> integrator;
	integrator.set_range(xmin,xmax);
	integrator.set_tolerance(tolerance);
	const double value = integrator.integrate(f);
	const double reldiff = std::abs((value-expected)/expected);
	return reldiff<tolerance;
}

int main()
{
	// 2^3-1^3 (=7)
	test_expr( calculate_integral([](double x) -> double { return 3*x*x; }, 1, 2, 7, 1e-15) );
	// sin(pi/2)-sin(0) (=1)
	test_expr( calculate_integral([](double x) -> double { return std::cos(x); }, 0, Constant::pi/2, 1, 1e-15) );

	// report test results and return the numer of errors.
        test_report;
        return test_nerrors();
}
