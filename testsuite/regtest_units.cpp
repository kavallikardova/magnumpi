#include "magnumpi/units.h"
#include "magnumpi/consts.h"
#include "plutil/test_utils.h"
#include <sstream>

void conversion_tests()
{
	using namespace magnumpi;

	test_expr( convert_to<int>("42")==42 );
	test_expr( convert_to<int>("0x2A")==42 );
	test_code_throws( convert_to<int>("42."); );
	test_code_throws( convert_to<int>("42 "); );
	test_code_throws( convert_to<int>("42x"); );
	test_code_throws( convert_to<int>("x42x"); );

	test_expr( convert_to<unsigned long>("42")==42 );
	test_expr( convert_to<unsigned long>("0x2A")==42 );
	test_code_throws( convert_to<unsigned long>("42."); );
	test_code_throws( convert_to<unsigned long>("42 "); );
	test_code_throws( convert_to<unsigned long>("42x"); );
	test_code_throws( convert_to<unsigned long>("x42x"); );

	test_expr( convert_to<long>("42")==42 );
	test_expr( convert_to<long>("0x2A")==42 );
	test_code_throws( convert_to<long>("42."); );
	test_code_throws( convert_to<long>("42 "); );
	test_code_throws( convert_to<long>("42x"); );
	test_code_throws( convert_to<long>("x42x"); );
	// the following test relies on long being larger than int,
	// which is not guaranteed by the standard (and may not be
	// the case in particular on 32-bit systems).
	if (sizeof(long)!=sizeof(int))
	{
		std::stringstream lmax; lmax << std::numeric_limits<long>::max();
		test_code_throws( convert_to<int>(lmax.str()); );
		std::stringstream llow; llow << std::numeric_limits<long>::lowest();
		test_code_throws( convert_to<int>(llow.str()); );
	}

	test_expr( convert_to<double>("42")==42 );
	test_expr( convert_to<double>("0x2A")==42 );
	test_code_throws( convert_to<double>("42.0."); );
	test_code_throws( convert_to<double>("42 "); );
	test_code_throws( convert_to<double>("42x"); );
	test_code_throws( convert_to<double>("x42x"); );

	test_expr( convert_to<bool>("true")==true );
	test_expr( convert_to<bool>("false")==false );
	test_code_throws( convert_to<bool>("0"); );
	test_code_throws( convert_to<bool>("1"); );
	test_code_throws( convert_to<bool>("x"); );
}

void unit_tests()
{
	using namespace magnumpi::units;

	test_expr( unit_in_si("kg",mass)==1.0 );
	test_expr( unit_in_si("amu",mass)==Constant::AMU );
	test_expr( quantity_in_si("1*kg",mass)==1.0 );
	test_expr( quantity_in_si("1*amu",mass)==Constant::AMU );
	test_expr( quantity_in_si("3*amu",mass)==3*Constant::AMU );
	test_expr( in_units(Constant::AMU,"amu",mass)=="1*amu" );

	test_expr( unit_in_si("m",length)==1.0 );
	test_expr( unit_in_si("a0",length)==Constant::BohrRadius );
	test_expr( quantity_in_si("1*m",length)==1.0 );
	test_expr( quantity_in_si("1*a0",length)==Constant::BohrRadius );
	test_expr( quantity_in_si("3*a0",length)==3*Constant::BohrRadius );
	test_expr( in_units(Constant::BohrRadius,"a0",length)=="1*a0" );

	// qualify, since time is also a C function in the global namespace
	test_expr( unit_in_si("s",magnumpi::units::time)==1.0 );

	test_expr( unit_in_si("K",temperature)==1.0 );
	test_expr( unit_in_si("eVT",temperature)==Constant::eVT );
	test_expr( quantity_in_si("1*K",temperature)==1.0 );
	test_expr( quantity_in_si("1*eVT",temperature)==Constant::eVT );
	test_expr( quantity_in_si("3*eVT",temperature)==3*Constant::eVT );
	test_expr( in_units(Constant::eVT,"eVT",temperature)=="1*eVT" );

	test_expr( unit_in_si("J",energy)==1.0 );
	test_expr( unit_in_si("eV",energy)==Constant::eV );
	test_expr( quantity_in_si("1*J",energy)==1.0 );
	test_expr( quantity_in_si("1*eV",energy)==Constant::eV );
	test_expr( quantity_in_si("3*eV",energy)==3*Constant::eV );
	test_expr( in_units(Constant::eV,"eV",energy)=="1*eV" );

	test_expr( unit_in_si("rad",angle)==1.0 );
	test_expr( unit_in_si("deg",angle)==Constant::deg );
	test_expr( quantity_in_si("1*rad",angle)==1.0 );
	test_expr( quantity_in_si("1*deg",angle)==Constant::deg );
	test_expr( quantity_in_si("3*deg",angle)==3*Constant::deg );
	test_expr( in_units(Constant::deg,"deg",angle)=="1*deg" );

	test_expr( unit_in_si("m^2",area)==1.0 );
	test_expr( unit_in_si("m^3/s",m3_s)==1.0 );
	test_expr( quantity_in_si("1*m^2",area)==1.0 );
	test_expr( quantity_in_si("1*m^3/s",m3_s)==1.0 );

	test_code_throws( quantity_in_si("",length) );
	test_code_throws( quantity_in_si("1",length) );
	test_code_throws( quantity_in_si("1*kg",length) );
	test_code_throws( quantity_in_si("*m",length) );
	test_code_throws( quantity_in_si("1*",length) );
	test_code_throws( quantity_in_si("1*2*m",length) );
	test_code_throws( quantity_in_si("1.2.3*m",length) );
	test_code_throws( quantity_in_si("1*m*1",length) );
}

int main()
{
	conversion_tests();
	unit_tests();

	// report test results and return the numer of errors.
        test_report;
        return test_nerrors();
}
