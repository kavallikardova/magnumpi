#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
hints:
  DockerRequirement:
    dockerImageId: magnumpi
baseCommand: calc_cs
inputs:
  config:
    type: File
    doc: Configuration file
    inputBinding:
      position: 1
outputs:
  cs:
    type: File
    outputBinding:
      glob: cs.txt
  ci:
    type: File
    outputBinding:
      glob: ci.txt
