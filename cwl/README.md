[Common workflow language](https://www.commonwl.org) tool wrappers.

# Dependencies

Requirements for the CWL tool wrappers:
* [Docker](https://docs.docker.com)
* [CWL interpreter](https://github.com/common-workflow-language/cwltool)

The reference CWL interpreter can be installed with
```
pipenv install cwlref-runner
pipenv shell 
```

# Build

Build binaries inside a Docker image.

From root of this repo run:
```
docker build -t magnumpi .
```

# Run

The CWL 

```
./cwl/calc_cs.cwl --config input/finite_well_cs.txt
./cwl/calc_dcs.cwl --config input/finite_well_dcs.txt
```

Output `*.txt` files will be written to current directory.
