#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
hints:
  DockerRequirement:
    dockerImageId: magnumpi
baseCommand: calc_dcs
inputs:
  config:
    type: File
    doc: Configuration file
    inputBinding:
      position: 1
outputs:
  dcs:
    type: File
    outputBinding:
      glob: DCS.txt
  cs:
    type: File
    outputBinding:
      glob: cs_odd_even.txt
