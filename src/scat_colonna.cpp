/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/scat_colonna.h"
#include "magnumpi/potential.h"
#include "magnumpi/consts.h"

#include <cmath>
#include <algorithm>
#include <iostream>
#include <limits>

namespace magnumpi
{

double scat_Colonna_original(const potential *V, double b, double eps, double tolerance, int n)
{
	/* Part 1/3 - find r_high
	 *
	 * In this first part of the function, we look for a suitable r_high,
	 * based on the algorithm that is described by Colonna2008, equations
	 * 13-17 and accompanying text. Starting from an initial proposition
	 * for r_high, its value is increased until the relative difference
	 * between bI1 and bI2 (given by Colonna equation 15, except for an
	 * additional multiplication with b) is below the tolerance.
	 *
	 * Note that the variable 'integral' includes a factor b to avoid
	 * a singularity for b=0. In Colonna that factor is taken into account
	 * at the end of the calculation when his equation (1) is used to
	 * transform the integral into the scattering angle.
	 */
	/** \todo Should the parameters r_max, r_high_step be configurable?
	 *        In the input files we already have rmin, rmax, rdel, for
	 *        example. Values could taken from the calc_params structure.
	 *  \todo in the DCS version, r_high is doubled in the loop; no fixed
	 *        step is taken so dr_high_step is not needed. What should we do?
	 *  \todo For an ill-specified potential that does not go to zero for
	 *        r->infty, this function will never return. This can be solved
	 *        by exiting the loop with an error in case r_high would become
	 *        larger than r_max. (At present, r_max is used as the INITIAL
	 *        value for r_high --- that is confusing so we better redefine
	 *        the meaning of r_max and perhaps change its value.)
	 *  \todo In the code below, bI1 or bI2 is added to the integral
	 *        contribution, depending on the sign of V(r_high). This must be
	 *        explained. Is this necessary? Since bI1 and bI2 are lower and
	 *        upper bounds of the integral, it seems to make more sense to
	 *        add the average value.
	 *        It is not clear from Colonna's paper how they handle this
	 *        contribution, if at all.
	 *  \todo Test that b>0. The calculation of 'error' does not handle b==0,
	 *        maybe fix that? (Update: a test for b==0 has been added.)
	 *  \todo In scat_colonna_dcs.cpp, no r_high_step appears,
	 *        r_high *= 2 is used. Decide what is best, and
	 *        make this the same in the two implementations.
	 *  \todo In scat_colonna_dcs.cpp there is an additional check. Apart
	 *        from error <= tolerance It is also checked that
	 *        1.0 - std::pow(b/r_high, 2) - pot_high / eps > 0,
	 *        which is needed if the initial r_high is chosen too low.
	 *        Decide what is best, and make this the same in the two
	 *        implementations.
	 */
	const double r_max = 25e-9;
	double r_high = (b > r_max) ? b + 1e-9 : r_max;
	// These will be properly initialized in the for loop below.
	double pot_high = std::numeric_limits<double>::quiet_NaN();
	double integral = std::numeric_limits<double>::quiet_NaN();
	for (;;)
	{
		pot_high = V->value(r_high);
		// Colonne2008 eqn. 15, multiplied with b
		const double bI1 = std::asin(b/r_high/std::sqrt(1.0-pot_high/eps));
		const double bI2 = std::asin(b/r_high);
		const double error = b==0.0 ? 0.0 : std::abs(bI1 - bI2) / bI1;
		if (error <= tolerance)
		{
			// choose an underestimate of the long range scattering angle
			integral = pot_high<0 ? bI1 : bI2;
			break;
		}
		const double r_high_step = 2.5e-9;
		r_high += r_high_step;
	}

	/* Part 2/3 - Calculate the integral for (r_m,r_high].
	 *
	 * NOTE also here that 'integral' includes an additional factor b when
	 * compared with Colonna, which is compensated when the integral is
	 * used to calculate the scattering angle.
	 */
	/** \todo In the code there was a commented out variable double
	 *        dr_rel=1e-10, accompanied by the comment 'Switch to the
	 *        analytical integration when delta_r reaches this value'.
	 *        Is that still a useful outstanding task?
	 *  \todo Define rel_test, rel_stop. In scat_colonna_dcs.cpp, these
	 *        do not appear, a different stop criterion is used. Decide
	 *        what works best, and make this the same in both implementations
	 *        (if possible).
	 *  \todo Related: check what lower bound to use for delta_r. There
	 *        may be serious cancellation errors and fake precision when
	 *        that gets too small. What is the lower limit dictated by
	 *        these finite-precision concerns? Also look critically at
	 *        operations such as `delta_r=r_high-r_low` and `I+=value*delta_r`
	 *        from this perspective.
	 *  \todo Check the influence of the choice for the initial delta_r.
	 *        Is the present choice a good one?
	 *  \todo What is the purpose and meaning of r0? Document this.
	 *  \todo In the calculation of q, q is clipped to at least 2,
	 *        in Colonna2008 (below equation 4) the value 1.5 is suggested.
	 *  \todo Remove 'delta_r' out of the loop. Apply that when adding
	 *        'integrand*delta_r' to the integral.
	 */

	/* For convenience: introduce (lambda) function F, denoting the integrand
	 * (Colonna eqn. 14)
	 */
	auto F = [eps, b](double r, double pot)
	{
		const double b_r=b/r;
		return 1.0/((r*r)*std::sqrt(1.0 - b_r*b_r - pot/eps));
	};
	const double rel_test = 1e-5;
	const double rel_stop = 1e-12;
	double delta_r = 1e-12;
	double r0 = 0.0;
	double F_high = F(r_high,pot_high);
	// Loop for the integration from r_high to a value close to r0 (eq 1)
	for(;;)
	{
		// Obtain the integral contribution of one segment.
		// (I2 will be properly initialized in the for loop below.)
		double I2 = std::numeric_limits<double>::quiet_NaN();
		for(;;)
		{
			/* Calculate r_low and the values of the potential
			 * and F in that point. Reduce the step size
			 * if necessary (i.e. the new r_low is negative
			 * or is below r0).
			 */
			double r_low = r_high - delta_r;
			double pot_low = V->value(r_low);
			while ((1.0-std::pow(b/r_low, 2)-pot_low/eps) < 0.0 || r_low < 0.0)
			{
				delta_r = delta_r / 2.0;
				r_low = r_high - delta_r;
				pot_low = V->value(r_low);
			}
			const double F_low = F(r_low, pot_low);

			// calculate r_mid and the values of the potential
			// and F in that point.
			const double r_mid = (r_high + r_low) / 2.0;
			const double pot_mid = V->value(r_mid);
			const double F_mid = F(r_mid, pot_mid);

			/* calculate integral contributions according
			 * to Colonna2008 (eqns. 7,8,9), multiplied
			 * with b. NOTE: the factor delta_r is not yet
			 * included: we do that after a suitable delta_r
			 * has been established, when I2*delta_r is added
			 * to the variable 'integral'.
			 */
			const double I_trapz = (F_high + F_low) / 2.0;
			const double I_midp  = F_mid;

			// Apply Colonna's integration rule:
			const double I1 = I_trapz;
			I2 = (I_trapz + I_midp) / 2.0;
			const double error = std::abs(I2 - I1) / I2;

			// evaluate error
			if (error < tolerance)
			{
				/* We found a suitable dr. Adopt the 'low'
				 * variables as the new 'high' variables to
				 * prepare for the next step.
				 *
				 * Also add the contribution to the integral.
				 * Note that our integral includes the factor b.
				 * In Colonna, that factor is added afterwards,
				 * along with the factor 2. Also note that I2
				 * does NOT include delta_r, we add that here
				 * (saving repeated delta_r multiplications in
				 * the inner loop).
				 */
				integral += b*I2*delta_r;
				r_high = r_low;
				pot_high = pot_low;
				F_high = F_low;
				/* If the error is small enough, increase
				 * delta_r (Colonna2008 equation 4).
				 */
				if (error < tolerance / 4.0)
				{
					const double q = std::min(2.0, 0.9 * std::pow(tolerance / error, n + 1));
					delta_r *= q;
				}
				/* Finish the delta_r step by breaking out of the
				 * inner loop.
				 */
				break;
			}
			else
			{
				/* The error exceeds the tolerance. Decrease
				 * delta_r and try again (continue the inner
				 * loop).
				 */
				const double q = std::max(0.5, 0.9 * std::pow(tolerance / error, n + 1));
				delta_r *= q;
			}
		}

		/* Check if we have reached reached a distance sufficiently
		 * close to r0. If so, 'integral' is complete and we break
		 * out of the outer loop. Otherwise we try try again (with
		 * the modified delta_r).
		 */
		/** \todo in the paper the contribution of the last
		 *        section (closest to r0) is extrapolated
		 *        with an analytical function. Is that a better
		 *        way to cutoff the integration?
		 *  \todo When can r0 be <=0.0? Was this if used perhaps
		 *        to force at least two runs in this loop (note
		 *        that r0 is initialized to 0.0 before the loop
		 *        starts)? It seems that the r0 > 0.0 half of
		 *        the if statement can be removed.
		 *  \todo Explain why the else clause is needed here.
		 *        Isn't r0 our closest guess for the minimal
		 *        distance? If so, why not get rid of variable r0
		 *        and just use the value of r_high at this point?
		 */
		if (r0 > 0.0 && delta_r / r_high <= rel_stop)
		{
			// end outer while loop
			break;
		}
		else if (delta_r / r_high <= rel_test) // && (delta_r/r_high)<dr_rel)
		{
			r0 = r_high;
		}
	}
	/* Part 3/3 - Calculate and return the scattering angle.
	 *
	 * Use Colonna2008 eqn. 1 to calculate the scattering angle
	 * and return the result. Note that 'integral' already
	 * contains the factor b that appears in the equation.
	 * scattering angles that are sufficiently small (that
	 * is: chi/pi is smaller than the tolerance are clipped
	 * to 0.0.
	 */
	/** \todo Rethink whether this clipping is a good idea,
	 *        and how it should be implemented in detail.
	 */
	const double scat = Constant::pi - 2*integral;
	return std::abs(scat/Constant::pi)<tolerance ? 0 : scat;
}

double scat_Colonna_rk45(const potential *V, double b, double eps, double tolerance)
{
	// Set up the integrand
	auto sqrt_term = [eps, b](double r, double pot)
	{
		// This value has to be positive throughout the entire
		// integration domain.
		const double b_r = b / r;
		return 1.0 - b_r * b_r - pot / eps;
	};
	auto integrand = [eps, b](double r, double sqrt_term_value)
	{
		return 2 * b * std::pow(sqrt_term_value, -0.5) / (r * r);
	};

	// Starting r
	double r_max = 10;
	// Initial step size, note dr is negative! We're integrating from a large value to a small value
	double dr = -1e-12;
	// Value of the sqrt term in Theta
	double sqrt_term_value;

	// Accumulated value of the integral
	double integral = 0.0;
	// Intermediate step values for the Runge-Kutta scheme
	double k1, k3, k4, k5, k6, k7;
	// Boolean to signal whether or not the sqrt term is positive
	bool sqrt_positive;
	// Starting r of every step
	double r = r_max;

	// Main integration loop
	while (true)
	{
		double C;
		// If for any intermediate step the sqrt term is negative
		// then sqrt_positive==false.
		sqrt_positive = true;

		// Calculate k1-k7, make sure the sqrt term remains positive.
		sqrt_term_value = sqrt_term(r, V->value(r));
		sqrt_positive = (sqrt_positive && (sqrt_term_value > 0)) ? true : false;
		k1 = dr * integrand(r, sqrt_term_value);

		// C=1.0/5.0
		// the k2 term is only used if the rhs would also depend on theta somehow
		// sqrt_term_value=sqrt_term(r+C*dr,V->value(r+C*dr));
		// sqrt_positive=(sqrt_positive && (sqrt_term_value>0))? true:false;
		// k2=dr*integrand(r+C*dr,sqrt_term_value);

		C = 3.0 / 10.0;
		sqrt_term_value = sqrt_term(r + C * dr, V->value(r + C * dr));
		sqrt_positive = (sqrt_positive && (sqrt_term_value > 0)) ? true : false;
		k3 = dr * integrand(r + C * dr, sqrt_term_value);

		C = 4.0 / 5.0;
		sqrt_term_value = sqrt_term(r + C * dr, V->value(r + C * dr));
		sqrt_positive = (sqrt_positive && (sqrt_term_value > 0)) ? true : false;
		k4 = dr * integrand(r + C * dr, sqrt_term_value);

		C = 8.0 / 9.0;
		sqrt_term_value = sqrt_term(r + C * dr, V->value(r + C * dr));
		sqrt_positive = (sqrt_positive && (sqrt_term_value > 0)) ? true : false;
		k5 = dr * integrand(r + C * dr, sqrt_term_value);

		C = 1.0;
		sqrt_term_value = sqrt_term(r + C * dr, V->value(r + C * dr));
		sqrt_positive = (sqrt_positive && (sqrt_term_value > 0)) ? true : false;
		k6 = dr * integrand(r + C * dr, sqrt_term_value);

		// C = 1.0;
		// sqrt_term_value = sqrt_term(r + C * dr, V->value(r + C * dr));
		// sqrt_positive = (sqrt_positive && (sqrt_term_value > 0)) ? true : false;
		// k7 = dr * integrand(r + C * dr, sqrt_term_value);
		// The RHS does not depend on theta, in that case k7=k6
		k7 = k6;

		// Low order estimate
		double integral_next_low = integral + (5179.0 / 57600.0) * k1
		+ (7571.0 / 16695.0) * k3 + (393.0 / 640.0) * k4
		+ (-92097.0 / 339200.0) * k5 + (187.0 / 2100.0) * k6
		+ (1.0 / 40.0) * k7;
		// High order estimate
		double integral_next = integral + (35.0 / 384.0) * k1
		+ (500.0 / 1113.0) * k3 + (125.0 / 192.0) * k4
		+ (-2187.0 / 6784.0) * k5 + (11.0 / 84.0) * k6;
		// Estimate the error as the difference between the low and high order
		double error = std::abs(integral_next - integral_next_low);
		// Take the largest of the absolute and relative errors
		error = std::max(error, error / std::abs(integral_next));
		// Compute the factor by which the next step should change in size
		double factor = std::pow(tolerance / error, (1.0 / 5.0));

		// To prevent arbitrarily large changes to the stepsize
		// it is limited to x2 or /2.
		if (factor > 2)
		{
			factor = 2;
		}
		else if (factor < 0.5)
		{
			factor = 0.5;
		}
		double dr_next = 0.9 * dr * factor;

		if (sqrt_positive == false || r + dr < 0 || std::isnan(error))
		{
			// The sqrt term has become negative, resulting in a complex value
			// the scattering angle has to be real, so we take a smaller step.
			dr = dr / 2.0;
			if (r + dr == r)
			{
				// We've reached the limits of machine precision, there is no point in continuing
				break;
			}
		}
		else if (error > tolerance)
		{
			// The error is too large, do not accept the computed value
			// start again with a new dr.
			dr = dr_next;
		}
		else if (r == r + dr)
		{
			// We've reached the limits of machine precision, there is no point in continuing
			break;
		}
		else
		{
			// If everything is OK, accept this step.
			if (std::abs(integral - integral_next) < std::abs(integral_next * tolerance) && factor != 2)
			{
				// The integration process is no longer making any significant progress
				break;
			}
			integral = integral_next;
			r = r + dr;
			dr = dr_next;
		}
	}

	// We integrated from [r_max,rm], this flips the bounds of the integral and introduces an extra -
	return Constant::pi + integral;
}

} // namespace magnumpi

#include "magnumpi/data_set.h"
#include "magnumpi/utility.h"
#include <vector>

namespace magnumpi {

data_set get_scat_vs_b_vs_E(const std::vector<double>& b_vals, const std::vector<double>& energy_vals,
	const potential* pot, int interpol, double tol)
{
	data_set chi_data(
		"chi(b,E)", units::angle, "rad",
		{"b", units::length, "m"},
		{"E", units::energy, "J"}
	);
	chi_data.col_axis().set_display_unit("eV");

	// first set up the columns. These are labeled with
	// numerical values that represent the energies
	for (unsigned int i = 0; i < energy_vals.size(); ++i)
	{
		chi_data.add_column(energy_vals[i]);
	}
	for (unsigned int r = 0; r != b_vals.size(); ++r)
	{
		chi_data.add_row(b_vals[r]);

		for (unsigned int c = 0; c != energy_vals.size(); ++c)
		{
			/** \todo Should the case b==0 be handled by
			 *        scat_Colonna instead?
			 */
			// perform integration
			double scat = (b_vals[r] == 0.0)
				? Constant::pi
				: scat_Colonna(pot, b_vals[r], energy_vals[c], tol, interpol);

			// write impact parameter and scattering angle
			chi_data(r, c) = scat;
		}
	}
	return chi_data;
}

data_set get_scat_vs_b_vs_E(double b_low, double b_high, int num_b, double logE_low, double logE_high,
	int logE_pdecade, int interpol, double tol, const potential* pot)
{
	// impact parameter values
	std::vector<double> b_vals = make_samples_lin(b_low,b_high,num_b);
	// energy values
	std::vector<double> e_vals = make_samples_log(logE_low, logE_high, logE_pdecade);
	return get_scat_vs_b_vs_E(b_vals, e_vals, pot, interpol, tol);
}

data_set get_scat_vs_b_vs_E(const json_type& cnf, const json_type& pot)
{
	const std::unique_ptr<const potential> V{potential::create(pot)};

	// Set tolerance and interpolation order
	const int interpol=3;
	const double tolerance = cnf.at("tolerance");

	const double b_low=in_si(cnf.at("b_range").at("min"),units::length);
	const double b_high=in_si(cnf.at("b_range").at("max"),units::length);
	const int num_b=cnf.at("b_range").at("points");

	const double logE_low=std::log10(in_si(cnf.at("energy_range").at("min"),units::energy));
	const double logE_high=std::log10(in_si(cnf.at("energy_range").at("max"),units::energy));
	const int E_pdecade=cnf.at("energy_range").at("points_per_decade");

	std::cout << "Calculating chi(b,E)" << std::endl;

	return get_scat_vs_b_vs_E(b_low, b_high, num_b, logE_low, logE_high, E_pdecade,interpol,tolerance,V.get());
}

data_set get_scat_vs_b_vs_E(const json_type& cnf)
{
	return get_scat_vs_b_vs_E(cnf,cnf.at("interaction").at("potential"));
}

json_type get_scattering_angles(const json_type& cnf)
{
	return get_scat_vs_b_vs_E(cnf).write_json();
}

} // namespace magnumpi
