/** \file
 *
 * Copyright (C) 2020,2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

// Based on:
//  https://stackoverflow.com/questions/56265040/boost-beast-sample-to-send-post
//
// Changes wrt that code:
//
//  - the fixes that were suggested in the reply to that question:
//      x dynamic_body -> string_body
//      x comment in req_.body() = body;
//  - removed the create_body() function; the body is now read from the command line
//      => Boost property_tree/json headers are no longer needed: removed includes.
//  - just return 1 (error) or 0 (success), do not use the EXIT_XXX macros's and
//      remove the cstdlib include
//

#include "magnumpi/beast_post.h"
#include "magnumpi-config.h"

#ifndef MAGNUMPI_HAVE_BOOST

#include "magnumpi/json.h"

namespace magnumpi {

/** If we do not have Boost (beast), we implement post_request
 *  to return a document with error code 1. The advnatage is that
 *  for now we have no hard requirement for Boost, so the CI
 *  jobs will not fail.
 *
 *  \todo Make sure boost-1.66 or later is available on platforms where this is compiled.
 */
json_type post_request(
	const std::string& host,
	const std::string& port,
	const std::string& target,
	const std::string& req_body,
	int version)
{
	json_type res;
	res["meta"]["error"]=1;
	res["meta"]["error_msg"]="magnumpi: post_request not available (Boost>=1.66) not found.";
	return res;
}

} // namespace magnumpi

#else

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <memory>
#include <string>
#include <stdexcept>

#include "magnumpi/json.h"

namespace magnumpi {

namespace  {

namespace beast = boost::beast; // from <boost/beast.hpp>
namespace http = beast::http; // from <boost/beast/http.hpp>
namespace net = boost::asio; // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp; // from <boost/asio/ip/tcp.hpp>

// Performs an HTTP POST and prints the response
class session : public std::enable_shared_from_this<session>
{
	tcp::resolver resolver_;
	beast::tcp_stream stream_;
	beast::flat_buffer buffer_; // (Must persist between reads)
	http::request<http::string_body> req_;
	http::response<http::string_body> res_;
	json_type result;

	// Report a failure
	void fail(beast::error_code ec, const std::string& what)
	{
std::cout << what << ": " << ec.message() << std::endl;
		result["meta"]["result"]=ec.value();
		throw std::runtime_error(what + ": " + ec.message());
	}

public:
	const json_type& json_result() const { return result; }
	// Objects are constructed with a strand to
	// ensure that handlers do not execute concurrently.
	explicit
		session(net::io_context& ioc)
		: resolver_(net::make_strand(ioc))
		, stream_(net::make_strand(ioc))
	{
	}
	// Start the asynchronous operation
	void
		run(
			const std::string& host,
			const std::string& port,
			const std::string& target,
			const std::string& body,
			int version)
	{
		// Set up an HTTP POST request message
		req_.version(version);
		req_.method(http::verb::post);
		req_.target(target);
		req_.set(http::field::host, host);
		req_.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
		req_.set(http::field::content_type, "application/json");
		/** \todo JvD before we used set() to set the content lengt.
		 *  That does not work with recent boost versions (>=1.74, I
		 *  believe). See the last API change notes on
		 *  https://www.boost.org/doc/libs/1_74_0/libs/beast/doc/html/beast/release_notes.html
		 *  \todo Check if the new syntax also works on older Boost versions,
		 *  add version detection logic otherwise.
		 */
		// req_.set(http::field::content_length, body.length());
		req_.content_length(body.length());
		req_.body() = body;
		req_.prepare_payload();
		// Look up the domain name
		resolver_.async_resolve(
			host,
			port,
			beast::bind_front_handler(
				&session::on_resolve,
				shared_from_this()));
	}
	void
		on_resolve(
			beast::error_code ec,
			tcp::resolver::results_type results)
	{
		if (ec)
			return fail(ec, "resolve");
		// Set a timeout on the operation
		stream_.expires_after(std::chrono::seconds(30));
		// Make the connection on the IP address we get from a lookup
		stream_.async_connect(
			results,
			beast::bind_front_handler(
				&session::on_connect,
				shared_from_this()));
	}
	void
		on_connect(beast::error_code ec, tcp::resolver::results_type::endpoint_type)
	{
		if (ec)
			return fail(ec, "connect");
		// Set a timeout on the operation
		stream_.expires_after(std::chrono::seconds(30));
		// Send the HTTP request to the remote host
		http::async_write(stream_, req_,
			beast::bind_front_handler(
				&session::on_write,
				shared_from_this()));
	}
	void
		on_write(
			beast::error_code ec,
			std::size_t bytes_transferred)
	{
		boost::ignore_unused(bytes_transferred);
		if (ec)
			return fail(ec, "write");
		// Receive the HTTP response
		http::async_read(stream_, buffer_, res_,
			beast::bind_front_handler(
				&session::on_read,
				shared_from_this()));
	}
	void
		on_read(
			beast::error_code ec,
			std::size_t bytes_transferred)
	{
		boost::ignore_unused(bytes_transferred);
		if (ec)
			return fail(ec, "read");
		// Gracefully close the socket
		stream_.socket().shutdown(tcp::socket::shutdown_both, ec);
		// not_connected happens sometimes so don't bother reporting it.
		if (ec && ec != beast::errc::not_connected)
			return fail(ec, "shutdown");
		// If we get here then the connection is closed gracefully

		/** \todo If we arrive here, we got a reply.
		 *  Copy the fields from the header into the result,
		 *  and if a body was received as well, copy that too.
		 *  At present we decide if that is the case by checing
		 *  if the result code is equal to 200 ("OK").
		 *
		 *  Is there a better way to find out what was sent? Under
		 *  what conditions do we receive a header, but not a body?
		 *
		 *  All this logic must be reviewed by somebody who knows
		 *  what he or she is doing.
		 */

		json_type& header = result["meta"]["header"];
		for (const auto& f : res_.base())
		{
			header[std::string(f.name_string())]=f.value();
		}
		result["meta"]["result"]=res_.result();
		if (res_.result_int()==200)
		{
			result["body"] = json_type::parse(res_.body());
		}
	}
};

} // namespace

json_type post_request(
	const std::string& host,
	const std::string& port,
	const std::string& target,
	const std::string& req_body,
	int version)
{
	// The io_context is required for all I/O
	net::io_context ioc;
	// Launch the asynchronous operation
	auto s = std::make_shared<session>(ioc);
	try {
		s->run(host, port, target, req_body, version);
		// Run the I/O service. The call will return when
		// the get operation is complete.
		ioc.run();
	}
	catch(std::exception& exc)
	{
		json_type res = s->json_result();
		res["meta"]["error_msg"]=exc.what();
		return res;
	}
	return s->json_result();
}

} // namespace magnumpi

#endif // MAGNUMPI_HAVE_BOOST_H
