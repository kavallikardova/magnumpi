/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/json.h"
#include "plutil/environment.h"

#include <fstream>
#include <iostream>
#include <stdexcept>

namespace magnumpi {

json_type read_json_from_stream(std::istream& is)
{
	std::string str((std::istreambuf_iterator<char>(is)),
		std::istreambuf_iterator<char>());
	return json_type::parse(str);
}

json_type read_json_from_file(const std::string& fname)
{
	std::ifstream is(plasimo::substitute_env_vars_copy(fname,true));
	if (!is)
	{
		throw std::runtime_error("Could not open file '"
			+ fname + "' for reading.");
	}
	try {
		return read_json_from_stream(is);
	}
	catch (std::exception& exc)
	{
		throw std::runtime_error("Error reading file '"
			+ fname + "':\n" + std::string(exc.what()));
	}
}

void write_json_to_file(const json_type& data, const std::string& fname, int indent)
{
	std::ofstream os(plasimo::substitute_env_vars_copy(fname,true));
	if (!os)
	{
		throw std::runtime_error("Could not open file '"
			+ fname + "' for writing.");
	}
	try {
		os << data.dump(indent) << std::endl;
	}
	catch (std::exception& exc)
	{
		throw std::runtime_error("Error writing JSON file '"
			+ fname + "':\n" + std::string(exc.what()));
	}
}

}; // namespace magnumpi
