/** \file
 *
 * Copyright (C) 2016-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/potential.h"
#include "plutil/environment.h"
#include "magnumpi/consts.h"
#include "magnumpi/utility.h"
#include "magnumpi/beast_post.h"
#include "tk_spline/spline.h"
#include <string>
#include <stdexcept>
#include <cmath>
#include <iostream>
#include <vector>
#include <fstream>
#include <limits>

namespace magnumpi {

potential::~potential()
{
}

data_set potential::write_xny(double rmin, double rmax, std::size_t npoints) const
{
	data_set V_data(
		"V(r)", units::energy, "J",
		{"distance", units::length, "m"},
		{"V(r)"} );
	V_data.add_column("potential");
	const auto rvec = make_range(rmin,rmax,npoints,false);
	for (const double r : rvec)
	{
		V_data.add_row(r,{ value(r) });
	}
	return V_data;
}

json_type potential::write_json(double rmin, double rmax, std::size_t npoints) const
{
	return write_xny(rmin,rmax,npoints).write_json();
}

double rmin_input_evaluation(double r, const magnumpi::potential* p, double Vmin)
{
	double Vr=p->value(r);
	unsigned iteration_count=0;
	/* If the potential is piecewise constant, and there is no bound on the number of
	 * iterations used, this loop will never terminate. If r is initially 1 m, after 760
	 * iterations it is reduced to approx a Planck length, which is small enough to cover all cases.
	 */
	while (Vr<Vmin && iteration_count<760)
	{
		r*=0.9;
		Vr=p->value(r);
		++iteration_count;
	}
	std::cout << "rmin\t" << r << std::endl;
	return r;
}

double rmax_input_evaluation(double r, const magnumpi::potential* p, double Vmax)
{
	double Vr=std::abs(p->value(r));
	unsigned iteration_count=0;
	while (Vr>Vmax && iteration_count<760)
	{
		r*=1.1;
		Vr=std::abs(p->value(r));
		++iteration_count;
	}
	if (r>100e-10)
	{
		std::cout << "WARNING: rmax exceeds 100 Angstrom" << std::endl;
		r=100e-10;
		std::cout << "Value limited to 100 Angstrom" << std::endl;
	}
	std::cout << "rmax\t" << r << std::endl;
	return r;
}

/** The Hulburt-Hirschfelder potential.
 *
 *  This model potential is defined in equation (31) of Colonna et al; \cite Colonna2008
 */
class HulburtHirschfelder: public potential
{
  public:
	HulburtHirschfelder(double alpha,double beta,double gamma,double phi0,double re)
	 : m_alpha(alpha),m_beta(beta),m_gamma(gamma),m_phi0(phi0),m_re(re)
	{}
	HulburtHirschfelder(const json_type& cnf)
	 : HulburtHirschfelder(
		cnf.at("alpha"),
		cnf.at("beta"),
		cnf.at("gamma"),
		in_si(cnf.at("phi0"),units::energy),
		in_si(cnf.at("re"),units::length)
	)
	{
	}
  protected:
	virtual double get_value(double r) const
	{
		using std::exp;
		using std::pow;
		double rt=r/m_re-1.0;
		double res=m_phi0*(exp(-2.0*m_alpha*rt)-2.0*exp(-m_alpha*rt)+m_beta*pow(rt,3)*(1.0+m_gamma*rt)*exp(-2.0*m_alpha*rt));
		return std::isnan(res) ? 0 : res ;
	}
	virtual double get_dVdr(double r) const
	{
		using std::exp;
		using std::pow;
		double rt=r/m_re-1.0;
		double dVdr= m_phi0*exp(-2.0*m_alpha*rt)/m_re*(m_beta*pow(rt,2)*(4.0*m_gamma*rt+3.0)
			+2.0*m_alpha*(-1.0+exp(m_alpha*rt)-m_beta*pow(rt,3)*(m_gamma*rt+1.0)) );
		return std::isnan(dVdr) ? 0 : dVdr;
	}
	virtual double get_d2Vdr2(double r) const
	{
		using std::exp;
		using std::pow;
		double rt=r/m_re-1.0;
		double d2Vdr2 = -2.0*m_phi0*exp(-2.0*m_alpha*rt)/pow(m_re,2)*(
				-3.0*m_beta*rt*(2.0*m_gamma*rt+1.0)
				+2.0*m_alpha*m_beta*pow(rt,2)*(4.0*m_gamma*rt+3.0)
				+pow(m_alpha,2)*(exp(m_alpha*rt)-2.0*(1.0+m_beta*pow(rt,3)*(m_gamma*rt+1.0) ))  );
		return std::isnan(d2Vdr2) ? 0 : d2Vdr2 ;
	}
  private:
	const double m_alpha;
	const double m_beta;
	const double m_gamma;
	const double m_phi0;
	const double m_re;
};

/** The Exponential-Spline-Morse-Spline-Van der Waals (ESMSV) potential.
 *
 *  This model potential is defined on page 6736 of Brunetti et al; \cite Brunetti1981
 */
class ESMSV: public potential
{
  public:
	ESMSV(double eps,double rm,double betam,double A,
		double alpha,double beta,
		double a1,double a2, double a3,double a4,
		double gamma,
		double x1,double x2,double x3,double x4,
		double b1,double b2,double b3,double b4,
		double C6)
	 : m_eps(eps),
	   m_rm(rm),
	   m_betam(betam),
	   m_A(A),
	   m_alpha(alpha),
	   m_beta(beta),
	   m_a1(a1),
	   m_a2(a2),
	   m_a3(a3),
	   m_a4(a4),
	   m_gamma(gamma),
	   m_x1(x1),
	   m_x2(x2),
	   m_x3(x3),
	   m_x4(x4),
	   m_b1(b1),
	   m_b2(b2),
	   m_b3(b3),
	   m_b4(b4),
	   m_C6(C6),
	   m_c6(m_C6/(m_eps*std::pow(m_rm,6)))
	{}
	ESMSV(const json_type& cnf)
	 : ESMSV(
		in_si(cnf.at("eps"),units::energy),
		in_si(cnf.at("rm"),units::length),
		cnf.at("betam"),
		cnf.at("A"),
		cnf.at("alpha"),
		in_si(cnf.at("beta"),units::area),
		cnf.at("a1"),
		cnf.at("a2"),
		cnf.at("a3"),
		cnf.at("a4"),
		cnf.at("gamma"),
		cnf.at("x1"),
		cnf.at("x2"),
		cnf.at("x3"),
		cnf.at("x4"),
		cnf.at("b1"),
		cnf.at("b2"),
		cnf.at("b3"),
		cnf.at("b4"),
		in_si(cnf.at("C6"),units::Jm6)
	)
	{
	}
  protected:
	virtual double get_value(double r) const
	{
		using std::exp;
		using std::pow;
		const double x=r/m_rm;
		double phi;
		if (x<=m_x1)
		{
			phi=m_A*exp(-m_alpha*(x-1.0));
		}
		else if (x<m_x2)
		{
			phi=exp(m_a1+(x-m_x1)*(m_a2+(x-m_x2)*(m_x3+(x-m_x1)*m_x4)) );
		}
		else if (x<=m_x3)
		{
			phi=exp(-2*m_betam*(x-1.0))-2*exp(-m_betam*(x-1.0));
		}
		else if (x<m_x4)
		{
			phi=m_b1+(x-m_x3)*(m_b2+(x-m_x4)*(m_b3+(x-m_x3)*m_b4));
		}
		else
		{
			phi=-m_c6*pow(x,-6)*(1.0+m_beta/pow(m_rm*x,2)*(1.0+m_beta*m_gamma/pow(m_rm*x,2)));
		}
		phi=phi*m_eps;
		return phi;
	}
	virtual double get_dVdr(double r) const
	{
		using std::exp;
		using std::pow;
		const double x=r/m_rm;
		double deriv;
		if (x<=m_x1)
		{
			deriv=-m_alpha/m_rm*m_A*exp(-m_alpha*(x-1.0));
		}
		else if (x<m_x2)
		{
			deriv=(m_a2 + m_a3*(2.0*x-m_x2-m_x1) + m_a4*(3.0*pow(x,2) - 4.0*m_x1*x - 2.0 *x*m_x2 + 2.0*m_x1*m_x2 + m_x1*m_x1) )/m_rm
				*exp(m_a1+(x-m_x1)*(m_a2+(x-m_x2)*(m_x3+(x-m_x1)*m_x4)) );
		}
		else if (x<=m_x3)
		{
			deriv=(- 2.0*m_betam*exp(-2*m_betam*(x-1.0)) + 2.0*m_betam*exp(-m_betam*(x-1.0)) ) / m_rm;
		}
		else if (x<m_x4)
		{
			//deriv=(m_b2 + m_b3*(2.0*x - m_x4 + m_x3) + m_b4*( 3.0*x*x - 4.0*x*m_x3 - 2.0*x*m_x4 + m_x3*m_x4 + m_x3*m_x3  ) )/m_rm;
			const double y = x-m_x3;
			deriv = (m_b2 + (m_x3-m_x4)*m_b3 + 2*y*(m_b3 + (m_x3-m_x4)*m_b4) + 3*y*y*m_b4)/m_rm;
		}
		else
		{
			deriv=-m_c6*(-10.0*pow(m_beta,2)*m_gamma/pow(m_rm,4)*pow(x,-11) - 8.0*m_beta*pow(x,-9)/pow(m_rm,2)-6.0*pow(x,-7))/m_rm;
		}
		return deriv*m_eps;
	}
	virtual double get_d2Vdr2(double r) const
	{
		using std::exp;
		using std::pow;
		const double x=r/m_rm;
		double deriv;
		if (x<=m_x1)
		{
			deriv=pow(m_alpha/m_rm,2)*m_A*exp(-m_alpha*(x-1.0));
		}
		else if (x<m_x2)
		{
			deriv = (2.0*m_a3 + m_a4*(6.0*x - 4.0*m_x1 - 2.0*m_x2) + std::pow(m_a2 + m_a3*(2.0*x-m_x2-m_x1) + m_a4*(3.0*pow(x,2) - 4.0*m_x1*x - 2.0 *x*m_x2 + 2.0*m_x1*m_x2 + m_x1*m_x1),2 ) )/m_rm/m_rm
			*exp(m_a1+(x-m_x1)*(m_a2+(x-m_x2)*(m_x3+(x-m_x1)*m_x4)) );
		}
		else if (x<=m_x3)
		{
			deriv= ( 4.0*m_betam*m_betam*exp(-2*m_betam*(x-1.0)) - 2.0*m_betam*m_betam*exp(-m_betam*(x-1.0)) ) / m_rm / m_rm;
		}
		else if (x<m_x4)
		{
			//deriv=( 2.0*m_b3 + m_b4*(6.0*x - 4.0*m_x3 - 2.0*m_x4) )/m_rm/m_rm;
			const double y = x-m_x3;
			deriv = (2*(m_b3 + (m_x3-m_x4)*m_b4) + 6*y*m_b4)/m_rm/m_rm;
		}
		else
		{
			deriv=-m_c6*(110.0*pow(m_beta,2)*m_gamma/pow(m_rm,4)*pow(x,-12) + 72.0*m_beta*pow(x,-10)/pow(m_rm,2) + 42.0*pow(x,-8))/m_rm/m_rm;
		}
		return deriv*m_eps;
	}
  private:
	const double m_eps;
	const double m_rm;
	const double m_betam;
	const double m_A;
	const double m_alpha;
	const double m_beta;
	const double m_a1;
	const double m_a2;
	const double m_a3;
	const double m_a4;
	const double m_gamma;
	const double m_x1;
	const double m_x2;
	const double m_x3;
	const double m_x4;
	const double m_b1;
	const double m_b2;
	const double m_b3;
	const double m_b4;
	const double m_C6;
	const double m_c6;
};

/** The BuckinghamCorner (6,8,10) potential.
 *
 *  This model potential is defined on page 6737 of Brunetti et al; \cite Brunetti1981
 */
class BuckinghamCorner : public potential
{
  public:
	BuckinghamCorner(double A,double alpha,double rm,double C6,double beta,double gamma)
	 : m_A(A),m_alpha(alpha),m_rm(rm),m_C6(C6),m_beta(beta),m_gamma(gamma),
	   m_C6t(m_C6/m_A/std::pow(m_rm,6)), m_betat(m_beta/(m_rm*m_rm))
	{}
	BuckinghamCorner(const json_type& cnf)
	 : BuckinghamCorner(
		in_si(cnf.at("A"),units::energy),
		cnf.at("alpha"),
		in_si(cnf.at("rm"), units::length),
		in_si(cnf.at("C6"),units::Jm6),
		in_si(cnf.at("beta"),units::area),
		cnf.at("gamma")
	)
	{}
  protected:
	virtual double get_value(double r) const
	{
		using std::exp;
		using std::pow;
		const double x = r/m_rm;
		const double p = -m_C6t*(pow(x,-6)*(1+m_betat*pow(x,-2)*(1+m_betat*m_gamma*pow(x,-2))));
		if (x<1)
		{
			const double h = -4*pow((1/x-1.0),3);
			return m_A*(exp(-m_alpha*(x-1.0)) + p*exp(h));
		}
		else
		{
			return m_A*(exp(-m_alpha*(x-1.0)) + p);
		}
	}
	virtual double get_dVdr(double r) const
	{
		using std::exp;
		using std::pow;
		const double x = r/m_rm;
		const double x2 = x*x;
		const double p1 = m_C6t*pow(x,-7)*(6 + m_betat/x2*(8 + 10*m_betat*m_gamma/x2));
		if (x<1)
		{
			const double p = -m_C6t*pow(x,-6)*(1+m_betat/x2*(1+m_betat*m_gamma/x2));
			const double h  = -4*pow((1/x-1),3);
			const double h1 = 12*pow((1/x-1),2)/x2;
			return (m_A/m_rm)*(-m_alpha*exp(-m_alpha*(x-1)) + (p1+p*h1)*exp(h));
		}
		else
		{
			return (m_A/m_rm)*(-m_alpha*exp(-m_alpha*(x-1)) + p1);
		}
	}
	virtual double get_d2Vdr2(double r) const
	{
		using std::exp;
		using std::pow;
		const double x = r/m_rm;
		const double x2 = x*x;
		const double p2 = -m_C6t*pow(x,-8)*(42 +m_betat/x2*(72 + 110*m_betat*m_gamma/x2));
		if (x<1)
		{
			const double p  = -m_C6t*pow(x,-6)*(1+m_betat/x2*(1+m_betat*m_gamma/x2));
			const double p1 =  m_C6t*pow(x,-7)*(6 + m_betat/x2*(8 + 10*m_betat*m_gamma/x2));
			const double h  =  -4*pow((1/x-1),3);
			const double h1 =  12*pow((1/x-1),2)/x2;
			const double h2 = -24*(1/x-1)*pow(x,-3)*(2/x-1);
			return (m_A/m_rm/m_rm)*(m_alpha*m_alpha*exp(-m_alpha*(x-1)) + (p2 + 2*p1*h1+p*(h2+h1*h1))*exp(h));
		}
		else
		{
			return (m_A/m_rm/m_rm)*(m_alpha*m_alpha*exp(-m_alpha*(x-1)) + p2);
		}
	}
  private:
	const double m_A;
	const double m_alpha;
	const double m_rm;
	const double m_C6;
	const double m_beta;
	const double m_gamma;
	const double m_C6t;
	const double m_betat;
};

/** The Exponential Repulsive potential.
 *
 *  This model potential is defined, for example, in equation (5)
 *  of Capitelli and Devoto (1973) \cite Capitelli1973
 *
 *  \todo Use A or phi_0 for the parameter V(r=0)? The latter
 *  is used in the paper, both seem to be quite common.
 */
class ExponentialRepulsive: public potential
{
  public:
	ExponentialRepulsive(double A,double rho)
	 : m_A(A),m_rho(rho)
	{}
	ExponentialRepulsive(const json_type& cnf)
	 : ExponentialRepulsive(
		in_si(cnf.at("A"), units::energy),
		in_si(cnf.at("rho"), units::length)
	)
	{}
  protected:
	virtual double get_value(double r) const
	{
		return m_A*std::exp(-r/m_rho);
	}
	virtual double get_dVdr(double r) const
	{
		return -m_A/m_rho*std::exp(-r/m_rho);
	}
	virtual double get_d2Vdr2(double r) const
	{
		return m_A/(m_rho*m_rho)*std::exp(-r/m_rho);
	}
  private:
	const double m_A;
	const double m_rho;
};

/** The Mason-Schamp (12,4) potential (special case, gamma=0)
 *
 *  The general Mason-Schamp model potential is defined in
 *  equation (1) of Mason et al. \cite Mason1958
 *  This class implements the special case of that equation
 *  with gamma=0.
 */
class MS_12_4 : public potential
{
  public:
	MS_12_4(double epsilon,double rm)
	 : m_epsilon(epsilon),m_rm(rm)
	{}
	MS_12_4(const json_type& cnf)
	 : MS_12_4(
		in_si(cnf.at("epsilon"),units::energy),
		in_si(cnf.at("rm"),units::length)
	)
	{}
  protected:
	virtual double get_value(double r) const
	{
		using std::pow;
		const double x=r/m_rm;
		const double Vt = (pow(x,-8)-3.0)*pow(x,-4)/2;
		return Vt*m_epsilon;
	}
	virtual double get_dVdr(double r) const
	{
		using std::pow;
		const double x=r/m_rm;
		const double dVt_dx = (-pow(x,-8)+1)*12*pow(x,-5)/2;
		return dVt_dx*m_epsilon/m_rm;
	}
	virtual double get_d2Vdr2(double r) const
	{
		using std::pow;
		const double x=r/m_rm;
		const double d2Vt_dx2 = (13*pow(x,-8)-5)*12*pow(x,-6)/2;
		return d2Vt_dx2*m_epsilon/(m_rm*m_rm);
	}
  private:
	const double m_epsilon;
	const double m_rm;
};

/** The Hard sphere potential
 */
class HardSphere: public potential
{
  public:
	//constructor
	HardSphere(double rm)
	 : m_rm(rm)
	{}
	HardSphere(const json_type& cnf)
	 : HardSphere(in_si(cnf.at("re"),units::length))
	{
	}
  protected:
	virtual double get_value(double r) const
	{
		if (r>m_rm)
			return 0.0;
		else
			return std::numeric_limits<double>::infinity();
	}
	virtual double get_dVdr(double r) const
	{
		/// \todo Document the magic number 1e-10 in this code
		if (std::abs(r - m_rm) < 1e-10 * m_rm)
			return std::numeric_limits<double>::infinity();
		else
			return 0.0;
	}
	virtual double get_d2Vdr2(double r) const
	{
		/// \todo Document the magic number 1e-10 in this code
		if (r - m_rm < 1e-10 * m_rm)
			return std::numeric_limits<double>::infinity();
		else
			return 0.0;
	}
  private:
	const double m_rm;
};

/** The 12-6 Lennard Jones potential.
 *
 *  See \cite LennardJones1924_1, \cite LennardJones1924_2, \cite LennardJones1931
 *  Also see \cite Torrens1972 section 4.4.
 */
class LennardJones: public potential
{
  public:
	LennardJones(double epsilon,double sigma)
	 : m_epsilon(epsilon),m_sigma(sigma)
	{
	}
	LennardJones(const json_type& cnf)
	 : LennardJones(
		in_si(cnf.at("epsilon"),units::energy),
		in_si(cnf.at("sigma"),units::length)
	)
	{}
  protected:
	virtual double get_value(double r) const
	{
		using std::pow;
		const double x=r/m_sigma;
		const double Vt = 4*( pow(x,-12)-pow(x,-6) );
		return Vt*m_epsilon;
	}
	virtual double get_dVdr(double r) const
	{
		using std::pow;
		const double x=r/m_sigma;
		const double dVt_dx = 4*( -12*pow(x,-13) + 6*pow(x,-7) );
		return dVt_dx*m_epsilon/m_sigma;
	}
	virtual double get_d2Vdr2(double r) const
	{
		using std::pow;
		const double x=r/m_sigma;
		const double d2Vt_dx2 = 4*( 156*pow(x,-14) - 42*pow(x,-8) );
		return d2Vt_dx2*m_epsilon/(m_sigma*m_sigma);
	}
  private:
	const double m_epsilon;
	const double m_sigma;
};

/** The generalized Lennard-Jones potential.
 *
 *  See \cite Laricchiuta2007_1 and \cite Pirani2001
 *
 *  \todo Implement the analytical expressions for the derivaties. At present,
 *        get_dVdr and get_d2Vdr2 use numerical derivation with a fixed dr.
 */
class LennardJonesLaricchiuta: public potential
{
  public:
	LennardJonesLaricchiuta(double epsilon,double re, double beta, bool is_neutral)
	 : m_epsilon(epsilon),m_re(re), m_beta(beta), m_m(is_neutral ? 6 : 4)
	{
	}
	LennardJonesLaricchiuta(const json_type& cnf)
	 : LennardJonesLaricchiuta(
		in_si(cnf.at("epsilon"),units::energy),
		in_si(cnf.at("re"),units::length),
		cnf.at("beta"),
		cnf.at("is_neutral")
	)
	{}
  protected:
	/// Returns the potential at \a r. See \cite Laricchiuta2007_1 eqn. 4
	virtual double get_value(double r) const
	{
		using std::pow;
		const double x=r/m_re;
		const double n=m_beta+4*x*x;
		const double Vt = m_m/(n-m_m)*pow(x,-n) - n/(n-m_m)*pow(x,-1.0*m_m);
		return Vt*m_epsilon;
	}
	virtual double get_dVdr(double r) const
	{
		//throw std::runtime_error("LennardJonesLaricchiuta: get_dVdr not implemented.");
		/// \todo For the moment, we do a numerical derivation
		double dr = 0.01e-9;
		return (value(r+dr)-value(r-dr))/(2*dr);
	}
	virtual double get_d2Vdr2(double r) const
	{
		//throw std::runtime_error("LennardJonesLaricchiuta: get_d2Vdr2 not implemented.");
		/// \todo For the moment, we do a numerical derivation
		double dr = 0.01e-9;
		return (value(r+dr)+value(r-dr)-2*value(r))/(dr*dr);
	}
  private:
	const double m_epsilon;
	const double m_re;
	const double m_beta;
	const unsigned m_m;
};

/** The Morse potential.
 *
 *  Many representations of this potential exist. The one implemented here
 *  is defined in equation (30) of Colonna et al; \cite Colonna2008
 *
 *  \todo In this representation there is one parameter too many:
 *        the potential depends only on the ratio C/sigma.
 *        Capitellie \cite Capitelli2012 eqn. 5.46 uses beta=C/sigma,
 *        in Wikipedia we find a instead of C/signa. I would like to
 *        get rid of one of the parameters.
 *        Internally we use (only) beta already.
 */
class Morse : public potential
{
  public:
	Morse(double epsilon,double re,double sigma,double C)
	 : m_epsilon(epsilon),m_re(re),m_sigma(sigma),m_C(C), m_beta(m_C/m_sigma)
	{}
	Morse(const json_type& cnf)
	 : Morse(
		in_si(cnf.at("phi0"),units::energy),
		in_si(cnf.at("re"),units::length),
		in_si(cnf.at("sigma"),units::length),
		cnf.at("C")
	)
	{}
  protected:
	virtual double get_value(double r) const
	{
		using std::pow;
		using std::exp;
		return m_epsilon*( exp(-2*m_beta*(r-m_re))-2*exp(-m_beta*(r-m_re)) );
	}
	virtual double get_dVdr(double r) const
	{
		using std::pow;
		using std::exp;
		return 2*m_beta*m_epsilon*( -exp(-2*m_beta*(r-m_re))+exp(-m_beta*(r-m_re)) );
	}
	virtual double get_d2Vdr2(double r) const
	{
		using std::pow;
		using std::exp;
		return 2*m_beta*m_beta*m_epsilon*( 2*exp(-2*m_beta*(r-m_re))-exp(-m_beta*(r-m_re)) );
	}
  private:
	const double m_epsilon;
	const double m_re;
	const double m_sigma;
	const double m_C;
	const double m_beta;
};

/** The repulsive screened coulomb potential.
 *
 *  \todo Provide a reference to a source that has a discussion and
 *        the specific form implemented here.
 */
class RSCP : public potential
{
  public:
	//constructor
	RSCP(double epsilon,double rho)
	 : m_epsilon(epsilon),m_rho(rho)
	{}
	RSCP(const json_type& cnf)
	 : RSCP(
		in_si(cnf.at("epsilon"),units::energy),
		in_si(cnf.at("rho"),units::length)
	)
	{}
  protected:
	virtual double get_value(double r) const
	{
		using std::exp;
		return m_epsilon*exp(-r/m_rho)/r*m_rho;
	}
	virtual double get_dVdr(double r) const
	{
		using std::exp;
		return m_epsilon*exp(-r/m_rho)*(-1.0/r-m_rho/std::pow(r,2));
	}
	virtual double get_d2Vdr2(double r) const
	{
		using std::exp;
		return m_epsilon*exp(-r/m_rho)*(1.0/r/m_rho+2.0/std::pow(r,2)+2.0*m_rho/std::pow(r,3));
	}
  private:
	const double m_epsilon;
	const double m_rho;
};

/** The FiniteWell potential.
 *
 *  piecewise constant potential with a well near the origin
 *
 *  \todo Provide a reference to a source that has a discussion and
 *        the specific form implemented here.
 *  \todo Explain the sign-handling on the expression for the first derivative
 */
class FiniteWell : public potential
{
public:
	FiniteWell(double r_s,double V_s)
	 : m_r_s(r_s), m_V_s(V_s)
	{}
	FiniteWell(const json_type& cnf)
	 : FiniteWell(
		in_si(cnf.at("r_s"),units::length),
		in_si(cnf.at("V_s"),units::energy)
	)
	{}
protected:
	virtual double get_value(double r) const
	{
		return (r<m_r_s) ? m_V_s : 0;
	}
	virtual double get_dVdr(double r) const
	{
		if(std::abs(r-m_r_s)/m_r_s>1e-10)
		{
			return 0;
		}
		else
		{
			if(m_V_s!=0)
			{
				return (m_V_s>0) ?
					-std::numeric_limits<double>::infinity()
				: 	+std::numeric_limits<double>::infinity();
			}
			else
			{
				return 0;
			}
		}
	}
	virtual double get_d2Vdr2(double r) const
	{
		if(std::abs(r-m_r_s)/m_r_s>1e-10)
		{
			return 0;
		}
		else
		{
			return std::numeric_limits<double>::signaling_NaN();
		}
	}
private:
	const double m_r_s;
	const double m_V_s;
};

/** A potential that is based on a lookup table
 *
 *
 *  \todo explain how interpolation is done
 *  \todo Explain what happens for r outside the table range
 *  \todo explain the expected table format
 *
 *  expect [rmin rmax c3 c2 c1 c0]. V=c3*(r-rmin)^3+c2*(r-rmin)^2+c1*(r-rmin)+c0
 */
class LUT: public potential
{
  public:
	LUT(int nlong, const std::string& filename);
	LUT(const json_type& cnf);
	void write_potential(double rstep) const;
  protected:
	virtual double get_value(double r) const
	{
		using std::pow;
		if (r<m_rmin)
		{
			//V=V1*(r1/r)^Nshort
			return m_V0*pow(m_rmin/r,m_Nshort);
		}
		else if (r>=m_rmax)
		{
			//V=-Clong/r^Nlong
			return -m_Clong/pow(r,m_Nlong);
		}
		else
		{
			//use piecewise cubic polynomial fit:
			return m_spline_fit(r);
		}
	}
	virtual double get_dVdr(double r) const
	{
		using std::pow;
		if (r<m_rmin)
		{
			//V=V1*(r1/r)^Nshort
			return -m_Nshort*m_V0*pow(m_rmin/r,m_Nshort)/r;
		}
		else if (r>=m_rmax)
		{
			//V=-Clong/r^Nlong
			return m_Nlong*m_Clong/pow(r,m_Nlong+1);
		}
		else
		{
			//use piecewise cubic polynomial fit:
			return m_spline_fit.deriv1(r);
		}
	}
	virtual double get_d2Vdr2(double r) const
	{
		using std::pow;
		if (r<m_rmin)
		{
			//V=V1*(r1/r)^Nshort
			return m_Nshort*(m_Nshort+1.0)*m_V0*pow(m_rmin/r,m_Nshort)/pow(r,2);
		}
		else if (r>=m_rmax)
		{
			//V=-Clong/r^Nlong
			return -m_Nlong*(m_Nlong+1.0)*m_Clong/pow(r,m_Nlong+2.0);
		}
		else
		{
			//use piecewise cubic polynomial fit:
			return m_spline_fit.deriv2(r);
		}
	}
  private:
	const int m_Nlong;
	double m_Clong;
	double m_Nshort;
	double m_rmin;
	double m_rmax;
	double m_V0;
	tk::spline m_spline_fit;
};

LUT::LUT(const json_type& cnf)
 : m_Nlong(cnf.at("is_neutral").get<bool>() ? 6 : 4)
{
	std::vector<double> r,V;
	if (cnf.contains("lookup_table_xy"))
	{
		const json_type lut = cnf.at("lookup_table_xy");
		const double r_scale = units::unit_in_si(lut.at("abscissa").at("unit"),units::length);
		const double V_scale = units::unit_in_si(lut.at("ordinate").at("unit"),units::energy);
		const std::vector<std::vector<double>> rV = lut.at("data");
		r.reserve(rV.size());
		V.reserve(rV.size());
		for (const auto& p : rV)
		{
			if (p.size()!=2)
			{
				throw std::runtime_error("Bad number of items in V(r) lookup table, data line "
					+ std::to_string(r.size())
					+ ": expected 2, got " + std::to_string(p.size()) + ".");
			}
			r.push_back(p[0]*r_scale);
			V.push_back(p[1]*V_scale);
		}
	}
	else
	{
		std::string filename = plasimo::substitute_env_vars_copy(cnf.at("file"),true);
		const std::string fileuri_prefix = "file://";
		if (filename.substr(0,fileuri_prefix.size())==fileuri_prefix)
		{
			std::cout << "Stripped " << fileuri_prefix << " from file name '"
				<< filename << "'." << std::endl;
			filename = filename.substr(fileuri_prefix.size());
		}
		std::clog << "Loading LUT table from file:" << filename << std::endl;
		if (filename.find(".json")!=std::string::npos)
		{
			const json_type pot_file(read_json_from_file(filename));
			const json_type& lut = pot_file.at("lookup_table_xy");
			const double r_scale = units::unit_in_si(lut.at("abscissa").at("unit"),units::length);
			const double V_scale = units::unit_in_si(lut.at("ordinate").at("unit"),units::energy);
			const std::vector<std::vector<double>> rV = lut.at("data");
			r.reserve(rV.size());
			V.reserve(rV.size());
			for (const auto& p : rV)
			{
				if (p.size()!=2)
				{
					throw std::runtime_error("Bad number of items in V(r) lookup table, data line "
						+ std::to_string(r.size())
						+ ": expected 2, got " + std::to_string(p.size()) + ".");
				}
				r.push_back(p[0]*r_scale);
				V.push_back(p[1]*V_scale);
			}
		}
		else
		{
			std::clog << "Expecting V in eV and r in m" << std::endl;
			std::ifstream is(filename);
			if (!is)
			{
				throw std::runtime_error("Could not open file '" + filename + "' for reading.");
			}
			double tmp_r=0,tmp_E_eV=0;
			while (is >> tmp_r)
			{
				// if we read an r, we also expect a V.
				if (!(is >> tmp_E_eV))
				{
					throw std::runtime_error("Bad data in '" + filename + "', data line "
						+ std::to_string(r.size()) + ".");
				}
				// add to data structure, convert potential energy to J
				r.push_back(tmp_r);
				V.push_back(tmp_E_eV * Constant::eV);
			}
			if (!is.eof())
			{
				throw std::runtime_error("Bad data in '" + filename + "', data line "
					+ std::to_string(r.size()) + ".");
			}
		}
	}
	if (r.size()<2)
	{
		throw std::runtime_error("At least two potential data points must be provided.");
	}
	for (std::size_t i=1; i!=r.size(); ++i)
	{
		if (r[i]<=r[i-1])
		{
			throw std::runtime_error("Potential data must be provided in ascending r-order.");
		}
	}
	//make spline fit
	m_spline_fit.set_points(r,V);
	//set potential parameters
	m_V0=V[0];
	m_rmin=r[0];
	m_rmax=r[r.size()-1];
	//determine Nshort
	m_Nshort=std::log( V[1]/m_V0 )/std::log( m_rmin/r[1]);
	//determine Clong=-V_last *r_last^Nlong
	m_Clong=-V[V.size()-1]*pow(m_rmax,m_Nlong);

	//display fit results
	std::clog << "Nshort\t" << m_Nshort << std::endl;
	std::clog << "Clong\t" << m_Clong/Constant::eV << " eV m^" << m_Nlong << std::endl;
}

void LUT::write_potential(double rstep) const
{
	std::string filename = "potential.txt";
	std::ofstream os(filename);
	if (!os)
	{
		throw std::runtime_error("Could not open file '" + filename + "' for writing.");
	}
	double r = m_rmin;
	while (r < m_rmax)
	{
		os << r << "\t" << value(r) << "\t" << dVdr(r) << "\t" << d2Vdr2(r) << std::endl;
		r += rstep;
	}
}

/** A potential that is based on a lookup table. This version assumes
 *  an experimental JSON-based LXCat file format that is not yet used
 *  or even endorsed by the LXCat team. This format may change without
 *  prior warning. Use this at your own risk.
 *
 *  \todo explain how interpolation is done
 *  \todo Explain what happens for r outside the table range
 *  \todo explain the expected table format
 */
class LXCatJSON : public potential
{
	public:
		LXCatJSON(const json_type& cnf);
	protected:
		virtual double get_value(double r) const
		{
			using std::pow;
			if (r<m_rmin)
			{
				//V=V1*(r1/r)^Nshort
				return m_V0*pow(m_rmin/r,m_Nshort);
			}
			else if (r>=m_rmax)
			{
				//V=-Clong/r^Nlong
				return -m_Clong/pow(r,m_Nlong);
			}
			else
			{
				//use piecewise cubic polynomial fit:
				return m_spline_fit(r);
			}
		}
		virtual double get_dVdr(double r) const
		{
			using std::pow;
			if (r<m_rmin)
			{
				//V=V1*(r1/r)^Nshort
				return -m_Nshort*m_V0*pow(m_rmin/r,m_Nshort)/r;
			}
			else if (r>=m_rmax)
			{
				//V=-Clong/r^Nlong
				return m_Nlong*m_Clong/pow(r,m_Nlong+1);
			}
			else
			{
				//use piecewise cubic polynomial fit:
				return m_spline_fit.deriv1(r);
			}
		}
		virtual double get_d2Vdr2(double r) const
		{
			using std::pow;
			if (r<m_rmin)
			{
				//V=V1*(r1/r)^Nshort
				return m_Nshort*(m_Nshort+1.0)*m_V0*pow(m_rmin/r,m_Nshort)/pow(r,2);
			}
			else if (r>=m_rmax)
			{
				//V=-Clong/r^Nlong
				return -m_Nlong*(m_Nlong+1.0)*m_Clong/pow(r,m_Nlong+2.0);
			}
			else
			{
				//use piecewise cubic polynomial fit:
				return m_spline_fit.deriv2(r);
			}
		}
	private:
		int m_Nlong;
		double m_Clong;
		double m_Nshort;
		double m_rmin;
		double m_rmax;
		double m_V0;
		tk::spline m_spline_fit;
};

LXCatJSON::LXCatJSON(const json_type& cnf)
{
	// Potential must have type key to determine type, but lxcat json document does not
	// So lxcat document is inlined as
	// {"type":"LXCatJSON", "record": {... LXCat JSON document ...}}
	// or externally
	// {"type":"LXCatJSON", "file": "Path to LXCatJSON document"}
	json_type record;
	if (cnf.contains("record"))
	{
		// inlined LXCatJSON document
		record = cnf["record"];
	}
	else if (cnf.contains("file"))
	{
		// external LXCatJSON document
		const std::string filename = cnf["file"].get<std::string>();
		record = read_json_from_file(filename);
	}
	else
	{
		throw std::runtime_error("Missing record field with inlined LXCatJSON document or file field with path to LXCatJSON document.");	;
	}

	// TODO dont always use first process
	const json_type& lut = record["Database"]["Groups"]["Group"]["Processes"][0]["Values"];

	// LXCat for the moment only contains ion-neutral interactions
	// TODO infer ion-ion or ion-neutral or neutral-neutral interaction based on species.
	// or add a field to the LXCat JSON document schema for it
	m_Nlong = 4;

	// TODO check label of axes
	if (lut["Axes"].size() != 2) {
		throw std::runtime_error("Bad number of axes: expected 2, got " + std::to_string(lut["Axes"].size()) + ".");
	}
	const std::string xaxislabel = lut["Axes"][0]["label"].get<std::string>();
	if (xaxislabel != "r") {
		throw std::runtime_error("Bad first axis label, expected 'r' got " + xaxislabel);
	}
	const double r_scale = units::unit_in_si(lut["Axes"][0]["unit"].get<std::string>(),units::length);
	const std::string yaxislabel = lut["Axes"][1]["label"].get<std::string>();
	if (yaxislabel != "V") {
		throw std::runtime_error("Bad second axis label, expected 'V' got " + yaxislabel);
	}
	const double V_scale = units::unit_in_si(lut["Axes"][1]["unit"].get<std::string>(),units::energy);
	const std::vector<std::vector<double>> rV = lut["Data"];
	std::vector<double> r,V;
	r.reserve(rV.size());
	V.reserve(rV.size());
	for (const auto& p : rV)
	{
		if (p.size()!=2)
		{
			throw std::runtime_error("Bad number of items in V(r) lookup table, data line "
				+ std::to_string(r.size())
				+ ": expected 2, got " + std::to_string(p.size()) + ".");
		}
		r.push_back(p[0]*r_scale);
		V.push_back(p[1]*V_scale);
	}
	if (r.size()<2)
	{
		throw std::runtime_error("At least two potential data points must be provided.");
	}
	for (std::size_t i=1; i!=r.size(); ++i)
	{
		if (r[i]<=r[i-1])
		{
			throw std::runtime_error("Potential data must be provided in ascending r-order.");
		}
	}
	//make spline fit
	m_spline_fit.set_points(r,V);
	//set potential parameters
	m_V0=V[0];
	m_rmin=r[0];
	m_rmax=r[r.size()-1];
	//determine Nshort
	m_Nshort=std::log( V[1]/m_V0 )/std::log( m_rmin/r[1]);
	//determine Clong=-V_last *r_last^Nlong
	m_Clong=-V[V.size()-1]*pow(m_rmax,m_Nlong);

	//display fit results
	std::clog << "Nshort\t" << m_Nshort << std::endl;
	std::clog << "Clong\t" << m_Clong/Constant::eV << " eV m^" << m_Nlong << std::endl;
}

std::unique_ptr<const potential> potential::create(const json_type& cnf)
{
	if (cnf.contains("query"))
	{
                const json_type& q = cnf.at("query");
		std::cout << "magnumpi::potential: found a query section. Will "
			"fetch the potential information from elsewhere. Query:\n"
			<< q.dump(2) << std::endl;
                json_type reply = post_request(
                        q.at("host").get<std::string>(),
                        q.at("port").get<std::string>(),
                        q.at("target").get<std::string>(),
                        q.at("body").dump()
                );
		if (reply["meta"]["result"]!=200)
		{
			throw std::runtime_error("Error fetching potential, error = "
				+ std::to_string(reply["meta"]["result"].get<int>()));
		}
                /** \todo (Optionally) do something with the rest of the
                 *  "meta" section, reply["meta"]["header"].
                 */
                std::cout << "magnumpi: query result\n" << reply.dump(2) << std::endl;

                std::cout << "magnumpi: patching the query result." << std::endl;
		// some patchwork is required. We assume a LUT is returned.
                json_type sec;
		if (reply.at("body").size()!=1)
		{
			throw std::runtime_error("Query did not result in a unique potential section, I received " + std::to_string(reply.at("body").size()) + ".");
		}
		/** \todo some patchwork is still needed for now:
		 *  Later we want to be able to say just
		 *  sec = reply.at("body")[0];
		 */
		if (reply.at("body")[0].at("type")=="LUT")
		{
		 	/* LUT's have the data in the section itsels, but not exactly
		 	 * in the magnumpi form.
		 	 */
			/** \todo type and is_neutral should come from elsewhere
			 */
			sec["is_neutral"] = cnf["is_neutral"];
			sec["lookup_table_xy"] = reply.at("body")[0];
			sec["type"] = reply.at("body")[0].at("type");
		}
		else
		{
			/* All other types gave all that is needed in data/,
			 * except for the type, which is in the section itself.
			 * (Also meta-information is in the section itself, such
			 * as annotations etc.
			 */
			sec = reply.at("body")[0].at("data");
			sec["type"] = reply.at("body")[0].at("type");
		}
		// recurse, use sec as the new cnf
                std::cout << "magnumpi: creating a potential object from the query result." << std::endl;
		return create(sec);
	}
	const std::string type(cnf.at("type").get<std::string>());
	if (type=="BuckinghamCorner")
	{
		return std::make_unique<BuckinghamCorner>(cnf);
	}
	else if (type=="ESMSV")
	{
		return std::make_unique<ESMSV>(cnf);
	}
	else if (type=="ExponentialRepulsive")
	{
		return std::make_unique<ExponentialRepulsive>(cnf);
	}
	else if (type=="FiniteWell")
	{
		return std::make_unique<FiniteWell>(cnf);
	}
	else if (type=="HardSphere")
	{
		return std::make_unique<HardSphere>(cnf);
	}
	else if (type=="HulburtHirschfelder")
	{
		return std::make_unique<HulburtHirschfelder>(cnf);
	}
	else if (type=="LennardJones")
	{
		return std::make_unique<LennardJones>(cnf);
	}
	else if (type=="LennardJonesLaricchiuta")
	{
		return std::make_unique<LennardJonesLaricchiuta>(cnf);
	}
	else if (type=="LUT")
	{
		return std::make_unique<LUT>(cnf);
	}
	else if (type=="Morse")
	{
		return std::make_unique<Morse>(cnf);
	}
	else if (type=="MS_12_4")
	{
		return std::make_unique<MS_12_4>(cnf);
	}
	else if (type=="RSCP")
	{
		return std::make_unique<RSCP>(cnf);
	}
	else if (type=="LXCatJSON")
	{
		return std::make_unique<LXCatJSON>(cnf);
	}
	else
	{
		throw std::runtime_error("Unknown potential model '" + type + "'.");
	}
}


} // namespace magnumpi
