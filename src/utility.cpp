/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/utility.h"
#include "magnumpi/json.h"
#include <cmath>

namespace magnumpi {

std::vector<double> make_samples_lin(double low, double high, unsigned n)
{
	const double step = (high-low)/(n-1);
	std::vector<double> res(n);
	for (unsigned i=0; i!=res.size();++i)
	{
		res[i]=low+i*step;
	}
	return res;
}

std::vector<double> make_samples_log(double log_low, double log_high, unsigned n_decade)
{
	const unsigned n=std::round((log_high-log_low)*n_decade+1);
	const double step=1.0/n_decade;
	std::vector<double> res(n);
	for (unsigned i=0;i!=res.size();++i)
	{
		res[i]=std::pow(10,log_low+i*step);
	}
	return res;
}

std::vector<double> make_range(
	double first,
	double last,
	std::vector<double>::size_type n,
	bool log_spacing)
{
	using vector_type = std::vector<double>;
	if (n<2)
	{
		throw std::runtime_error("range: steps must be at least 2.");
	}
	vector_type res(n);
	if (log_spacing)
	{
		if (first==0.0 || last==0.0)
		{
			throw std::runtime_error("range: first and last must "
				"be non-zero for a logarithmic spacing.");
		}
		const double last_first = last/first;
		if (last_first<=0.0)
		{
			throw std::runtime_error("range: first and last must "
				"have the same sign for a logarithmic spacing.");
		}
		const double ldel = std::log(last_first)/(n-1);
		for (vector_type::size_type i=0; i!=n-1; ++i)
		{
			res[i] = first*std::exp(i*ldel);
		}
		// do the last one separately to avoid round of errors at the
		// and of the range
		res[n-1] = last;
	}
	else
	{
		const double del = (last-first)/(n-1);
		for (vector_type::size_type i=0; i!=n-1; ++i)
		{
			res[i] = first + i*del;
		}
		// do the last one separately to avoid round of errors at the
		// and of the range
		res[n-1] = last;
	}
	return res;
}

std::vector<double> make_range(const json_type& cnf, units::dimension d)
{
	const double first = in_si(cnf.at("first"),d);
	const double last = in_si(cnf.at("last"),d);
	const std::vector<double>::size_type n = cnf.at("steps");
	const std::string type = cnf.at("type");
	if (type=="linear")
	{
		return make_range(first,last,n,false);
	}
	else if (type=="logarithmic")
	{
		return make_range(first,last,n,true);
	}
	else
	{
		throw std::runtime_error("range: type must be 'linear' "
			"or 'logarithmic'.");
	}
}


} // namespace magnumpi

