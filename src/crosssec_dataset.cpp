/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/crosssec_dataset.h"
#include "magnumpi/interpolation.h"
#include "magnumpi/consts.h"
#include <cmath>
#include <stdexcept>

namespace magnumpi {

cross_section_from_dataset_log::cross_section_from_dataset_log(const data_set& Q)
 : m_log_energy(Q.row_axis().size()),
   m_log_Q(Q.row_axis().size(),std::vector<double>(Q.col_axis().size())),
   m_l_max(Q.col_axis().size())
{
	//calculate log of Q
	for(unsigned i=0;i!=Q.row_axis().size();++i)
	{
		m_log_energy[i]=std::log10(Q.row_axis().value(i));
		for(unsigned j=0;j!=Q.col_axis().size();++j)
		{
			m_log_Q[i][j]=std::log10(Q(i,j));
		}
	}
}

cross_section_from_dataset_log::cross_section_from_dataset_log(const json_type& Q)
 : m_l_max(Q.at("column_axis").at("labels").size())
{
	// set up the energy data
	const json_type& energy_data = Q.at("row_axis").at("data");
	m_log_energy.resize(energy_data.at("values").size());
	const double unit_eps_J = units::unit_in_si(energy_data.at("unit"),units::energy);
	// convert the inergy values in SI unit J and take the log10
	for (dvector::size_type e=0; e!=energy_data.at("values").size(); ++e)
	{
		m_log_energy[e]=std::log10(energy_data.at("values")[e].get<double>()*unit_eps_J);
	}

	// set up the column labels (should be string values representing l)
	const json_type& column_labels = Q.at("column_axis").at("labels");
	const std::size_t nl = column_labels.size();

	// retrieve the value of the cross section unit, expressed in SI unit m^2.
	const double unit_sig_m2 = units::unit_in_si(Q.at("unit"),units::area);

	// resize the data object. It will contain log10(Q_l(energy)).
	// This is a vector of vectors of doubles. Every element m_log_Q[e]
	// contains the values of Q_l(energy_e) for j=1...,nl (it has length nl),
	// m_log_Q itself has the same size as m_log_energy.size().
	m_log_Q.resize(m_log_energy.size(),dvector(nl));
	const json_type& rows = Q.at("columns");
	//calculate log of Q
	for(std::size_t e=0; e!=m_log_energy.size(); ++e)
	{
		const json_type& Qe = rows[e];
		for(unsigned l=0;l!=nl;++l)
		{
			// note the transposition. In the file, Q's are stored
			// per l-value, in the code per energy value.
			const double Q = Qe[l].get<double>()*unit_sig_m2;
			m_log_Q[e][l]=std::log10(Q);
		}
	}
}

double cross_section_from_dataset_log::get_value(unsigned l, double eps) const
{
	double log_E=std::log10(eps);
	double log_CS;
	single_linear_interp1(m_log_energy,m_log_Q,log_E,&log_CS,l-1 );
	double CS=std::pow(10,log_CS);
	return CS;
}

bool cross_section_from_dataset_log::get_supports_l(unsigned l) const
{
	return l!=0 && l<=m_l_max;
}

} // namespace magnumpi
