/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/clencurt.h"
#include "magnumpi/potential.h"
#include "magnumpi/calc_cs.h"
#include "magnumpi/consts.h"
#include "magnumpi/scat_colonna.h"
#include "magnumpi/scat_viehland.h"
#include "magnumpi/utility.h"
#include <vector>
#include <cmath>
#include <string>
#include <iostream>

namespace magnumpi {

/** Case 1 matches \cite Viehland2010 section 5.1
 */
class calc_cs::Viehland_case_1_sub
{
public:
	using result_type = double;
	using argument_type = double;
	Viehland_case_1_sub(const calc_cs& cs, double E, const triplet_data::case_data& data);
	void operator()(double y,std::vector<double>& h) const;
private:
	const calc_cs& m_cs;
	const double m_energy;
	const triplet_data::case_data& m_data;
};

calc_cs::Viehland_case_1_sub::Viehland_case_1_sub(const calc_cs& cs, double E, const triplet_data::case_data& data)
 : m_cs(cs), m_energy(E), m_data(data)
{
#if 0
	//write orbiting parameters
	for (unsigned i=0;i<m_b_vec.size();++i)
	{
		const double b = m_data.b_vec[i];
		const double r = m_data.r_vec[i];
		std::cout << "b\t" << b
			<< "\tr\t" << r
			//<< "\t" << scat_Col_get_r(b,m_energy,m_cs.m_tolerance,m_cs.interp)
			<< "\t" << (1.0-pow(b/r,2)-m_cs.m_pot.value(r)/m_energy)
			<< std::endl;
	}
#endif

}

void calc_cs::Viehland_case_1_sub::operator()(double y, std::vector<double>& h) const
{
	// for brevity of the implementation below:
	const std::vector<double>& b_vec = m_data.b_vec;
	const std::vector<double>& r_vec = m_data.r_vec;

	//*************************PART 1**********************************
	//calculate impact parameter and scattering angle for part 1 of case 1
	if (y>-1 && y<1)
	{
		//discretization of impact parameter
		// equation 21
		double b = b_vec[0] *cos(Constant::pi *(y+1.0)/4.0);
		double scat;
		if (m_cs.m_use_scat_Colonna==1)
		{
			scat=scat_Colonna(&m_cs.m_pot,b,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			//use last found r0 as a lower boundary
			//initialize with lowest r0 value: *(rvec+0)
			//note: this also adjusts b
			const double r0=scat_viehland::find_r0_case_1(m_cs.m_pot,m_energy,&b,r_vec[0]);
			scat=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b,r0,m_cs.m_tolerance);
		}

		//add contribution to output
		for (unsigned l=1; l<=m_cs.m_l_max;++l)
		{
			// equation 23
			h[l-1]=pow(Constant::pi,2)/4.0*pow(b_vec[0],2)*(1.0-pow(cos(scat),l))*sin(Constant::pi*(y+1.0)/2.0);
		}
	}
	else
	{
		//assign zeros
		for (unsigned i=0;i!=h.size();++i)
		{
			h[i]=0.0;
		}
	}
	//**************************PART 2*********************************
	//part 2: multiple orbiting b_i<=b<=b_{i+1}, eq 24-36
	/** \todo Remove this 'if'. We already now that b.size()>=1,
	 *  and if it is 1 the j-loop below will never be visited.
	 */
	if (b_vec.size()>1)
	{
		for (unsigned j=0;j!=b_vec.size()-1;j++)
		{
			// average radius, eq 26
			double r_avr=(r_vec[j]+r_vec[j+1])/2.0;
			/* The integration in the r-domain (equation 25) is
			 * split in parts [r_i,r_avg] and [r_avg,r_{i+1}].
			 * For ech interval a transformation is done that maps
			 * that interval to y in [-1,1]. The result is that
			 * this integrand consists of two terms (see eqn 34),
			 * each representing one of these pieces. In the special
			 * cases of y==-1 and y==1, only one term is present,
			 * as expressed by eqns. 35 and 36, respectively.
			 *
			 * See how to write this in a way more similar to Viehland,
			 * but it would also be nice to merge the two paths.
			 */
			/** \todo Is equation 35 the same as the first term
			 *  of eqn 34, when evaluated for y==-1? Is it just
			 *  an optimization to not handle the second term
			 *  (because it is zero), or does that avoid some
			 *  numerical issue?
			 */
			if (y<1)
			{
				double xi,r1,x1;
				//part r1
				/** \todo the expression for xi is the same in the if and else,
				 *  move that here.
				 */
				if (y==-1)
				{
					/// \todo make the order the same as in the else: xi, x1, r1
					r1=r_vec[j];
					xi=4.0/Constant::pi*asin(r_vec[j]/r_avr)-1.0;
					x1=xi;
				}
				else
				{
					// equation 29
					xi=4.0/Constant::pi*asin(r_vec[j]/r_avr)-1.0;
					// equation 28
					x1=( (1.0-xi)*y+xi+1.0)/2.0;
					// equation 27
					r1=r_avr*sin(Constant::pi/4.0*(x1+1.0));
					/** \todo Here we calculate a sine, below (eq 34)
					 *  we need the sin of twice that angle. See if
					 *  this can be optimized. (Then we may also not
					 *  need to keep x1, except for the calculation of
					 *  that sine.)
					 */
				}
				// equation 24
				const double b=r1*sqrt(1.0-m_cs.m_pot.value(r1)/m_energy);
				/// \todo Why is m_cs.m_use_scat_Colonna==0 not supported here?
				double scat;

				//if (m_cs.m_use_scat_Colonna==1)
				//{
					scat=scat_Colonna(&m_cs.m_pot,b,m_energy,m_cs.m_tolerance,m_cs.m_interp);
				//}
				//else
				//{
				//adjust_b(m_energy,&b,r1);
						//std::cout << y << "\t" << r1 << "\t" << b << "\t"
				//	<< m_cs.m_pot.value(r1)<< "\t" << 1.0-pow(b/r1,2) - m_cs.m_pot.value(r1)/m_energy << std::endl;
				//scat=m_cs.m_scat_viehland.scat2(m_cs.m_pot,m_energy,b,r_avr,r1,coeff);
				//scat=m_cs.m_scat_viehland.scat1(m_cs.m_pot,m_energy,b,r1,coeff);
				//}

				const double dVdr=m_cs.m_pot.dVdr(r1);

				for (unsigned l=1; l<=m_cs.m_l_max;++l)
				{
					// euation 34 (first term)
					h[l-1] += pow(Constant::pi,2)/8.0*pow(r_avr,2)*(1.0-xi)
					*(1.0-pow(cos(scat),l))
					*(1.0-m_cs.m_pot.value(r1)/m_energy-r1/2.0*dVdr/m_energy)
					*sin(Constant::pi*(1.0+x1)/2.0);
					/*
					//check whether number is positive
					if (1.0-m_cs.m_pot.value(r1)/m_energy-r1/2.0*dVdr/m_energy < 0)
					{
						std::cout << "case 2 1" << y << "\t" << 1.0-m_cs.m_pot.value(r1)/m_energy-r1/2.0*dVdr/m_energy << std::endl;
					}
					if ( isnan(*(f_out+l-1))==1 )
					{
						std::cout <<y << "\t" << scat << "\t" << r1 << "\t" << 1.0-pow(b/r1,2)-m_cs.m_pot.value(r1)/m_energy << "\t" << *(f_out+l-1) << std::endl;
					}
					*/
				}
			}

			/// \todo See the comments on 'if (y<1)' above.
			if (y>-1)
			{
				double xi,x2,r2;
				if (y==1)
				{
					r2=r_vec[j+1];
					xi=4.0/Constant::pi*acos(r_avr/ r_vec[j+1])-1.0;
					x2=xi;
				}
				else
				{
					// equation 32
					xi=4.0/Constant::pi*acos(r_avr/ r_vec[j+1])-1.0;
					// equation 31
					x2=( (1.0+xi)*y+xi-1.0)/2.0;
					// equation 30
					r2=r_vec[j+1]*cos(Constant::pi/4.0*(x2+1.0));
				}
				const double b=r2*sqrt(1.0-m_cs.m_pot.value(r2)/m_energy);
				/// \todo Why is m_cs.m_use_scat_Colonna==0 not supported here?
				double scat;

				//if (m_cs.m_use_scat_Colonna==1)
				//{
					scat=scat_Colonna(&m_cs.m_pot,b,m_energy,m_cs.m_tolerance,m_cs.m_interp);
				//}
				//else
				//{
				//	adjust_b(m_energy,&b,r2);
				//std::cout << y << "\t" << r2 << "\t" << b << "\t" << 1.0-pow(b/r2,2) - m_cs.m_pot.value(r2)/m_energy << std::endl;
				//	scat=m_cs.m_scat_viehland.scat1(m_cs.m_pot,m_energy,b,r2,coeff);
				//}

				const double dVdr=m_cs.m_pot.dVdr(r2);

				for (unsigned l=1; l<=m_cs.m_l_max;++l)
				{
					// euation 34 (second term)
					h[l-1] += pow(Constant::pi,2)/8.0*pow(r_vec[j+1],2)*(1.0+xi)
					*(1.0-pow(cos(scat),l))
					*(1.0-m_cs.m_pot.value(r2)/m_energy-r2/2.0*dVdr/m_energy)
					*sin(Constant::pi*(1.0+x2)/2.0);

					/*
					//check whether number is positive
					if (1.0-m_cs.m_pot.value(r2)/m_energy-r2/2.0*dVdr/m_energy < 0)
					{
						std::cout << "case 2 2" << y << "\t" << 1.0-m_cs.m_pot.value(r2)/m_energy-r2/2.0*dVdr/m_energy << std::endl;
					}
					// */
				}
			}
		}
	}
	//**********************PART 3******************************************
	//part 3: b_last<=b<=infinity, eq 37-46
	if (y>-1 && y<1)
	{
		// eqn. 41
		const double x3=(1+y)/2;
		// eqn. 39:
		double r3=r_vec[r_vec.size()-1]/sin((Constant::pi/2.0)*x3);
		double b=r3*sqrt(1.0-m_cs.m_pot.value(r3)/m_energy);
		double scat;

		if (m_cs.m_use_scat_Colonna==1)
		{
			scat=scat_Colonna(&m_cs.m_pot,b,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			/** \todo this appears to be eqn. 38. But the comment of adjust_b says
			 *  that that implements eqn. 24. Is that the same function, just called
			 *  with different parameters?
			 */
			adjust_b(m_cs.m_pot,m_energy,&b,r3);
			scat=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b,r3,m_cs.m_tolerance);
		}
		const double dVdr=m_cs.m_pot.dVdr(r3);
		for (unsigned l=1; l<=m_cs.m_l_max;++l)
		{
			// equation 43
			if (1.0-m_cs.m_pot.value(r3)/m_energy-r3/2.0*dVdr/m_energy >= 0)
			{
				h[l-1] += pow(Constant::pi,2)/2.0*pow(r_vec[r_vec.size()-1],2)
				*(1.0-pow(cos(scat),l))
				*(1.0-m_cs.m_pot.value(r3)/m_energy-r3/2.0*dVdr/m_energy)
				*pow(r3/r_vec[r_vec.size()-1],3)
				*cos(Constant::pi*(1.0+y)/4.0);
			}

			/*
			//check whether number is positive
			if (1.0-m_cs.m_pot.value(r3)/m_energy-r3/2.0*dVdr/m_energy < 0 )
			{
				std::cout << "case 3 " << y << "\t" << r3 << "\t" << 1.0-m_cs.m_pot.value(r3)/m_energy-r3/2.0*dVdr/m_energy
					<< "\t" << 1.0-pow(b/r3,2)-m_cs.m_pot.value(r3)/m_energy << std::endl;
			}
			// */
		}
	}
}

class calc_cs::Viehland_case_2_sub
{
public:
	using result_type = double;
	using argument_type = double;
	Viehland_case_2_sub(const calc_cs& cs, double E, const triplet_data::case_data& data);
	void operator()(double y,std::vector<double>& h) const;
private:
	const calc_cs& m_cs;
	const double m_energy;
	const triplet_data::case_data& m_data;
};

calc_cs::Viehland_case_2_sub::Viehland_case_2_sub(const calc_cs& cs, double E, const triplet_data::case_data& data)
 : m_cs(cs), m_energy(E), m_data(data)
{
}

void calc_cs::Viehland_case_2_sub::operator()(double y, std::vector<double>& h) const
{
	const double rc_tilde = m_data.rc_tilde;
	const double r0_tilde = m_data.r0_tilde;

	//***************PART 1*************************************************
	if (y>-1)
	{
		//discretization of the distance of closest approach
		const double r4=((rc_tilde-r0_tilde)*y+rc_tilde+r0_tilde)/2.0;
		double b4=r4*sqrt(1.0-m_cs.m_pot.value(r4)/m_energy);

		double scat;
		if (m_cs.m_use_scat_Colonna==1)
		{
			scat=scat_Colonna(&m_cs.m_pot,b4,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			adjust_b(m_cs.m_pot,m_energy,&b4,r4);
			scat=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b4,r4,m_cs.m_tolerance);
		}
		const double dVdr=m_cs.m_pot.dVdr(r4);

		for (unsigned l=1;l<=m_cs.m_l_max;l++)
		{
			//eq 53
			h[l-1] = Constant::pi*(rc_tilde-r0_tilde)*(1.0-pow(cos(scat),l))*
				(1.0-m_cs.m_pot.value(r4)/m_energy-r4/2.0*dVdr/m_energy)*r4;
		}
	}
	else
	{
		const double r4=r0_tilde;
		const double dVdr=m_cs.m_pot.dVdr(r4);
		/** \todo See how to present this. scat is unused because cos(scat)=-1
		 *        is used directly in the code below.
		 */
		//const double scat=Constant::pi;
		for (unsigned l=1;l<=m_cs.m_l_max;l++)
		{
			//eq 54
			h[l-1] = Constant::pi*r4*(rc_tilde-r0_tilde)*(1.0-pow(-1,l))*(-r4/2.0*dVdr/m_energy);
		}
	}
	//************************PART 2************************************
	//part 2: r5, eq 51
	if (y>-1 && y<1)
	{
		const double r5=rc_tilde/sin(Constant::pi*(1.0+y)/4.0);
		double b5=r5*sqrt(1.0-m_cs.m_pot.value(r5)/m_energy);
		double scat;
		if (m_cs.m_use_scat_Colonna==1)
		{
			scat=scat_Colonna(&m_cs.m_pot,b5,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			adjust_b(m_cs.m_pot,m_energy,&b5,r5);
			scat=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b5,r5,m_cs.m_tolerance);
		}

		const double dVdr=m_cs.m_pot.dVdr(r5);
		for (unsigned l=1;l<=m_cs.m_l_max;l++)
		{
			//eq 53
			h[l-1] += 0.5*pow(Constant::pi,2)*pow(rc_tilde,2)*(1.0-pow(cos(scat),l))*
				(1.0-m_cs.m_pot.value(r5)/m_energy-r5/2.0*dVdr/m_energy)*pow(r5/rc_tilde,3)*cos(Constant::pi*(1.0+y)/4.0);
		}
	}
}

class calc_cs::Viehland_case_3_sub
{
public:
	using result_type = double;
	using argument_type = double;
	Viehland_case_3_sub(const calc_cs& cs, double E, const triplet_data::case_data& data);
	void operator()(double y,std::vector<double>& h) const;
private:
	const calc_cs& m_cs;
	const double m_energy;
	const triplet_data::case_data& m_data;
};

calc_cs::Viehland_case_3_sub::Viehland_case_3_sub(const calc_cs& cs, double E, const triplet_data::case_data& data)
 : m_cs(cs), m_energy(E), m_data(data)
{
}

void calc_cs::Viehland_case_3_sub::operator()(double y, std::vector<double>& h) const
{
	//*****************************part 1***********************************
	/// \todo move the l-loop out if the if-else; only the scat-calculation is different
	if (y>-1)
	{
		const double b4=m_data.b_split/2.0*(y+1.0);
		double scat;
		if (m_cs.m_use_scat_Colonna==1)
		{
			scat=scat_Colonna(&m_cs.m_pot,b4,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			const double r0=scat_viehland::find_r0_case_3(m_cs.m_pot,m_cs.m_rmin,m_cs.m_rmax,m_energy,&b4);
			scat=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b4,r0,m_cs.m_tolerance);
		}
		for (unsigned l=1; l<=m_cs.m_l_max;++l)
		{
			h[l-1] = Constant::pi/2.0*pow(m_data.b_split,2)*(1.0-pow(cos(scat),l))*(y+1.0);
		}
	}
	else
	{
		const double scat=Constant::pi;
		for (unsigned l=1; l<=m_cs.m_l_max;++l)
		{
			h[l-1] = Constant::pi/2.0*pow(m_data.b_split,2)*(1.0-pow(cos(scat),l))*(y+1.0);
		}
	}
	//******************************PART 2**********************************
	if (y>-1)
	{
		/// \todo merge the two if (y>-1) ... else ... loops? Then also b5=2*b4
		const double b5=2.0*m_data.b_split/(y+1.0);
		double scat;
		if (m_cs.m_use_scat_Colonna==1)
		{
			scat=scat_Colonna(&m_cs.m_pot,b5,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			const double r0=scat_viehland::find_r0_case_3(m_cs.m_pot,m_cs.m_rmin,m_cs.m_rmax,m_energy,&b5);
			scat=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b5,r0,m_cs.m_tolerance);
		}
		for (unsigned l=1; l<=m_cs.m_l_max;++l)
		{
			h[l-1] += Constant::pi*pow(m_data.b_split,2)*(1.0-pow(cos(scat),l))*pow(b5/m_data.b_split,3);
		}
	}
	else
	{
		/** \todo This code does nothing, since scat=0,cos(scat)=1 -> 1.0-pow(cos(scat),l) == 0,
		 *  so the contribution to h is 0.
		 */
		//avoid division by 0 for y=-1
		const double scat=0.0;
		const double b5=0.0; //replace b=Infinite by b=0 => h is zero anyway
		for (unsigned l=1; l<=m_cs.m_l_max;++l)
		{
			h[l-1] += Constant::pi*pow(m_data.b_split,2)*(1.0-pow(cos(scat),l))*pow(b5/m_data.b_split,3);
		}
	}
}

calc_cs::calc_cs(int lower, int upper,
		double E_low,
		unsigned l_max,
		double rmin,double rmax,double dr,
		int use_scat_Col,
		int interpol,
		double tol,
		const potential* pot)
 : m_integrator(-1.0,1.0,lower,upper),
   m_rmin(rmin),m_rmax(rmax),
   m_use_scat_Colonna(use_scat_Col),
   m_interp(interpol),
   m_tolerance(tol),
   m_pot(*pot),
   m_l_max(l_max),
   m_triplets(pot, rmin, rmax, dr, E_low)
{
}

std::unique_ptr<const calc_cs> calc_cs::create(const json_type& cnf, const potential* pot)
{
	//consider these clenshaw curtis quadratures
	const int lower_cc=cnf.at("cc").at("lower");
	const int upper_cc=cnf.at("cc").at("upper");

	//set range for calculating cross section as a function of energy [log]
	/// \todo pass around the energy itself, in SI units, around internally
	const double logE_low=std::log10(in_si(cnf.at("energy_range").at("min"),units::energy));
	//const double logE_high=std::log10(in_si(cnf.at("energy_range").at("max"),units::energy));
	//const double E_points_decade=cnf.at("energy_range").at("points_per_decade");

	// max l for which the cross section is calculated
	const unsigned l_max = cnf.at("l_max");

	double rmin=in_si(cnf.at("r").at("min"),units::length);
	double rmax=in_si(cnf.at("r").at("max"),units::length);
	const double dr=in_si(cnf.at("r").at("del"),units::length);

	// adjust rmin,rmax if necessary
	const double Vmin=in_si(cnf.at("r").at("Vmin"),units::energy);
	const double Vmax=in_si(cnf.at("r").at("Vmax"),units::energy);
	rmin_input_evaluation(rmin,pot,Vmin);
	rmax_input_evaluation(rmax,pot,Vmax);

	/** \todo Make use_scat_Colonna user-configurable?
	 */
	//determine whether scat_Colonna or scat_Viehland is used for estimating scattering angles
	const int use_scat_Colonna=1;
	//set tolerance and interpolation order
	/** \todo Make interp user-configurable?
	 */
	const int interpol=3;
	const double tolerance = cnf.at("tolerance");

	return std::make_unique<const calc_cs>(lower_cc,upper_cc,std::pow(10,logE_low),l_max,
		rmin,rmax,dr,use_scat_Colonna,interpol,tolerance,pot);
}

std::vector<double> calc_cs::integrate(double energy) const
{
	const triplet_data::case_data data{m_triplets,energy};

	std::vector<double> Q(m_l_max,0.0);

	switch (data.case_id)
	{
		case 1:
			//integrate cross section case 1: Ed<=E<=Ec
			m_integrator.integrate_md(Viehland_case_1_sub(*this,energy,data),m_l_max,m_tolerance,Q);
		break;
		case 2:
			//integrate cross section case 2: Ec<E<=3*Ec
			m_integrator.integrate_md(Viehland_case_2_sub(*this,energy,data),m_l_max,m_tolerance,Q);
		break;
		case 3:
			//integrate cross section case 3: E<Ed || E>3 Ec
			m_integrator.integrate_md(Viehland_case_3_sub(*this,energy,data),m_l_max,m_tolerance,Q);
		break;
	}

	//print results to screen
	std::cout << energy/Constant::eV << "\t";
	for (unsigned l=0;l!=Q.size();++l)
	{
		std::cout << Q[l] << "\t";
	}
	std::cout << std::endl;

	return Q;
}

data_set calc_cs::integrate(const std::vector<double>& energy_vals) const
{
	data_set sigma_data(
		"sigma_l(eps)", units::area, "m^2",
		{"epsilon", units::energy, "J"},
		{"l"} );
	// while we use SI internally, in output we prefer to write eV for the energies
	sigma_data.row_axis().set_display_unit("eV");
	for (unsigned l=1; l<=m_l_max; ++l)
	{
		sigma_data.add_column(std::to_string(l));
	}

	std::cout << "Starting calculation of cross section" << std::endl;

	for (auto energy : energy_vals)
	{
		std::vector<double> Q = integrate(energy);
		sigma_data.add_row(energy,Q);
	}
	return sigma_data;
}

data_set calc_cs::integrate(double logE_low, double logE_high, unsigned logE_pdecade) const
{
	std::vector<double> energy_vals = make_samples_log(logE_low,logE_high,logE_pdecade);
	return integrate(energy_vals);
}

data_set calc_cross_sec(int lower_cc, int upper_cc, double logE_low,double logE_high,int logE_pdecade,unsigned l_max,
	double rmin,double rmax,double dr,int use_scat_Col,int interpol,double tol,const potential* pot)
{
	//make cross section object
	calc_cs m_cs(lower_cc,upper_cc,std::pow(10,logE_low),l_max,
		rmin,rmax,dr,use_scat_Col,interpol,tol,pot);
	//perform integration
	return m_cs.integrate(logE_low,logE_high,logE_pdecade);
}

data_set calc_cross_sec(const json_type& cnf, const potential* pot)
{
	//make cross section object
	const auto cs = calc_cs::create(cnf,pot);
	//perform integration
	const double logE_low=std::log10(in_si(cnf.at("energy_range").at("min"),units::energy));
	const double logE_high=std::log10(in_si(cnf.at("energy_range").at("max"),units::energy));
	const double logE_pdecade=cnf.at("energy_range").at("points_per_decade");
	return cs->integrate(logE_low,logE_high,logE_pdecade);
}

data_set calc_cross_sec(const json_type& cnf)
{
	const std::unique_ptr<const potential> pot(potential::create(cnf.at("interaction").at("potential")));
	return calc_cross_sec(cnf,pot.get());
}

} // namespace magnumpi
