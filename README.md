# MagnumPI

Potential Integrator - A package for the calculation of scattering angles and cross sections.

The web application is hosted on [https://magnumpi.gitlab.io/magnumpi](https://magnumpi.gitlab.io/magnumpi).

For installation instructions see [INSTALL.md](INSTALL.md).

## Directory layout

* [magnumpi/](magnumpi/), C++ header files
* [src/](src/), C++ source code
* [pi_support/](pi_support/), C++ dependencies
* [app/](app/), C++ source code for command line tools
* [testsuite/](testsuite/), C++ tests
* [python/](python/), Python bindings
* [pymagnumpi/](pymagnumpi/), Python package and Python command line tools
* [web/assembly/](web/assembly/), JavaScript npm package called `@magnumpi/wasm` with WebAssembly version of C++ library compiled with [emscripten](https://emscripten.org/)
* [web/magnumpi-app/](web/magnumpi-app/), [Single Page web application](https://en.wikipedia.org/wiki/Single-page_application) using `@magnumpi/wasm` npm package
* [web/ws/python-connexion/](web/ws/python-connexion/), Web service written in Python
* [web/ws/cpp-pistache-server/](web/ws/cpp-pistache-server/), Web service written in C++
* [web/cgi/](web/cgi/), C++ CGI scripts

During CI job the following files get generated:

* [doc/notes/notes.pdf](https://gitlab.com/magnumpi/magnumpi/-/jobs/artifacts/master/file/build/doc/notes/notes.pdf?job=cpp)
