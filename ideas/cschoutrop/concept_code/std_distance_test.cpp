#include <vector>
#include <random>
#include <iostream>
#include <chrono>
#include <iomanip>

std::vector<double> random_double_vector(size_t size_of_vector, double min=-10.0,double max=+10.0)
{
	std::vector<double> vec(size_of_vector);
	std::uniform_real_distribution<double> unif(min,max);
	std::random_device rd;
	uint64_t random_seed;
	if(rd.entropy()!=0)
	{
		random_seed=rd();
	}
	else
	{
		using namespace std::chrono;
		auto point_in_time=high_resolution_clock::now().time_since_epoch();
		random_seed=point_in_time.count();
	}
	std::default_random_engine re(random_seed);
	for(auto &a:vec)
	{
		a=unif(re);
	}
	return vec;
}

template<typename T>
void print_vector(std::vector<T> vec)
{
	for(auto a:vec){
		std::cout<<std::setw(9)<<a<<" ";
	}
	std::cout<<"\n";
}

int new_method(std::vector<double> m_x,double x) {
	// find the closest point m_x[idx] < x, idx=0 even if x<m_x[0]
	std::vector<double>::iterator it;
	it=std::lower_bound(m_x.begin(),m_x.end(),x);

	//<concept new method>
	int idx=std::distance(m_x.begin(),it)-1;
	idx=(idx>0) ? idx : 0;
	//</concept new method>

	return idx;
}

int original_method(std::vector<double> m_x,double x) {
	// find the closest point m_x[idx] < x, idx=0 even if x<m_x[0]
	std::vector<double>::iterator it;
	it=std::lower_bound(m_x.begin(),m_x.end(),x);

	//<original method as reference>
	int idx=std::max( int(it-m_x.begin())-1, 0);
	//</original method as reference>

	return idx;
}

double fRand(double fMin, double fMax)
{
	double f = (double)rand() / RAND_MAX;
	return fMin + f * (fMax - fMin);
}

int main()
{
	srand(1);
	bool isOk=true;
	for(int i=0;i<1000;++i)
	{
		//Setup
		int N=10;
		double value=fRand(-12,12);
		std::vector<double> m_x=random_double_vector(N);

		//Compute distance with both methods
		int original_value=original_method(m_x,value);
		int new_value=new_method(m_x,value);

		//Process results
		std::cout<<"original_method: "<<original_value<<"\n";
		std::cout<<"new_method: "<<new_value<<"\n";
		if(original_value!=new_value)
		{
			std::cout<<"Failed, iter: "<<i<<"\n";
			isOk=false;
			break;
		}
	}
	if(isOk)
	{
		std::cout<<"Results are the same";
	}




	std::cout<<"\n";
}