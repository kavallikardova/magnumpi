#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <algorithm>

using item_type = std::pair<std::string, std::string>;
using item_list_type = std::vector<item_type>;
item_list_type m_items;

std::string* find_item1(const std::string& key)
{
	for (auto& i : m_items)
	{
		if (i.first == key)
		{
			return &i.second;
		}

		//os << std::string('\t',level) << pref << i << '\n';
	}

	return nullptr;
}
std::string* find_item2(const std::string& key)
{
	auto result_iterator = std::find_if(m_items.begin(),
			m_items.end(), [&key](const std::pair<std::string, std::string>& item)
	{
		return item.first == key;
	});

	if (result_iterator != m_items.end())
	{
		return &(result_iterator->second);
	}
	else
	{
		return nullptr;
	}
}

int compare_methods(const std::string& key)
{
	std::string* result1 = find_item1(key);
	std::string* result2 = find_item2(key);
	std::cout << "Item: " << key << "\n";
	std::cout << "result1: " << result1 << "\n";
	std::cout << "result2: " << result2 << "\n\n";
	if (result1 != result2)
	{
		std::cout << "test failed\n";
		return 1; //EXIT_FAILURE
	}
	else
	{
		return 0; //EXIT_SUCCES
	}
}

int main()
{

	m_items.push_back(std::make_pair("coffee", "square"));
	m_items.push_back(std::make_pair("tea", "triangle"));
	m_items.push_back(std::make_pair("water", "circle"));
	m_items.push_back(std::make_pair("cola", "parallelogram"));
	m_items.push_back(std::make_pair("co la", "rectangle"));
	m_items.push_back(std::make_pair("co	la", "rhombus"));
	m_items.push_back(std::make_pair("", "hexagon"));
	m_items.push_back(std::make_pair("\t\n\0\r", "_~@.,*/-{}:`'"));
	m_items.push_back(std::make_pair("_~@.,*/-{}:`'", "\t\n\0\r"));

	int test_fails=0;

	for (size_t i = 0; i < m_items.size(); ++i)
	{
		test_fails+=compare_methods(m_items[i].first);
	}
	test_fails+=compare_methods("cake");
	test_fails+=compare_methods("0");
	test_fails+=compare_methods("\0");
	test_fails+=compare_methods("\t");
	test_fails+=compare_methods("\n");
	test_fails+=compare_methods("\r");

	if(test_fails==0)
	{
		std::cout << "test passed\n";
		return 0; //EXIT_SUCCES
	}
	else
	{
		return 1; //EXIT_FAILURE
	}

}
