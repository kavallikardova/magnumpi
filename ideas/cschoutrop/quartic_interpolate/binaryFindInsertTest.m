%% Test for known values

x=[0;1;2;3;4];

%Values inside the array
xp=0.5;
[L,R]=binaryFindInsert(x,xp);
assert(L==1 && R==2)
xp=1.5;
[L,R]=binaryFindInsert(x,xp);
assert(L==2 && R==3)
xp=2.5;
[L,R]=binaryFindInsert(x,xp);
assert(L==3 && R==4)

%Values outside of the array
xp=-0.5;
[L,R]=binaryFindInsert(x,xp);
assert(L==0 && R==1)
xp=4.5;
[L,R]=binaryFindInsert(x,xp);
assert(L==5 && R==6)

%% Test for edge cases

%Array of length 1
x=[0];
xp=0.5;
[L,R]=binaryFindInsert(x,xp);
assert(L==1 && R==2)

%xp exactly at a value that is also in the input array
x=[0;1;2;3;4];
xp=1;
[L,R]=binaryFindInsert(x,xp);
assert(L==2 && R==3)

%Duplicate values in array
x=[0;1;1;2;3;4];
xp=1.5;
[L,R]=binaryFindInsert(x,xp);
assert(L==3 && R==4)

%Array of length 0
try
    x=[];
    xp=1.5;
    [L,R]=binaryFindInsert(x,xp);
catch ME
    if strcmp(ME.message,'Input array must be non-empty')==false
        error('Expected binaryFindInsert to throw: Input array must be non-empty')
    end
end

%Array with NaN
try
    x=[0;1;NaN;2;3;4];
    xp=1.5;
    [L,R]=binaryFindInsert(x,xp);
catch ME
    if strcmp(ME.message,'Invalid input array or input value')==false
        error('Expected binaryFindInsert to throw: Invalid input array or input value')
    end    
end

%Input value with NaN
try
    x=[0;1;1.5;2;3;4];
    xp=NaN;
    [L,R]=binaryFindInsert(x,xp);
catch ME
    if strcmp(ME.message,'Invalid input array or input value')==false
        error('Expected binaryFindInsert to throw: Invalid input array or input value')
    end    
end

%% Randomly generated tests comparing to naive for loop implementation
%Naive for loop complexity is O(N), the binaryFindInsert O(log(N)), and
%will be much faster for large arrays.
for i=1:100
    N=10;
    x=sort(rand(N,1));
    xp=rand();
    L=0;
    R=1;
    for j=1:N
        if xp<x(R)
            break
        else
            L=L+1;
            R=R+1;
        end
    end
    [L_bin,R_bin]=binaryFindInsert(x,xp);
    assert(L==L_bin && R==R_bin);
end




