clear all
close all
%{
Verification of the table above equation 13
%}
%Hermite spline base
h{1}= @(u) 1+u.^2.*(2.*u-3);
h{2}= @(u) u.*(u-1).^2;
h{3}= @(u) u.^2.*(3-2.*u);
h{4}= @(u) u.^2.*(u-1);
h_num{1}='00';
h_num{2}='10';
h_num{3}='01';
h_num{4}='11';

%Derivatives of the Hermite splines
hp{1}= @(u) 6.*u.*(u-1);
hp{2}= @(u) 3.*u.^2-4.*u+1;
hp{3}= @(u) -6.*(u-1).*u;
hp{4}= @(u) u.*(3.*u-2);
hp_num{1}='00';
hp_num{2}='10';
hp_num{3}='01';
hp_num{4}='11';

for i=1:4
    for j=1:4
        f=@(u) h{i}(u).*hp{j}(u);
        I_calculated(i,j)=integral(f,0,1);
        
        disp([h_num{i} hp_num{j} ,': ',num2str(I_calculated(i,j))])
    end
end
I_calculated
I_expected=[
    -1/2 1/10 1/2 -1/10;...
    -1/10 0 1/10 -1/60;...
    -1/2 -1/10 1/2 1/10;...
    1/10 1/60 -1/10 0;...
    ]