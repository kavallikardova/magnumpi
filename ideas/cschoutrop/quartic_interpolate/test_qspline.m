clear all
close all

%qspline_object=qspline2();
qspline_object=qspline();

x=linspace(1,5,25);
s=x.^2;


qspline_object.set_points(x,s);

x_new=linspace(1,5)';
s_new=0
for i=1:100
s_new(i)=qspline_object.get(x_new(i));
end
figure()
plot(x_new,s_new);
title('Interpolated function')

s_new=0
for i=1:100
s_new(i)=qspline_object.deriv1(x_new(i));
end
figure()
plot(x_new,s_new);
title('Derivative of interpolated function')

s_new=0
for i=1:100
s_new(i)=qspline_object.deriv2(x_new(i));
end
figure()
plot(x_new,s_new);
title('Second derivative of interpolated function')
%{
qspline_object.get(x_new)
qspline_object.deriv1(x_new)
qspline_object.deriv2(x_new)
%}