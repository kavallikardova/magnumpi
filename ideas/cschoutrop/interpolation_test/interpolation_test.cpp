#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include "magnumpi/spline.h"
#include "magnumpi/vector_sort.h"
/*
	Script set up to test the spline interpolation.
	Goals:
	-Confirm spline implementation works as intended
	-Look into the method of "minimizing curvature" by F.H.F. Wulf

	Conclusions:
	-Implementation of spline shows expected behavior for the test cases.
	-When the first derivative strongly changes, the spline interpolation is likely to overshoot/undershoot and cause oscillations.
	-Choosing nodes where the second derivative is large can lower the interpolation error significantly.
*/

template<typename T>
double norm1_error(std::vector<double> x, std::vector<double> y, T function)
{
	double error = 0;

	for (unsigned i = 0; i < x.size(); ++i)
	{
		error += std::abs(y[i] - function(x[i]));
	}

	return error;
}

std::vector<double> LinearSpacedArray(double a, double b, std::size_t N)
{
	double h = (b - a) / static_cast<double>(N - 1);
	std::vector<double> xs(N);
	std::vector<double>::iterator x;
	double val;

	for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h)
	{
		*x = val;
	}

	return xs;
}

template<typename T>
std::vector<double> apply_function(std::vector<double> x, T function)
{
	std::vector<double> y(x.size());

	for (unsigned i = 0; i < x.size(); ++i)
	{
		y[i] = function(x[i]);
	}

	return y;
}

template<typename T>
void print(std::vector<T> x)
{
	std::for_each(x.begin(), x.end(), [](T n)
	{
		std::cout << n << " ";
	});
	std::cout << "\n";
}

template<typename T>
void print(std::vector<T> x, std::vector<T> y)
{
	for (unsigned i = 0; i < x.size(); ++i)
	{
		std::cout << x[i] << " " << y[i] << "\n";
	}

	std::cout << "\n";
}

template<typename T>
void save_to_file(std::string filename, std::vector<T> x, std::vector<T> y)
{
	std::ofstream output_file;
	output_file.open (filename);

	for (unsigned i = 0; i < x.size(); ++i)
	{
		output_file << x[i] << " " << y[i] << "\n";
	}

	output_file << "\n";
	output_file.close();
}

template<typename T>
void minimize_curvature(T function, std::vector<double>& x, std::vector<double>& y, unsigned N_max)
{
	unsigned N = x.size();

	while (N < N_max)
	{
		std::vector<double> angles;

		for (unsigned i = 1; i < x.size() - 1; ++i)
		{
			double r1 = (y[i] - y[i - 1]) / (x[i] - x[i - 1]);
			double r2 = (y[i] - y[i + 1]) / (x[i + 1] - x[i]);
			angles.push_back(std::abs(std::atan(r1) + std::atan(r2)));
		}

		auto max_it = std::max_element(angles.begin(), angles.end());
		unsigned ix = std::distance(angles.begin(), max_it) + 1;
		double new_x = (x[ix + 1] + x[ix]) / 2;
		double new_y = function(new_x);
		x.push_back(new_x);
		y.push_back(new_y);
		new_x = (x[ix - 1] + x[ix]) / 2;
		new_y = function(new_x);
		x.push_back(new_x);
		y.push_back(new_y);

		//Sort m_x and keep corresponding x and y points together
		tk::vector_sort permutation(x, std::less<double>());
		permutation.permute(x);
		permutation.permute(y);
		N += 2;
	}


}

int main()
{
	{
		/*
		        Case 2:
		        Test with a high gradient.
		        Conclusion:
		        When interpolating the sigmoid function 1/(1+exp(25*x)) on the interval [-1,1] there is significant overshoot-undershoot.
		*/
		double a = 25.0;
		unsigned N_input = 9;
		unsigned N_output = 1001;
		auto sigmoid = [&](double x)
		{
			return 1.0 / (1.0 + std::exp(-a * x));
		};
		//Setup the real function
		std::vector<double> x_output = LinearSpacedArray(-1, +1, N_output);
		std::vector<double> y_real = apply_function(x_output, sigmoid);
		save_to_file("case2_real.txt", x_output, y_real);

		//Setup initial data
		std::vector<double> x_start_data = LinearSpacedArray(-1, +1, N_input);
		std::vector<double> y_start_data = apply_function(x_start_data, sigmoid);
		save_to_file("case2_start_data.txt", x_start_data, y_start_data);

		std::vector<double> y_interpolated_spline(x_output.size());
		tk::spline S;
		S.set_points(x_start_data, y_start_data);

		for (unsigned i = 0; i < x_output.size(); ++i)
		{
			y_interpolated_spline[i] = S(x_output[i]);
		}

		save_to_file("case2_interpolated_spline.txt", x_output, y_interpolated_spline);

		//Calculate the error in the spline interpolation
		std::cout << "Case 2 norm1 error: " << norm1_error(x_output, y_interpolated_spline,
				sigmoid) << "\n";
	}
	{
		/*
		        Case 3:
		        Test with a high second derivative

			Conclusion:
			Strong changes in first derivative case over-undershoot of the real function
		*/
		double a = 25.0;
		unsigned N_input = 9;
		unsigned N_output = 1001;
		auto sigmoid_derivative = [&](double x)
		{
			return -a * std::exp(-x * a) / std::pow(1 + exp(-a * x), 2);
		};
		//Setup the real function
		std::vector<double> x_output = LinearSpacedArray(-1, +1, N_output);
		std::vector<double> y_real = apply_function(x_output, sigmoid_derivative);
		save_to_file("case3_real.txt", x_output, y_real);

		//Setup initial data
		std::vector<double> x_start_data = LinearSpacedArray(-1, +1, N_input);
		std::vector<double> y_start_data = apply_function(x_start_data, sigmoid_derivative);
		save_to_file("case3_start_data.txt", x_start_data, y_start_data);

		//Determine spline interpolation of the initial data
		std::vector<double> y_interpolated_spline(x_output.size());
		tk::spline S;
		S.set_points(x_start_data, y_start_data);

		for (unsigned i = 0; i < x_output.size(); ++i)
		{
			y_interpolated_spline[i] = S(x_output[i]);
		}

		save_to_file("case3_interpolated_spline.txt", x_output, y_interpolated_spline);

		//Calculate the error in the spline interpolation
		std::cout << "Case 3 norm1 error: " << norm1_error(x_output, y_interpolated_spline,
				sigmoid_derivative) << "\n";
	}
	{
		/*
		        Case 4:
		        Curvature minimization, idea os based on "impact of interaction potential of transport coefficients" by F.H.F. Wulf

			Conclusion:
			Especially for a low number of points used to construct the interpolation the error is much lower than linearly spaced input.
		*/
		double a = 25.0;
		unsigned N_input = 5;
		unsigned N_max = 13;
		unsigned N_output = 1000;
		auto sigmoid_derivative = [&](double x)
		{
			return -a * std::exp(-x * a) / std::pow(1 + exp(-a * x), 2);
		};

		//Setup initial data
		std::vector<double> x_start_data = LinearSpacedArray(-1, +1, N_input);
		std::vector<double> y_start_data = apply_function(x_start_data, sigmoid_derivative);
		save_to_file("case4_start_data.txt", x_start_data, y_start_data);

		//Save real function to file
		std::vector<double> x_output = LinearSpacedArray(-1, +1, N_output);
		std::vector<double> y_real = apply_function(x_output, sigmoid_derivative);
		save_to_file("case4_real.txt", x_output, y_real);

		//Find additional points that best fit the regions with strong second derivatives
		minimize_curvature(sigmoid_derivative, x_start_data, y_start_data, N_max);
		std::vector<double> x_input_min = x_start_data;
		std::vector<double> y_input_min = y_start_data;
		save_to_file("case4_input_minimum_curvature.txt", x_input_min, y_input_min);

		//Determine spline interpolation using the "minimum curvature" input data
		tk::spline S_minimized;
		S_minimized.set_points(x_input_min, y_input_min);
		std::vector<double> y_min_curvature_spline(x_output.size());

		for (unsigned i = 0; i < x_output.size(); ++i)
		{
			y_min_curvature_spline[i] = S_minimized(x_output[i]);
		}

		save_to_file("case4_min_curvature_spline.txt", x_output, y_min_curvature_spline);

		//Setup linearly sampled input with same number of points for comparison
		unsigned N_minimized = x_input_min.size();
		std::vector<double> x_linear_sampled_spline = LinearSpacedArray(-1, +1, N_minimized);
		std::vector<double> y_linear_sampled_spline = apply_function(x_linear_sampled_spline,
				sigmoid_derivative);
		save_to_file("case4_input_linear_spaced.txt", x_linear_sampled_spline, y_linear_sampled_spline);

		//Determine spline interpolation of the linearly sampled input
		tk::spline S_linear;
		S_linear.set_points(x_linear_sampled_spline, y_linear_sampled_spline);
		std::vector<double> y_output_linear(x_output.size());

		for (unsigned i = 0; i < x_output.size(); ++i)
		{
			y_output_linear[i] = S_linear(x_output[i]);
		}

		save_to_file("case4_linear_spaced_spline.txt", x_output, y_output_linear);

		//Calculate the error in the spline interpolation
		std::cout << "Case 4 norm1 error linear : " << norm1_error(x_output, y_output_linear,
				sigmoid_derivative) << "\n";
		std::cout << "Case 4 norm1 error min_cur: " << norm1_error(x_output, y_min_curvature_spline,
				sigmoid_derivative) << "\n";
	}
	{
		/*
		        Test of the splines with given input files
		*/
		std::ifstream infile("../input/masonschamp.txt");
		std::vector<double> x_start_data;
		std::vector<double> y_start_data;
		std::string line;

		if (infile.is_open())
		{
			double x;
			double y;

			while ( getline (infile, line) )
			{
				std::cout << line << std::endl;
				std::stringstream line_stream(line);

				if (line_stream >> x >> y)
				{
					x_start_data.push_back(x);
					y_start_data.push_back(y);
				}

			}

			infile.close();
		}

		save_to_file("input_test.txt", x_start_data, y_start_data);
		unsigned N_output = 10000;

		//Determine spline interpolation of the initial data
		std::vector<double> x_output = LinearSpacedArray(1.0e-13, 4e-10, N_output);
		std::vector<double> y_interpolated_spline(x_output.size());
		tk::spline S;
		S.set_points(x_start_data, y_start_data);

		for (unsigned i = 0; i < x_output.size(); ++i)
		{
			y_interpolated_spline[i] = S(x_output[i]);
		}

		save_to_file("output_test.txt", x_output, y_interpolated_spline);
	}
	{
		/*
		        Test of the splines with all the input files
		*/
		std::vector<std::string> list_of_files = {"../input/rscp.txt", "../input/masonschamp.txt", "../input/morse.txt", "../input/He+_He_ungerade.txt", "../input/morse.txt", "../input/He+_He_gerade.txt", "../input/inverse_square.txt", "../input/coulomb.txt"};

		for (auto file : list_of_files)
		{
			std::cout << "input file: " + file << std::endl;
			std::ifstream infile(file);
			std::vector<double> x_start_data;
			std::vector<double> y_start_data;
			std::string line;

			if (infile.is_open())
			{
				double x;
				double y;

				while ( getline (infile, line) )
				{
					std::stringstream line_stream(line);

					if (line_stream >> x >> y)
					{
						x_start_data.push_back(x);
						y_start_data.push_back(y);
					}

				}

				infile.close();
			}

			tk::spline S;
			S.set_points(x_start_data, y_start_data);
		}

	}
}
