set terminal svg enhanced size 500,500
set key bottom right

set output 'case_3_fig_1.svg'
set xlabel "x"
set ylabel "y"
set title "-25exp(-25x) / (1 + exp(-25x))^2"

set style line 1 lt rgb "red" lw 2
set style line 2 lt rgb "orange" lw 2
set style line 3 lt rgb "yellow" lw 2
set style line 4 lt rgb "green" lw 2
set style line 5 lt rgb "cyan" lw 2
set style line 6 lt rgb "blue" lw 2
set style line 7 lt rgb "violet" lw 2

plot "case3_start_data.txt" ls 2 pt 7 ps 1, "case3_interpolated_spline.txt" with lines ls 1, "case3_real.txt" with lines ls 6
