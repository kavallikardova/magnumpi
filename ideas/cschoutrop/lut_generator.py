import physconst
import numpy as np
"""
Simple script to generate a LUT from a potential. 
Here the Coulomb potential for two elementary charges is set up, and the inverse 
square potential. For both cases analytical expressions for the scattering angle
are known. 

All quantities are in terms of SI-units.
"""

def coulomb_potential(C,r):
    return C/r

def inverse_square_potential(C,r):
    return C/r**2

r_values=np.arange(1.0e-13,1.0e-8,1.0e-13)

file_coulomb=open("coulomb.txt","w+")
file_inverse_square=open("inverse_square.txt","w+")
C_coulomb=physconst.ke*physconst.e**2 #could be proton-proton collision
C_inverse_square=C_coulomb*physconst.a0 #multiply by a0 to ensure the units still make sense

print('C_coulomb='+str(C_coulomb))
print('C_inverse_square='+str(C_inverse_square))

for r in r_values:
	coulomb_line="{:e}\t{:e}\n".format(r,coulomb_potential(C_coulomb,r))
	file_coulomb.write(coulomb_line)
	inverse_square_line="{:e}\t{:e}\n".format(r,inverse_square_potential(C_inverse_square,r))
	file_inverse_square.write(inverse_square_line)

file_coulomb.close()
file_inverse_square.close()