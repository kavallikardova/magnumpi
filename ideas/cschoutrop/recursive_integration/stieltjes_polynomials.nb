(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     15770,        510]
NotebookOptionsPosition[     14897,        480]
NotebookOutlinePosition[     15233,        495]
CellTagsIndexPosition[     15190,        492]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{"Clear", "[", "\"\<`*\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{"E4", "=", 
  RowBox[{
   RowBox[{"e4", " ", 
    SuperscriptBox["x", "4"]}], "+", 
   RowBox[{"e3", " ", 
    SuperscriptBox["x", "3"]}], "+", 
   RowBox[{"e2", " ", 
    SuperscriptBox["x", "2"]}], "+", 
   RowBox[{"e1", " ", 
    SuperscriptBox["x", "1"]}], "+", "e0"}]}], "\[IndentingNewLine]", 
 RowBox[{"E4Coeff", "=", 
  RowBox[{
   RowBox[{"Solve", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"E4", " ", "E4"}], " ", ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "1"}],
        ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{
           SuperscriptBox["x", "3"], "E4", " ", 
           RowBox[{"LegendreP", "[", 
            RowBox[{"3", ",", "x"}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "0"}],
        ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{
           SuperscriptBox["x", "2"], "E4", " ", 
           RowBox[{"LegendreP", "[", 
            RowBox[{"3", ",", "x"}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "0"}],
        ",", 
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"x", " ", "E4", " ", 
           RowBox[{"LegendreP", "[", 
            RowBox[{"3", ",", "x"}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "0"}],
        ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"E4", " ", 
           RowBox[{"LegendreP", "[", 
            RowBox[{"3", ",", "x"}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", 
        "0"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"e0", ",", "e1", ",", "e2", ",", "e3", ",", "e4"}], "}"}]}], 
    "]"}], "[", 
   RowBox[{"[", "1", "]"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"E4Roots", "=", 
  RowBox[{
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{"E4", "\[Equal]", "0"}], ",", "x"}], "]"}], "/.", 
   "E4Coeff"}]}], "\[IndentingNewLine]", 
 RowBox[{"E3", "=", 
  RowBox[{
   RowBox[{"e3", " ", 
    SuperscriptBox["x", "3"]}], "+", 
   RowBox[{"e2", " ", 
    SuperscriptBox["x", "2"]}], "+", 
   RowBox[{"e1", " ", 
    SuperscriptBox["x", "1"]}], "+", "e0"}]}], "\[IndentingNewLine]", 
 RowBox[{"E3Coeff", "=", 
  RowBox[{
   RowBox[{"Solve", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"E3", " ", "E3"}], " ", ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "1"}],
        ",", 
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{
           SuperscriptBox["x", "2"], "E3", " ", 
           RowBox[{"LegendreP", "[", 
            RowBox[{"2", ",", "x"}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "0"}],
        ",", 
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"x", " ", "E3", " ", 
           RowBox[{"LegendreP", "[", 
            RowBox[{"2", ",", "x"}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "0"}],
        ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"E3", " ", 
           RowBox[{"LegendreP", "[", 
            RowBox[{"2", ",", "x"}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", 
        "0"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"e0", ",", "e1", ",", "e2", ",", "e3"}], "}"}]}], "]"}], "[", 
   RowBox[{"[", "1", "]"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"E3Roots", "=", 
  RowBox[{
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{"E3", "\[Equal]", "0"}], ",", "x"}], "]"}], "/.", 
   "E3Coeff"}]}], "\[IndentingNewLine]", 
 RowBox[{"E2", "=", 
  RowBox[{
   RowBox[{"e2", " ", 
    SuperscriptBox["x", "2"]}], "+", 
   RowBox[{"e1", " ", 
    SuperscriptBox["x", "1"]}], "+", "e0"}]}], "\[IndentingNewLine]", 
 RowBox[{"E2Coeff", "=", 
  RowBox[{
   RowBox[{"Solve", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"E2", " ", "E2"}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "1"}],
        ",", 
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"x", " ", "E2", " ", 
           RowBox[{"LegendreP", "[", 
            RowBox[{"1", ",", "x"}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "0"}],
        ",", 
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"E2", " ", 
           RowBox[{"LegendreP", "[", 
            RowBox[{"1", ",", "x"}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", 
        "0"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"e0", ",", "e1", ",", "e2"}], "}"}]}], "]"}], "[", 
   RowBox[{"[", "1", "]"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"E2Roots", "=", 
  RowBox[{
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{"E2", "\[Equal]", "0"}], ",", "x"}], "]"}], "/.", 
   "E2Coeff"}]}], "\[IndentingNewLine]", 
 RowBox[{"E1", "=", 
  RowBox[{
   RowBox[{"e1", " ", 
    SuperscriptBox["x", "1"]}], "+", "e0"}]}], "\[IndentingNewLine]", 
 RowBox[{"E1Coeff", "=", 
  RowBox[{
   RowBox[{"Solve", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"E1", " ", "E1"}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "1"}],
        ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Integrate", "[", 
         RowBox[{
          RowBox[{"E1", " ", 
           RowBox[{"LegendreP", "[", 
            RowBox[{"0", ",", "x"}], "]"}]}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", 
            RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", 
        "0"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"e0", ",", "e1"}], "}"}]}], "]"}], "[", 
   RowBox[{"[", "1", "]"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"E1Roots", "=", 
  RowBox[{
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{"E1", "\[Equal]", "0"}], ",", "x"}], "]"}], "/.", 
   "E1Coeff"}]}], "\[IndentingNewLine]", 
 RowBox[{"E0", "=", "e0"}], "\[IndentingNewLine]", 
 RowBox[{"E0Coeff", "=", 
  RowBox[{
   RowBox[{"Solve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"Integrate", "[", 
       RowBox[{
        RowBox[{"E0", " ", "E0"}], ",", 
        RowBox[{"{", 
         RowBox[{"x", ",", 
          RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}], "\[Equal]", "1"}], 
     ",", "e0"}], "]"}], "[", 
   RowBox[{"[", "1", "]"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"E0Coeff", "=", 
  RowBox[{
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{"E0", "\[Equal]", "0"}], ",", "x"}], "]"}], "/.", 
   "E0Coeff"}]}]}], "Input",
 CellChangeTimes->{{3.752220933257637*^9, 3.752220964443495*^9}, {
  3.752220995228196*^9, 3.7522211168507023`*^9}}],

Cell[BoxData[
 RowBox[{"e0", "+", 
  RowBox[{"e1", " ", "x"}], "+", 
  RowBox[{"e2", " ", 
   SuperscriptBox["x", "2"]}], "+", 
  RowBox[{"e3", " ", 
   SuperscriptBox["x", "3"]}], "+", 
  RowBox[{"e4", " ", 
   SuperscriptBox["x", "4"]}]}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.752221117361269*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"e0", "\[Rule]", 
    FractionBox[
     RowBox[{"155", " ", 
      SqrtBox[
       FractionBox["7", "2006"]]}], "8"]}], ",", 
   RowBox[{"e1", "\[Rule]", "0"}], ",", 
   RowBox[{"e2", "\[Rule]", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"495", " ", 
       SqrtBox[
        FractionBox["7", "2006"]]}], "4"]}]}], ",", 
   RowBox[{"e3", "\[Rule]", "0"}], ",", 
   RowBox[{"e4", "\[Rule]", 
    FractionBox[
     RowBox[{"891", " ", 
      SqrtBox[
       FractionBox["7", "2006"]]}], "8"]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.7522211174006157`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{
      RowBox[{"-", "0.9604912687080204`"}], "+", 
      RowBox[{"8.210546252100817`*^-18", " ", "\[ImaginaryI]"}]}]}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{
      RowBox[{"-", "0.43424374934680265`"}], "-", 
      RowBox[{"1.8160671278121324`*^-17", " ", "\[ImaginaryI]"}]}]}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{"0.43424374934680265`", "\[VeryThinSpace]", "+", 
      RowBox[{"1.8160671278121324`*^-17", " ", "\[ImaginaryI]"}]}]}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{"0.9604912687080204`", "\[VeryThinSpace]", "-", 
      RowBox[{"8.210546252100817`*^-18", " ", "\[ImaginaryI]"}]}]}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.752221117408832*^9}],

Cell[BoxData[
 RowBox[{"e0", "+", 
  RowBox[{"e1", " ", "x"}], "+", 
  RowBox[{"e2", " ", 
   SuperscriptBox["x", "2"]}], "+", 
  RowBox[{"e3", " ", 
   SuperscriptBox["x", "3"]}]}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.7522211174095993`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"e0", "\[Rule]", "0"}], ",", 
   RowBox[{"e1", "\[Rule]", 
    RowBox[{
     RowBox[{"-", "3"}], " ", 
     SqrtBox[
      FractionBox["10", "11"]]}]}], ",", 
   RowBox[{"e2", "\[Rule]", "0"}], ",", 
   RowBox[{"e3", "\[Rule]", 
    RowBox[{"7", " ", 
     SqrtBox[
      FractionBox["5", "22"]]}]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.75222111743882*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{"0.9258200997725515`", "\[VeryThinSpace]", "-", 
      RowBox[{"5.551115123125783`*^-17", " ", "\[ImaginaryI]"}]}]}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{
      RowBox[{"-", "0.9258200997725514`"}], "-", 
      RowBox[{"5.551115123125783`*^-17", " ", "\[ImaginaryI]"}]}]}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{
      RowBox[{"-", "9.980671985735076`*^-17"}], "+", 
      RowBox[{"0.`", " ", "\[ImaginaryI]"}]}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.752221117453631*^9}],

Cell[BoxData[
 RowBox[{"e0", "+", 
  RowBox[{"e1", " ", "x"}], "+", 
  RowBox[{"e2", " ", 
   SuperscriptBox["x", "2"]}]}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.752221117454282*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"e0", "\[Rule]", 
    RowBox[{"-", 
     FractionBox["3", 
      RowBox[{"2", " ", 
       SqrtBox["2"]}]]}]}], ",", 
   RowBox[{"e1", "\[Rule]", "0"}], ",", 
   RowBox[{"e2", "\[Rule]", 
    FractionBox["5", 
     RowBox[{"2", " ", 
      SqrtBox["2"]}]]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.752221117470586*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{"-", "0.7745966692414834`"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", "0.7745966692414834`"}], "}"}]}], "}"}]], "Output",\

 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.752221117471243*^9}],

Cell[BoxData[
 RowBox[{"e0", "+", 
  RowBox[{"e1", " ", "x"}]}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.752221117471719*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"e0", "\[Rule]", "0"}], ",", 
   RowBox[{"e1", "\[Rule]", 
    RowBox[{"-", 
     SqrtBox[
      FractionBox["3", "2"]]}]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.752221117472157*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"x", "\[Rule]", "0.`"}], "}"}], "}"}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.7522211174725847`*^9}],

Cell[BoxData["e0"], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.752221117472974*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"e0", "\[Rule]", 
   RowBox[{"-", 
    FractionBox["1", 
     SqrtBox["2"]]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.7522211174733753`*^9}],

Cell[BoxData[
 RowBox[{"{", "}"}]], "Output",
 CellChangeTimes->{
  3.7522209648903437`*^9, {3.752221019865897*^9, 3.752221079047113*^9}, 
   3.7522211174738626`*^9}]
}, Open  ]]
},
WindowSize->{816, 936},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (September 9, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 8330, 249, 685, "Input"],
Cell[8913, 273, 371, 11, 31, "Output"],
Cell[9287, 286, 691, 23, 77, "Output"],
Cell[9981, 311, 955, 27, 57, "Output"],
Cell[10939, 340, 314, 9, 31, "Output"],
Cell[11256, 351, 489, 16, 59, "Output"],
Cell[11748, 369, 738, 21, 57, "Output"],
Cell[12489, 392, 253, 7, 31, "Output"],
Cell[12745, 401, 448, 15, 54, "Output"],
Cell[13196, 418, 361, 11, 31, "Output"],
Cell[13560, 431, 194, 5, 31, "Output"],
Cell[13757, 438, 314, 10, 59, "Output"],
Cell[14074, 450, 226, 6, 31, "Output"],
Cell[14303, 458, 148, 3, 31, "Output"],
Cell[14454, 463, 258, 8, 54, "Output"],
Cell[14715, 473, 166, 4, 64, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
