function [N,val]=int_Clenshaw_Curtis(x_min,x_max,func,N)
%{
Approximates the integral \int_{x_min}^{x_max}func(x)dx with the N-point
Clenshaw Curtis method. 
%}

[x,w]=fclencurt(N,x_min,x_max);
val=sum(w.*func(x));

end
