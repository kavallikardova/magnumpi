Using the astyle package, avalable through through regular software installer of openSUSE 15.0. C++ source/header files can be automatically formatted to a variety of styles. 

Detailed overview of options can be found on:
http://astyle.sourceforge.net/astyle.html

I think the following command formats everything close to the style suggested earlier:

astyle  --style=allman  --style=break --indent=tab --indent=force-tab=8 --indent-classes --indent-switches --indent-cases --indent-namespaces --indent-after-parens --indent-preproc-define --indent-preproc-cond --indent-col1-comments --break-blocks --pad-oper --pad-comma --pad-header  --align-pointer=type  --align-reference=type --break-one-line-headers --add-braces  --attach-return-type --remove-comment-prefix --mode=c --suffix=none --preserve-date --verbose --lineend=linux --max-code-length=100 --recursive /path/to/the/files/*.cpp,*.h

Note, this is not a full linter, only auto-formatting tool. For static analysis something like ```cppcheck --enable-all /path/``` seems to do a nice job finding suspicious constructs, unused variables, bounds checking, leaks etc.
Everything it checks for can be found here: https://sourceforge.net/p/cppcheck/wiki/ListOfChecks/