/* The following experimental code snippet appeared in
 * src/scat_colonna.cpp (commented out).
 */


/* Work in progress recursive integrator
 * TODO: Find a good method that guarantees the integrand is never nan
 * (this relies on finding a good r0).
 * This has to be done without using the Colonna integrator ideally.
 * Estimating when (1.0 - std::pow(b/r_low, 2) - pot_low / eps) == 0.0
 * using (damped) Newton iterations is unreliable.
 */
//Lambda function denoting the integrand:
auto fn = [eps, b,V](double r)
{
	double value=std::pow(r, -2) / std::sqrt(1.0 - std::pow(b/r, 2) - V->value(r) / eps);
	return value;
};
Integrator<GK15> integrator;
integrator.set_range(r0, r_high_original);
integrator.set_tolerance(tolerance);
error=tolerance;
double value = integrator.integrate(fn);	
integral=value+integral_long_range_estimate;
return Constant::pi - 2*b*integral;
