import math

c=299792458 			#m/s Speed of light
G=6.67408e-11 			#m3 kg-1 s-2 Gravitational constant
h=6.626070040e-34 		#Js Planck constant
hbar=h/(2*math.pi) 		#Planck's reduced constant
e=16021766208e-19 		#C Elementary charge
mu0=4*math.pi*1e-7 		#H/m Vacuum permeability
epsilon0=1/(c*c*mu0) 		#F/m vacuum permittivity
NA=6.02214086e23 		#mol-1 Avogadro's constant
ke=1/(4*math.pi*epsilon0) 	#m/F Coulomb force constant
kb=1.38064852e-23 		#m2 kg s-2 K-1 Boltzman constant
R=kb*NA 			#J K-1 mol-1 Ideal gas constant
amu=1.660539040e-27 		#kg Atomic mass unit
mp=1.672621898e-27 		#kg Mass proton
me=9.10938356e-31 		#kg Mass electron
eV=e 				#J Electron volts in terms of Joules
a0=0.52917721067e-10 		#m Bohr radius