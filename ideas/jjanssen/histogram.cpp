#include "magnumpi/histogram.h"

namespace magnumpi {

histogram::interval::interval(double low, double high)
{
	m_lower = low;
	m_higher = high;
}

void histogram::interval::get(double& low, double& high) const
{
	low = m_lower;
	high = m_higher;
}

histogram::hdata::hdata()
	: m_count(0)
{
}

void histogram::hdata::add_interval(double b1, double b2)
{
	m_step.push_back(interval(b1, b2));
}

void histogram::hdata::get_interval(unsigned index, double& b1, double& b2) const
{
	m_step[index].get(b1, b2);
}

histogram::histogram(unsigned num_cells)
{
	m_data.resize(num_cells, hdata());
}

void histogram::add_interval(unsigned index, double b1, double b2)
{
	m_data[index].add_interval(b1, b2);
	m_indices.push_back(interval(index, m_data[index].size() - 1));
}

void histogram::get_interval(unsigned ind_angle, unsigned ind_db, double& b1, double& b2) const
{
	m_data[ind_angle].get_interval(ind_db, b1, b2);
}

void histogram::add(unsigned index)
{
	m_data[index].add();
}

void histogram::reset(unsigned index)
{
	m_data[index].reset();
}

unsigned histogram::number(unsigned index) const
{
	return m_data[index].number();
}

void histogram::print_histogram() const
{
	std::cout << "index\tcounts" << std::endl;

	for (unsigned i = 0; i < m_data.size(); ++i)
	{
		std::cout << i << "\t" << m_data[i].number() << std::endl;
	}
}
void histogram::print_intervals() const
{
	std::cout << "angle\tsubinterval\tbmin\tbmax" << std::endl;

	for (unsigned i = 0; i < m_indices.size(); ++i)
	{
		double jj, kk;
		m_indices[i].get(jj, kk);

		unsigned j = static_cast<unsigned>(jj);
		unsigned k = static_cast<unsigned>(kk);

		double b1, b2;
		get_interval(j, k, b1, b2);
		std::cout << j << "\t" << k << "\t" << b1 << "\t" << b2 << std::endl;
	}
}

} // namespace magnumpi
