#include "nlohmann/json.hpp"
#include <iostream>
#include <vector>
#include <typeinfo>

/* Reads a JSON file from std::cin and writes it to std::cout.
 * This will be used to also test the C++ JSON-API that is
 * provided by the included json.hpp.
 */
int main()
{
	using namespace nlohmann;

	json j;

	// read from std input
	std::cin >> j;

	// STL-type iteration over all elements (in j, the root)
	// note that it.value() can be either am object, an array
	// or a 'simple value' (string, number, true, false, null)
	for (auto it = j.begin(); it != j.end(); ++it)
	{
		std::cout << "key: " << it.key() << ", value:" << it.value() << '\n';
	}
	// print (dump) subsection cc to std::cout
	std::cout << "CC object: " << std::endl;
	std::cout << j["cc"].dump(2) << std::endl;
	// assign the value of "lower" in subsection "cc" to cc_lower
	int cc_lower = j["cc"]["lower"];
	std::cout << "cc.lower = " << cc_lower << std::endl;

	std::cout << "equal: " << (j["cc"]["lower"]==6) << std::endl;
	std::cout << "equal: " << (j["cc"]["lower"]=="{ 6") << std::endl;
	std::cout << "equal: " << (j["cc"]["lower"]=="{ \"lower\": 6 }") << std::endl;
	std::cout << "equal: " << (j["cc"]==json{"{ \"lower\":  6,  \"upper\": 14 }"_json}) << std::endl;
	json j_cc;
	//j_cc.parse("{ \"cc\": { \"upper\": 14, \"lower\":  6 } }");
	j_cc = json::parse("{ \"lower\":  6,  \"upper\": 14 }");
	j_cc = "{\"lower\":  6,  \"upper\": 14 }"_json;
	std::cout << "j_cc: " << j_cc.dump(0) << std::endl;
	std::cout << "equal: " << (j["cc"]==j_cc) << std::endl;
	std::cout << "equal: " << (j["cc"]==j["cc"]) << std::endl;
	std::cout << "equal: " << (j["cc"]==j_cc["cc"]) << std::endl;
	std::cout << "jcc:  " << j["cc"].dump(0) << std::endl;
	std::cout << "j_cc: " << j_cc["cc"].dump(0) << std::endl;

	//std::cout << j.dump(2) << std::endl;

	return 0;
}
