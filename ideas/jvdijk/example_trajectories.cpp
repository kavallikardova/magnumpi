/* Example invocation:
 *   ./ideas/jvdijk/example_trajectories ../ideas/jvdijk/example_trajectories.in ../input/lennard_jones.json > out.dat
 *   gnuplot
 *     n=41 # number of b points in example_trajectories.in
 *     e=0  # energy index, [0,n_energies), also in example_trajectories.in
 *     plot "out.dat" u 4:5 index e*n:(e+1)*n-1 w l
 */

#include "magnumpi/json.h"
#include "magnumpi/potential.h"
#include "magnumpi/units.h"
#include "magnumpi/utility.h"
#include <boost/numeric/odeint.hpp>

#include <vector>
#include <array>
#include <iostream>
#include <cmath>

class scattering_problem
{
public:
	using vector_type = std::array<double,2>;

	scattering_problem(const magnumpi::json_type& cnf, const magnumpi::potential& V)
	 : m_V(V), m_m(in_si(cnf.at("mass"),magnumpi::units::mass))
	{}
	void operator()(const vector_type& x, const vector_type& v, vector_type &a, double t) const
	{
		double r = std::sqrt(x[0]*x[0]+x[1]*x[1]);
		double a_r_r = force(r)/m_m/r;
		a[0] = x[0]*a_r_r;
		a[1] = x[1]*a_r_r;
	}
	double force(double r) const
	{
		return -m_V.dVdr(r);
	}
	double V(double r) const
	{
		return m_V.value(r);
	}
private:
	const magnumpi::potential& m_V;
	const double m_m;
};

void calculate_trajectory(const magnumpi::json_type& cnf, const magnumpi::potential& V, double b, double E)
{
	using vector_type = scattering_problem::vector_type;

	const double m = in_si(cnf.at("mass"),magnumpi::units::mass);
	const double y0 = in_si(cnf.at("y0"),magnumpi::units::length);
	
	vector_type x{ b, y0 };
	vector_type v{ 0.0, std::sqrt(2*E/m) };
	
	double t = 0.0;
	double r0=std::sqrt(x[0]*x[0]+x[1]*x[1]);
	double r=r0;
	boost::numeric::odeint::velocity_verlet< vector_type > stepper;
	scattering_problem sys(cnf,V);
	do
	{
		const double a_r = sys.force(r)/m;
		const double dx=std::max(1e-13,r);
		const double speed=std::sqrt(v[0]*v[0]+v[1]*v[1]);
		double dt=std::min(dx/speed,speed/std::abs(a_r))/1000;
#define MAGNUMPI_TRAJECTORY_CLIP_DT
#ifdef MAGNUMPI_TRAJECTORY_CLIP_DT
		const double dt_max=1e-23;
		dt=std::max(dt,dt_max);
#endif
		stepper.do_step(sys, std::make_pair(std::ref(x),std::ref(v)), t, dt);
		t += dt;
		r=std::sqrt(x[0]*x[0]+x[1]*x[1]);

		std::cout
			<< t
			<< '\t' << dt
			<< '\t' << r
			<< '\t' << x[0] << '\t' << x[1]
			<< '\t' << v[0] << '\t' << v[1]
			<< '\t' << E
			<< '\t' << (sys.V(r)+0.5*m*speed*speed)/E
			<< std::endl;
	}
	while (r<r0);
}

void calculate_trajectories(const magnumpi::json_type& cnf, const magnumpi::json_type& cnf_pot)
{
	using namespace magnumpi;
	const std::unique_ptr<const magnumpi::potential> V(potential::create(cnf_pot.at("potential")));
	const auto bvals = make_range(cnf.at("impact_parameter"),magnumpi::units::length);
	const auto evals = make_range(cnf.at("energy"),magnumpi::units::energy);
	for (double E : evals)
	{
		for (double b : bvals)
		{
			calculate_trajectory(cnf,*V,b,E);
			std::cout << std::endl << std::endl;
		}
	}
}

int main(int argc, const char* argv[])
try
{
	if (argc!=3)
	{
		throw std::runtime_error("Usage: example_trajectories <settings file> <potential file>");
	}
	const magnumpi::json_type cnf(magnumpi::read_json_from_file(argv[1]));
	const magnumpi::json_type pot(magnumpi::read_json_from_file(argv[2]));
	calculate_trajectories(cnf,pot);
	return 0;
}
catch(std::exception& exc)
{
	std::cerr << exc.what() << std::endl;
	return 1;
}
