#include "ideas/dcs/dcs.h"
#include "ideas/dcs/scat_colonna_dcs.h"
#include "magnumpi/clencurt.h"
#include "magnumpi/potential.h"
#include "magnumpi/consts.h"
#include "magnumpi/interpolation.h"

#include <string>
#include <iostream>
#include <cmath>
#include <vector>
#include <ctime>
#include <sstream>
#include <algorithm>
#include <stdexcept>
#include <limits>

namespace magnumpi {


calc_dcs_base::Ql::Ql(int number_points, double dchi, unsigned l)
 : num_p(number_points)
{
	//first element represents the part from 0 to 0.5 dchi degrees, the center is therefore placed at 0.25 dchi
	DCS.push_back(struct_DCS(dchi/4.0, 0.0*dchi, 0.5*dchi));
	for (unsigned i=1;i<num_p-1;i++)
	{
		DCS.push_back(struct_DCS(i*dchi,(i-0.5)*dchi,(i+0.5)*dchi) );
	}
	//last element represents the part from 180-dchi/2 to 180 degrees, the center is therefore placed at 180-dchi/4
	DCS.push_back(struct_DCS(180.0-dchi/4.0, 180.0 - dchi/2.0, 180.0));
}

calc_dcs_base::calc_dcs_base(int lower,int upper,
	double E_low,
	int l_max, double rmin,double rmax,
	int use_scat_Col,int interpol,
	double tol,double red_mass)
 : m_rmin(rmin),m_rmax(rmax),
   m_use_scat_Colonna(use_scat_Col),
   m_interp(interpol),
   m_tolerance(tol),
   m_integrator(-1.0,1.0,lower,upper),
   m_l_max(l_max),
   m_reduced_mass(red_mass)
{
}

void calc_dcs_base::Viehland_DCS_case_1_sub(const potential* p, double y, double energy, int i, int mult, const std::vector<double>& b_vec, const std::vector<double>& r_vec, std::vector<double>& b,
	std::vector<double>& chi, std::vector<double>& eta, std::vector<double>& dbdchi) const
{
	//part 1: y=1 -> b=0	y=-1 -> b=b_vec
	//omit orbiting calculation
	double chi_temp,b_temp,eta_temp, dchidb_term;
	if (y>-1)
	{
		b_temp= b_vec[0] *cos(Constant::pi *(y+1.0)/4.0);
	}
	else
	{
		b_temp=b_vec[0];
	}
	//check if expected scattering angle is zero
	//if ( i % 2 == 0 && )
	scat_Colonna_DCS(chi_temp, dchidb_term, eta_temp, p, b_temp, energy, m_tolerance, m_interp, m_reduced_mass);
	b[i*mult]=b_temp;
	chi[i*mult]=chi_temp;
	eta[i*mult]=eta_temp;
	dbdchi[i*mult]= dchidb_term == 0.0 ? 0.0 : 1.0 / dchidb_term;

	//part 2
	if (b_vec.size()>1)
	{
		for (unsigned j=0; j<b_vec.size()-1; ++j)
		{
			//average radius, eq 26
			double r1,xi,x1,r2,x2;
			double r_avr=(r_vec[j]+r_vec[j+1])/2.0;

				//part 1: y=1 -> r=r_avr y=-1 -> r=r_vec_j
				xi=4.0/Constant::pi*asin(r_vec[j]/r_avr)-1.0;
				int start_ind=(pow(2,m_integrator.upper())+1)*(2*(j+1));

			//omit orbiting calculation
			if (y>-1)
			{
				x1=( (1.0-xi)*y+xi+1.0)/2.0;
				r1=r_avr*sin(Constant::pi/4.0*(x1+1.0));
					b_temp=r1*sqrt(1.0-p->value(r1)/energy);
			}
			else
			{
				b_temp=b_vec[j];
			}
			scat_Colonna_DCS(chi_temp, dchidb_term, eta_temp, p, b_temp, energy, m_tolerance, m_interp, m_reduced_mass);
			b[start_ind-1-i*mult]=b_temp;
			chi[start_ind-1-i*mult]=chi_temp;
			eta[start_ind-1-i*mult]=eta_temp;
			dbdchi[start_ind-1-i*mult]= dchidb_term == 0.0 ? 0.0 : 1.0 / dchidb_term;

			//part 2: y=1 -> r=r_avr y=-1 -> r=r_vec_j+1
			xi=4.0/Constant::pi*acos(r_avr/ r_vec[j+1])-1.0;
			//start_ind=(pow(2,m_integrator.upper())+1)*(2*j+2)-1;
			if (y>-1)
			{
				//omit orbiting calculation
				x2=( (1.0+xi)*y+xi-1.0)/2.0;
				r2=r_vec[j+1]*cos(Constant::pi/4.0*(x2+1.0));
					b_temp=r2*sqrt(1.0-p->value(r2)/energy);
			}
			else
			{
				b_temp=b_vec[j+1];
			}
			scat_Colonna_DCS(chi_temp, dchidb_term, eta_temp, p, b_temp, energy, m_tolerance, m_interp, m_reduced_mass);
			b[start_ind+i*mult]=b_temp;
			chi[start_ind+i*mult]=chi_temp;
			eta[start_ind+i*mult]=eta_temp;
			dbdchi[start_ind+i*mult] = dchidb_term == 0.0 ? 0.0 : 1.0 / dchidb_term;
		}
	}

	//part 3: y=1 -> r=r_vec_j y=-1 -> r=Infinite
	double r3;
	int start_ind=(pow(2,m_integrator.upper())+1)*(2*(r_vec.size()-1)+1);
	r3=r_vec[r_vec.size()-1]/sin(Constant::pi/4.0*(1.0+y));
	if (y==-1)
		b_temp=1e100;
	else
		b_temp=r3*sqrt(1.0-p->value(r3)/energy);
	b[start_ind+i*mult]=b_temp;
	if (y>-1.0 )
	{
		scat_Colonna_DCS(chi_temp, dchidb_term, eta_temp, p, b_temp, energy, m_tolerance, m_interp, m_reduced_mass);
		chi[start_ind+i*mult]=chi_temp;
		eta[start_ind+i*mult]=eta_temp;
		dbdchi[start_ind+i*mult]=dchidb_term == 0.0 ? 0.0 : 1.0 / dchidb_term;
	}
	else
	{
		chi[start_ind+i*mult]=0.0;
		eta[start_ind+i*mult]=0.0;
		dbdchi[start_ind+i*mult]=0.0;
	}
}

void calc_dcs_base::Viehland_DCS_case_2_sub(const potential* p, double y, double energy, int i, int mult, int n, double rc_tilde, double r0_tilde, std::vector<double>& b, std::vector<double>& chi, std::vector<double>& eta, std::vector<double>& dbdchi) const
{
	//part 1: y=-1 -> r=r0_tilde; y=1 -> r=rc_tilde
	double r4, chi_temp, b_temp, eta_temp, dchidb_term;
	//omit orbiting calculation
	if (y>-1)
	{
			r4=((rc_tilde-r0_tilde)*y+rc_tilde+r0_tilde)/2.0;
			b_temp=r4*sqrt(1.0-p->value(r4)/energy);
	}
	else
	{
			b_temp=0.0;
	}
	scat_Colonna_DCS(chi_temp, dchidb_term, eta_temp, p, b_temp, energy, m_tolerance, m_interp, m_reduced_mass);
	b[n-1-i*mult]=b_temp;
	chi[n-1-i*mult]=chi_temp;
	eta[n-1-i*mult]=eta_temp;
	dbdchi[n-1-i*mult]= dchidb_term == 0.0 ? 0.0 : 1.0 / dchidb_term;

	//part 2: y=1 -> r=rc_tilde; y=-1 -> r=infinity
	double r5;
	//omit orbiting calculation
	if (y>-1)
	{
			r5=rc_tilde/sin(Constant::pi*(1.0+y)/4.0);
			b_temp=r5*sqrt(1.0-p->value(r5)/energy);
			scat_Colonna_DCS(chi_temp, dchidb_term, eta_temp, p, b_temp, energy, m_tolerance, m_interp, m_reduced_mass);
	}
	else
	{
		b_temp= 1e100;
		chi_temp=0;
		eta_temp=0;
		dchidb_term = 0.0;
	}
	b[n+i*mult]=b_temp;
	//gerade results
	chi[n+i*mult]= chi_temp;
	eta[n+i*mult]= eta_temp;
	dbdchi[n+i*mult]= dchidb_term == 0.0 ? 0.0 : 1.0 / dchidb_term;
}

void calc_dcs_base::Viehland_DCS_case_3_sub(const potential* p, double y, double energy, int i, int mult, int n, double b_split, std::vector<double>& b,
	std::vector<double>& chi, std::vector<double>& eta, std::vector<double>& dbdchi ) const
{
	//part 1: y=1 -> b=b_split ;y=-1 -> b=0
	double b_temp, chi_temp, eta_temp, dchidb_term;
	if (y==-1)
	{
		b_temp=0.0;
	}
	else if (y==1)
	{
		b_temp=b_split;
	}
	else
	{
		b_temp=b_split/2.0*(y+1.0);
	}

	scat_Colonna_DCS(chi_temp, dchidb_term, eta_temp, p, b_temp, energy, m_tolerance, m_interp, m_reduced_mass);

	//change order
	b[n-1-i*mult]=b_temp;
	chi[n-1-i*mult]=chi_temp;
	eta[n-1-i*mult]=eta_temp;
	dbdchi[n-1-i*mult]= dchidb_term == 0.0 ? 0.0 : 1.0 / dchidb_term;

	//part 2:y=1 -> b=b_split; y=-1 -> b=infinity
	if (y>-1)
	{
		b_temp=2.0*b_split/(y+1.0);
		scat_Colonna_DCS(chi_temp, dchidb_term, eta_temp, p, b_temp, energy, m_tolerance, m_interp, m_reduced_mass);
	}
	else
	{
		b_temp=1e100;
		chi_temp=0;
		eta_temp=0;
		dchidb_term=0.0;
	}
	b[n+i*mult]=b_temp;
	chi[n+i*mult]=chi_temp;
	eta[n+i*mult]=eta_temp;
	dbdchi[n+i*mult]= dchidb_term == 0.0 ? 0.0 : 1.0 / dchidb_term;
}

calc_dcs::calc_dcs(int lower,int upper,
		double E_low,
		int l_max, double rmin,double rmax,double dr,
		int use_scat_Col,int interpol,
		double tol,double red_mass, const potential* pot)
 : calc_dcs_base(lower,upper,E_low,l_max,rmin,rmax,use_scat_Col,interpol,tol,red_mass),
   m_pot(pot),
   m_triplets(m_pot, rmin, rmax, dr, E_low)
{
}

std::pair<data_set,data_set> calc_dcs::DCS(const std::vector<double>& energy_vals, unsigned num_chi) const
{
	const double dchi=180.0/(num_chi-1);

	std::cout << "Using " << num_chi << "angles." << std::endl;
	std::cout << "Using angular steps of " << dchi << std::endl;

	std::vector<calc_dcs_base::Ql> DCS_new;
	std::vector<calc_dcs_base::Ql> DCS_old;

	for (unsigned l=1;l<=m_l_max;++l)
	{
		DCS_new.push_back(Ql(num_chi, dchi, l));
		DCS_old.push_back(Ql(num_chi, dchi, l));
	}

	data_set dcs_data(
		"dsigma_dOmega", units::area, "m^2/sr",
		{"chi", units::angle, "rad"},
		{"epsilon", units::energy, "J"} );
	// while we use SI internally, in output we prefer to write eV for the energies
	dcs_data.col_axis().set_display_unit("eV");
	/// \todo angles in degree or rad?
	// 1. set up the angular axis
	for (unsigned i=0;i<DCS_new[0].size_DCS();++i)
	{
		double chi = DCS_new[0].DCS[i].chi();
		dcs_data.row_axis() << chi;
	}

	data_set cs_data(
		"sigma_odd_even", units::area, "m^2",
		{"epsilon", units::energy, "J"},
		{"l"} );
	// while we use SI internally, in output we prefer to write eV for the energies
	cs_data.row_axis().set_display_unit("eV");
	for (unsigned l=1;l<=m_l_max;++l)
	{
		std::stringstream ss;
		ss << l;
		cs_data.add_column(ss.str());
	}

	//double b1,rc_tilde,r0_tilde;
	const double convert = Constant::pi / 180.0;

	for (unsigned e=0;e!=energy_vals.size();++e)
	{
		const double energy = energy_vals[e];
		//calculate DCS
		Viehland_DCS_cases(energy, DCS_new, DCS_old);

		dcs_data.add_column(energy);
		cs_data.add_row(energy);

		std::vector<double> cs(m_l_max,0.0);

		for (unsigned i=0;i<DCS_new[0].size_DCS();++i)
		{
			double chi = DCS_new[0].DCS[i].chi();

			dcs_data(i,e) = DCS_new[0].DCS[i].dcs() / (2.0 * Constant::pi * sin(chi * convert));

			for (unsigned j=0;j<DCS_new.size();++j)
			{
				const double dchi = DCS_new[j].DCS[i].bnd_up() - DCS_new[j].DCS[i].bnd_low();
				cs[j] += (1.0 - pow(cos(chi),j+1)) * DCS_new[j].DCS[i].dcs() * dchi * convert;
			}
		}

		for (unsigned j=1;j<=m_l_max;++j)
		{
			cs_data(e,j-1) = cs[j-1];
		}
	}
	return { std::move(dcs_data), std::move(cs_data) };
}

void calc_dcs::Viehland_DCS_cases(double energy,std::vector<calc_dcs_base::Ql>& DCS_new,std::vector<calc_dcs_base::Ql>& DCS_old) const
{
	//determine which case is applicable
	const triplet_data::case_data data{m_triplets, energy};

	//initialize variables
	const double init_val=std::numeric_limits<double>::quiet_NaN();
	const int n=pow(2,m_integrator.upper())+1;
	//ungerade
	const int s = (data.case_id==1) ?  2+2*(data.b_vec.size()-1) : 2;
	std::vector<double> b(n*s,init_val);
	std::vector<double> chi(n*s,init_val);
	std::vector<double> eta(n*s,init_val);
	std::vector<double> dbdchi(n*s,init_val);

	std::cout << "E= " << energy << "\tcase " << data.case_id << std::endl;

	//required for testing purposes (direct integration of cross section)
	std::vector<double> Ql_prev(DCS_new.size(),0.0);
	std::vector<double> Ql_iter(DCS_new.size(),0.0);

	//initialize iteration variables
	unsigned index_cc=m_integrator.lower();
	double residue = 1.0;
	//run calculation with improved resolution
	while (index_cc<=m_integrator.upper() && residue > m_tolerance)
	{
		const int mult=pow(2,m_integrator.upper()-index_cc);
		const int last=pow(2,index_cc)+1;
		//set start and step indices for output vector
		int start,step;
		if (index_cc==m_integrator.lower())
		{
			start=0;
			step=1;
		}
		else
		{
			start=1;
			step=2;
		}
		for (int i=start;i<last;i+=step)
		{
			const double y=m_integrator.cc_position(index_cc,i);
			if (data.case_id==1)
			{
				Viehland_DCS_case_1_sub(m_pot, y, energy, i, mult, data.b_vec, data.r_vec, b, chi, eta, dbdchi );
			}
			else if (data.case_id==2)
			{
				Viehland_DCS_case_2_sub(m_pot, y, energy, i, mult, n, data.rc_tilde, data.r0_tilde, b, chi, eta, dbdchi );
			}
			else if (data.case_id==3)
			{
				Viehland_DCS_case_3_sub(m_pot, y, energy, i, mult, n, data.b_split, b, chi, eta, dbdchi);
			}
		}
		//evaluate DCS
		std::cout << "index\t" << index_cc;
		residue=evaluate_DCS(index_cc,s, DCS_new, DCS_old, b, chi, dbdchi);

		//////////////////////////
		//write results for the direct integration of the cross section
		/*
		double res_alt=0.0;
		for (unsigned i=0; i<DCS_new.size();++i)
		{
			Ql_iter[i]=verify_Ql(b, chi, i+1, index_cc);
			//calculate max residue
			if (index_cc>m_integrator.lower())
				res_alt = std::max(res_alt, std::abs( (Ql_iter[i]-Ql_prev[i])/Ql_iter[i]) );
			else
				res_alt = 1.0;
			Ql_prev[i]=Ql_iter[i];
		}
		std::cout << "index\t" << index_cc << "\tresidue\t" << res_alt;
		for (unsigned i=0; i<DCS_new.size();++i)
		{
			std::cout << "\t" << Ql_iter[i];
		}
		std::cout << std::endl;
		// */
		/////////////////////////
		/*
		//write raw results
		if (energy==1e-4)
		{
			std::string file="raw.txt";
			std::ofstream fileout(file);
			if (index_cc<=m_integrator.upper())
				mult=std::pow(2,m_integrator.upper()-index_cc);
			else
				mult=1;

			//interpolate gerade results to ungerade b-grid
			step = 1;
			unsigned n = std::pow(2,m_integrator.upper())+1;
			for (unsigned j=0;j<b.size()/n;++j)
			{
				for (unsigned i=0;i<n;i+=mult)
				{
					fileout << std::scientific << energy << "\t" << b[j*n+i] << "\t" << chi[j*n+i] << "\t" << dbdchi[j*n+i] << std::endl;
				}
			}
		}
		// */

		//increase index
		++index_cc;
	}

	//warning for not converged results
	if (index_cc>m_integrator.upper() && residue>m_tolerance)
	{
		std::cout << "Result not converged. Residue: " << residue << std::endl;
	}
}

double calc_dcs::evaluate_DCS(int index_cc,int s, std::vector<Ql>& DCS_new, std::vector<Ql>& DCS_old,std::vector<double>& b,
	std::vector<double>& chi, std::vector<double>& dbdchi) const
{
	//radiance to degrees
	double deg_to_rad= Constant::pi / 180.0;
	double rad_to_deg= 180.0 / Constant::pi;
	//int num_angle = 180.0 / dchi_DCS + 1;
	//determine step size
	double dchi_DCS=(DCS_new[0].DCS[2].chi()-DCS_new[0].DCS[1].chi());
	int int_360 = 360.0 / dchi_DCS;
	double local_dchi;
	double tol_dchi = 1e-10;
	//store old result for DCS and reset DCS_new
	for (unsigned j=0;j<DCS_new.size();++j)
	{
		for (unsigned i=0;i<DCS_new[j].size_DCS();++i)
		{
			DCS_old[j].DCS[i].set_DCS(DCS_new[j].DCS[i].dcs());
			DCS_new[j].DCS[i].set_DCS(0.0);
		}
	}
	int step=pow(2,m_integrator.upper()-index_cc);
	int last=pow(2,m_integrator.upper())+1;
	double dchi=0.0;
	double chi_u=0.0;
	double chi_l=0.0;
	//double chi_temp = 0.0;
	double DCS, constant, dchi_step;
	int lower=0;
	int upper=0;
	std::vector<double> test_cs(DCS_new.size(),0.0);
	double dbdchi_temp = 0.0;

	for (int i=0; i<last;i+=step)
	{
		for (int j=0;j<s;++j)
		{
			DCS=0.0;
			//if ( dbdchi[j*last+i] != 0)
			//{
				//determine chi_u and chi_l
				if (i==0)
				{
					chi_u= (chi[j*last+i+step] + chi[j*last+i] )/2.0;
					chi_l= chi[j*last+i];
					//chi_temp = chi[j*last+i];
					//dchi = std::abs( chi_integral[j*last+i+step] - chi_integral[j*last+i] ) /2.0;
				}
				else if (i==last-1)
				{
					chi_l= (chi[j*last+i] + chi[j*last+i-step] )/2.0;
					chi_u= chi[j*last+i];
					//chi_temp = chi[j*last+i];
					//dchi = std::abs( chi_integral[j*last+i] - chi_integral[j*last+i-step]) / 2.0;
				}
				else
				{
					chi_u= (chi[j*last+i+step] + chi[j*last+i] )/2.0;
					chi_l= (chi[j*last+i] + chi[j*last+i-step] )/2.0;
					//chi_temp = chi[j*last+i];
					//dchi = std::abs( chi_integral[j*last+i+step] - chi_integral[j*last+i-step] ) / 2.0;
				}
			/*}
			else	{
				chi_l = 0.0;
				chi_u = 0.0;
				chi_temp = 0.0;
			}
			*/

			if (chi_l > chi_u)
				std::swap(chi_u,chi_l);

			dchi=chi_u-chi_l;
			lower = std::round(chi_l*rad_to_deg/dchi_DCS);
			upper = std::round(chi_u*rad_to_deg/dchi_DCS);

			if (dchi>0.0 && std::abs(chi[j*last+i])>0.0 )
			{
				//2* pi * sin(chi) *b *db/(dchi * sin(chi)) = 2 * pi b *db/dchi
				constant= 2.0 * Constant::pi * std::abs(b[j*last+i] * dbdchi[j*last+i]);
				if (i==0)
					dbdchi_temp = (b[j*last+i+step] - b[j*last+i]) / dchi / 2.0;
				else if (i==last-step)
					dbdchi_temp = (b[j*last+i] - b[j*last+i-step]) / dchi / 2.0;
				else
					dbdchi_temp = (b[j*last+i+step] - b[j*last+i-step]) / dchi / 2.0;

				if (std::isnan(dbdchi_temp)==true)
					dbdchi_temp = 0.0;

				//constant= 2.0 * Constant::pi * b[j*last+i] * dbdchi_temp;
				//constant= b[j*last+i] / sin(chi_temp) * dbdchi_temp;
				//std::cout << i << "\t" << j << "\t" << b[j*last+i] << "\t" << chi[j*last+i] << "\t" << dbdchi_temp << "\t" << constant << std::endl;
			}
			else	{
				constant=0.0;
				dbdchi_temp = 0.0;
			}

			int index;
			double test = 0.0;
			int count = lower;

			while (count <= upper && dchi>0.0)
			{
				//get correct index
				index = std::abs(count) % int_360;
				//index = std::fmod(std::abs(count), 2.0*Constant::pi);
				if (index> (int) DCS_new[0].DCS.size()-1 )
					index = std::abs(index-int_360);

				local_dchi=(DCS_new[0].DCS[index].bnd_up()-DCS_new[0].DCS[index].bnd_low()) * deg_to_rad;
				if ( count == lower)
				{
					if (lower==upper) {
						//dchi fits entirely in cell given by local_dchi
						dchi_step=dchi;
						//method = 1;
						//std::cout << "method 1" << std::endl;
					}
					else {
						//difference from chi_l till upper boundary of the cell
						dchi_step = dchi_DCS * (lower + 0.5 ) * deg_to_rad - chi_l;
						//dchi_step = DCS_new[0].DCS[index].bnd_up() * convert - chi_l;
						//method = 2;
						//std::cout << "method 2" << std::endl;
					}
				}
				else if (count == upper)
				{
					//difference from lower boundary till chi_u
					dchi_step = chi_u - (upper - 0.5 ) * dchi_DCS * deg_to_rad;
					//method = 3;
					//std::cout << "method 3" << std::endl;
				}
				else
				{
					//for example: from 179.5 to 180.0 and back from 180.0 to 179.5
					if (index==0 || index==(int)DCS_new[0].size_DCS()-1)	{
						dchi_step=2.0*local_dchi;
						//method=4;
						//std::cout << "method 4" << std::endl;
					}
					else	{
						dchi_step=local_dchi;
						//method =5;
						//std::cout << "method 5" << std::endl;
					}
				}

				DCS=constant * dchi_step / local_dchi;

				//chi_temp = DCS_new[0].DCS[index].chi();
				for (unsigned k=0;k<DCS_new.size();++k)
				{
					//classical expression
					DCS_new[k].DCS[index].add( DCS);
				}

				//check integration step
				test += dchi_step;
				++ count;
			}
			//*
			if (dchi >0.0 && std::abs(test / dchi- 1.0) > tol_dchi)
			{
				std:: cout << "Error in integration of dchi. Dchi is not conserved." << std::endl;
				std::cout << " Ratio: " << test / dchi << std::endl;
				std::cout << "Binned dchi " << test << std::endl;
				std::cout << "dchi " << dchi << std::endl;
				std::cout << "Ratio -1 " << test/dchi -1.0 << std::endl;
				throw std::runtime_error("Error in integration of dchi");
			}
			// */
			/*
			for (unsigned k=0;k<test_cs.size();++k)
			{
				//classical expression
				//test_cs[k] += 2.0 * Constant::pi * b[j*last+i] * dbdchi_temp * (1.0 - pow(cos(chi_temp),k+1)) * dchi;
				test_cs[k] += constant * (1.0 - pow(cos(chi_temp),k+1)) * dchi;
			}
			*/
		}
	}
	/*
	for (unsigned k=0;k<DCS_new.size();++k)
	{
		std::cout << test_cs[k] << std::endl;
	}
	*/
	//calculate relative change
	double residue=0.0;
	//compare calculated transport cross sections with the previous ones
	residue = DCS_residue(DCS_new, DCS_old);
	return residue;
}

double calc_dcs::DCS_residue(const std::vector<calc_dcs_base::Ql>& DCS_new, const std::vector<calc_dcs_base::Ql>& DCS_old) const
{
	//write DCS to file
	std::vector<double> cs_new(m_l_max,0.0);
	std::vector<double> cs_old(m_l_max,0.0);
	double residue=0.0;

	const double deg_to_rad = Constant::pi / 180.0;
	for (unsigned j=0;j<cs_new.size();++j)
	{
		for (unsigned i=0;i<DCS_new[0].size_DCS();++i)
		{
			const double dchi = (DCS_new[0].DCS[i].bnd_up() - DCS_new[0].DCS[i].bnd_low()) * deg_to_rad;
			const double chi = DCS_new[0].DCS[i].chi() * deg_to_rad;

			//cs_new[j] += 2.0 * Constant::pi * sin(chi) * DCS_new[0].DCS[i].dcs() * (1.0 - pow(cos(chi),j+1)) * dchi;
			//cs_old[j] += 2.0 * Constant::pi * sin(chi) * DCS_old[0].DCS[i].dcs() * (1.0 - pow(cos(chi),j+1)) * dchi;

			cs_new[j] += DCS_new[j].DCS[i].dcs() * (1.0 - pow(cos(chi),j+1)) * dchi;
			cs_old[j] += DCS_old[j].DCS[i].dcs() * (1.0 - pow(cos(chi),j+1)) * dchi;

			//determine residue
			residue = std::max(residue, std::abs( (DCS_new[j].DCS[i].dcs() - DCS_old[j].DCS[i].dcs()) / DCS_new[j].DCS[i].dcs()) );

			//write output
			//std::cout << DCS_new[j].DCS[i].chi() << "\t" << DCS_new[j].DCS[i].DCS << "\t" << DCS_old[j].DCS[i].DCS << "\t" <<
			//std::abs( (DCS_new[j].DCS[i].dcs() - DCS_old[j].DCS[i].dcs()) / DCS_new[j].DCS[i].dcs()) << std::endl;
		}
	}
	//calculate residue
	for (unsigned i=0; i<cs_new.size(); ++i)
	{
		residue = std::max(residue, std::abs( (cs_new[i] - cs_old[i])/cs_new[i]) );
	}

	std::cout << "\tresidue\t" << residue;
	for (unsigned i=0; i<cs_new.size();++i)
	{
		std::cout << "\t" << cs_new[i];
	}
	std::cout << std::endl;
	return residue;
}

double calc_dcs::verify_Ql(const std::vector<double>& b, const std::vector<double>& chi,int l,int index_cc) const
{
	double Ql=0.0;
	int step=std::pow(2,m_integrator.upper()-index_cc);
	unsigned n=std::pow(2,m_integrator.upper())+1;
	unsigned s=b.size()/n;
	int ind;
	double db;
	for (unsigned i=0;i<n;i += step)
	{
		for (unsigned j=0; j<s; ++j)
		{
			ind = i + j*n;
			if (chi[ind]!= 0.0)
			{
				if (i==0)
					db = (b[ind+step]-b[ind]);
				else
					db = (b[ind] - b[ind-step]);
				Ql += std::abs(2.0 * Constant::pi * b[ind] * db * (1.0 - std::pow(cos(chi[ind]),l) ));
			}
		}
	}
	return Ql;
}

calc_dcs_rce::QlRCE::QlRCE(int number_points, double dchi, unsigned l)
 : Ql(number_points, dchi, l)
{
	//calculate coefficients (Viehland 1986 eq 10, 11)
	if (l % 2 == 1)
	{
		//odd coefficients
		unsigned k = (l - 1 ) /2;
		//indices from 0 till k are considered
		for (unsigned i=0;i<k+1;++i)
		{
			double value=0.0;
			for (unsigned m=i;m<k+1;++m)
			{
				value += 2.0*(2.0*k+1.0)*(4.0*m+3.0)*permutation(2*i+2*m+2,i+m+1)*permutation(2*m-2*i,m-i)*permutation(2*k,k-m)/
					(std::pow(4.0,m)*(2.0*k+2.0*m+3.0)*(k+m+1.0)*permutation(2*k+2*m+2,k+m+1) );
			}
			coef.push_back(value);
		}
	}
	else
	{
		//even coefficients
		unsigned k = l/2;
		//The first element is not used
		coef.push_back(0.0);
		for (unsigned i=1;i<k+1;++i)
		{
			double value=0.0;
			for (unsigned m=i;m<k+1;++m)
			{
				value += 4.0 / (2.0*k) *(2.0*k+1.0) * (4.0 * m + 1.0) * permutation(2*i+2*m,i+m)*permutation(2*m-2*i,m-i)*permutation(2*k,k-m) /
					(std::pow(4.0,m)*(2.0*k+2.0*m+1.0)*permutation(2*k+2*m,k+m));
			}
			coef.push_back(value);
		}
	}
}

double calc_dcs_rce::QlRCE::permutation(unsigned n,unsigned k) const
{
	double value=1.0;
	for (unsigned i=1;i<k+1;++i)
	{
		value *= (n+1.0-i)/i;
	}
	return value;
}


calc_dcs_rce::calc_dcs_rce(int lower,int upper,
		double E_low,
		int l_max, double rmin,double rmax,double dr,
		int use_scat_Col,int interpol,
		double tol,double red_mass, const potential* pot_gerade,const potential* pot_ungerade)
 : calc_dcs_base(lower,upper,E_low,l_max,rmin,rmax,use_scat_Col,interpol,tol,red_mass),
   m_pot_u(pot_ungerade),
   m_pot_g(pot_gerade),
   m_triplets_u(m_pot_u, rmin, rmax, dr, E_low),
   m_triplets_g(m_pot_g, rmin, rmax, dr, E_low)
{
}

std::vector<data_set> calc_dcs_rce::DCS_RCE(const std::vector<double>& energy_vals, unsigned num_chi) const
{
	const double dchi=180.0/(num_chi-1);

	std::cout << "Using " << num_chi << "angles." << std::endl;
	std::cout << "Using angular steps of " << dchi << std::endl;

	std::vector<QlRCE> DCS_new;
	std::vector<QlRCE> DCS_old;

	std::vector<data_set> dcs_data;
	for (unsigned l=1;l<=m_l_max;++l)
	{
		DCS_new.push_back(QlRCE(num_chi, dchi, l));
		DCS_old.push_back(QlRCE(num_chi, dchi, l));
	}

	// index 0: transport cross sections for all l
	dcs_data.emplace_back(data_set(
		"sigma_odd_even", units::area, "m^2",
		{"epsilon", units::energy, "J"},
		{"l"} ));
	// while we use SI internally, in output we prefer to write eV for the energies
	dcs_data.back().row_axis().set_display_unit("eV");
	for (unsigned l=1;l<=m_l_max;++l)
	{
		std::stringstream ss;
		ss << l;
		dcs_data.back().add_column(ss.str());
	}
	// other indices: dcs for l=1...l_max
	for (unsigned l=1;l<=m_l_max;++l)
	{
		dcs_data.emplace_back(data_set(
			"dsigma_dOmega", units::area, "m^2/sr",
			{"chi", units::angle, "rad"},
			{"epsilon", units::energy, "J"} ));
		// while we use SI internally, in output we prefer to write eV for the energies
		dcs_data.back().col_axis().set_display_unit("eV");
		/// \todo angles in degree or rad?
		// 1. set up the angular axis
		for (unsigned i=0;i<DCS_new[0].size_DCS();++i)
		{
			double chi = DCS_new[0].DCS[i].chi();
			dcs_data.back().row_axis() << chi;
		}
	}
	for (unsigned e=0;e!=energy_vals.size();++e)
	{
		const double energy = energy_vals[e];
		//calculate DCS
		Viehland_DCS_RCE_cases(energy, DCS_new, DCS_old);

		for (unsigned l=1; l!=dcs_data.size(); ++l)
		{
			dcs_data[l].add_column(energy);
		}
		dcs_data[0].add_row(energy);

		std::vector<double> cs(m_l_max,0.0);

		for (unsigned i=0;i<DCS_new[0].size_DCS();++i)
		{
			const double chi = DCS_new[0].DCS[i].chi();
			for (unsigned j=0;j!=cs.size();++j)
			{
				const double convert = Constant::pi / 180.0;
				//divide by 2 * pi * sin(chi) * (1 - [cos(chi)]^l) to turn the calculated quantity back in a DCS
				dcs_data[j+1](i,e) = DCS_new[j].DCS[i].dcs() / (2.0 * Constant::pi * sin(chi * convert) );
				cs[j] += DCS_new[j].DCS[i].dcs() * dchi * convert;
			}
		}

		for (unsigned j=0;j<m_l_max;++j)
		{
			dcs_data[0](e,j) = cs[j];
		}
	}
	return dcs_data;
}

void calc_dcs_rce::Viehland_DCS_RCE_cases(double energy,std::vector<QlRCE>& DCS_new, std::vector<QlRCE>& DCS_old) const
{
	//determine which case is applicable

	const triplet_data::case_data data_u{m_triplets_u, energy};
	const triplet_data::case_data data_g{m_triplets_g, energy};

	//initialize variables
	const double init_val=std::numeric_limits<double>::quiet_NaN();
	unsigned n=pow(2,m_integrator.upper())+1;
	//ungerade
	const int su = (data_u.case_id==1) ? 2+2*(data_u.b_vec.size()-1) : 2;
	std::vector<double> b_u(n*su,init_val);
	std::vector<double> chi_u(n*su,init_val);
	std::vector<double> eta_u(n*su,init_val);
	std::vector<double> dbdchi_u(n*su,init_val);
	//gerade
	const int sg = (data_g.case_id==1) ? 2+2*(data_g.b_vec.size()-1) : 2;
	std::vector<double> b_g(n*sg,init_val);
	std::vector<double> chi_g(n*sg,init_val);
	std::vector<double> eta_g(n*sg,init_val);
	std::vector<double> dbdchi_g(n*sg,init_val);

	// delta_eta: document why this is on the ungerade grid
	std::vector<double> delta_eta(n*su,init_val);

	std::cout << "E= " << energy << "\tgerade case " << data_g.case_id << "\tungerade case " << data_u.case_id << std::endl;

	//required for testing purposes (direct integration of cross section)
	std::vector<double> Ql_prev(DCS_new.size(),0.0);
	std::vector<double> Ql_iter(DCS_new.size(),0.0);

	//initialize iteration variables
	unsigned index_cc=m_integrator.lower();
	double residue = 1.0;
	while (index_cc<=m_integrator.upper() && residue > m_tolerance)
	{
		const int mult=pow(2,m_integrator.upper()-index_cc);
		const int last=pow(2,index_cc)+1;
		//set start and step indices for output vector
		int start,step;
		if (index_cc==m_integrator.lower())
		{
			start=0;
			step=1;
		}
		else
		{
			start=1;
			step=2;
		}
		for (int i=start;i<last;i+=step)
		{
			const double y=m_integrator.cc_position(index_cc,i);
			//ungerade
			if (data_u.case_id==1)
			{
				Viehland_DCS_case_1_sub(m_pot_u, y, energy, i, mult, data_u.b_vec, data_u.r_vec, b_u, chi_u, eta_u , dbdchi_u );
			}
			else if (data_u.case_id==2)
			{
				Viehland_DCS_case_2_sub(m_pot_u, y, energy, i, mult, n, data_u.rc_tilde, data_u.r0_tilde, b_u, chi_u, eta_u, dbdchi_u );
			}
			else if (data_u.case_id==3)
			{
				Viehland_DCS_case_3_sub(m_pot_u, y, energy, i, mult, n, data_u.b_split, b_u, chi_u, eta_u, dbdchi_u );
			}

			//gerade
			if (data_g.case_id==1)
			{
				Viehland_DCS_case_1_sub(m_pot_g, y, energy, i, mult, data_g.b_vec, data_g.r_vec, b_g, chi_g, eta_g, dbdchi_g );
			}
			else if (data_g.case_id==2)
			{
				Viehland_DCS_case_2_sub(m_pot_g, y, energy, i, mult, n, data_g.rc_tilde, data_g.r0_tilde, b_g, chi_g, eta_g, dbdchi_g );
			}
			else if (data_g.case_id==3)
			{
				Viehland_DCS_case_3_sub(m_pot_g, y, energy, i, mult, n, data_g.b_split, b_g, chi_g, eta_g, dbdchi_g );
			}
		}
		std::cout << "index\t" << index_cc;
		residue=evaluate_DCS(index_cc,su,energy, DCS_new, DCS_old, b_g, chi_g, dbdchi_g, b_u, chi_u, dbdchi_u, delta_eta);

		//evaluate convergence of the momentum transfer cross section
		/*
		double Ql_mom =	DCS_new[0].calc_Ql();
		double sum= verify_Ql(b_u, chi_u, 1, index_cc);
		double sum2= verify_Ql_DCS(b_u, chi_u, dbdchi_u , 1, index_cc);
		std::cout << "index " << index_cc << " Q(1) " << Ql_mom << " sum 2*pi*b_u*db_u*(1-cos chi_u) " << sum <<
			" sum 2*pi*DCS*sin chi_u *(1-cos chi_u) *dchi_u " << sum2 << std::endl;
		// */

		//////////////////////////
		/*
		//write results for the direct integration of the cross section
		double res_alt=0.0;
		for (unsigned i=0; i<DCS_new.size();++i)
		{
			Ql_iter[i]=verify_Ql(b_g, chi_g, b_u, chi_u, delta_eta, i+1, index_cc);
			//calculate max residue
			if (index_cc>m_integrator.lower())
				res_alt = std::max(res_alt, std::abs( (Ql_iter[i]-Ql_prev[i])/Ql_iter[i]) );
			else
				res_alt = 1.0;
			Ql_prev[i]=Ql_iter[i];
		}
		std::cout << "index\t" << index_cc << "\tresidue\t" << res_alt;
		for (unsigned i=0; i<DCS_new.size();++i)
		{
			std::cout << "\t" << Ql_iter[i];
		}
		std::cout << std::endl;
		*/
		/////////////////////////

		//increase index
		++index_cc;
	}
	//warning for not converged results
	if (index_cc>m_integrator.upper() && residue>m_tolerance)
		std::cout << "Result not converged. Residue: " << residue << std::endl;

	/*
	//write raw results
	if (energy==1)
	{
		std::string file="raw.txt";
		std::ofstream fileout(file);
		if (index_cc<=m_integrator.upper())
			mult=std::pow(2,m_integrator.upper()-index_cc);
		else
			mult=1;

		//interpolate gerade results to ungerade b-grid
		std::vector<double> chig(b_u.size(),666);
		std::vector<double> etag(b_u.size(),666);
		std::vector<double> dbdchig(b_u.size(),666);
		step = mult;
		vector_linear_interp1(b_g, chi_g, b_u, chig, step, n);
		vector_linear_interp1(b_g, eta_g, b_u, etag, step, n);
		vector_linear_interp1(b_g, dbdchi_g, b_u, dbdchig, step, n);

		for (unsigned j=0;j<b_u.size()/n;++j)
		{
			for (unsigned i=0;i<n;i=i+mult)
			{
				unsigned k = j*n + i;
				fileout << std::scientific << energy << "\t" << b_u[k] << "\t" << chi_u[k] << "\t" << eta_u[k] << "\t" << dbdchi_u[k] <<
					"\t" << chig[k] << "\t" << etag[k] << "\t" << dbdchig[k] << "\t"
					 << delta_eta[k] << "\t" << sin(2.0*(delta_eta[k])) << "\t" << std::pow(sin(delta_eta[k]),2) << std::endl;
			}
		}
	}
	// */
}

double calc_dcs_rce::evaluate_DCS(int index_cc,int s, double energy, std::vector<QlRCE>& DCS_new, std::vector<QlRCE>& DCS_old,std::vector<double>& bg_in,
	std::vector<double>& chig_in, std::vector<double>& dbdchig_in, std::vector<double>& bu, std::vector<double>& chiu, std::vector<double>& dbdchiu, std::vector<double>& delta_eta) const
{
	//determine step size
	double dchi_DCS=DCS_new[0].DCS[2].chi()-DCS_new[0].DCS[1].chi();
	double local_dchi;
	double tol_dchi = 1e-10;
	int int_360 = 360.0 / dchi_DCS;
	//store old result for DCS and reset DCS_new
	for (unsigned j=0;j<DCS_new.size();++j)
	{
		for (unsigned i=0;i<DCS_new[j].size_DCS();++i)
		{
			DCS_old[j].DCS[i].set_DCS(DCS_new[j].DCS[i].dcs());
			DCS_new[j].DCS[i].set_DCS(0.0);
		}
	}
	//radiance to degrees
	double rad_to_deg= 180.0 / Constant::pi;
	double deg_to_rad= Constant::pi / 180.0;
	int step=pow(2,m_integrator.upper()-index_cc);
	int last=pow(2,m_integrator.upper())+1;
	double dchi=0.0;
	double chi_u=0.0;
	double chi_l=0.0;
	double chi=0.0;
	double DCS, constant, dchi_step;
	//double dbdchi_temp = 0.0;
	int lower=0;
	int upper=0;
	//std::vector<double> test_cs(m_l_max,0.0);
	double DCS_odd, DCS_quantum, quantum_factor, odd_factor;

	//interpolate gerade data to ungerade grid
	std::vector<double> bg(bu.size(),0.0);
	std::vector<double> chig(bu.size(),0.0);
	std::vector<double> dbdchig(bu.size(),0.0);
	//interpolation
	for (unsigned i=0;i<bu.size();++i)
	{
		bg[i]=bu[i];
	}

	/*
	for (unsigned j=0;j<bg_in.size()/last;++j)
	{
		for (unsigned i=0; i<last; i+=step)
		{
			unsigned ind = j*last + i;
			std::cout << bg_in[ind] << "\t" << chig_in[ind] << "\t" << etag_in[ind] << "\t" << dbdchig_in[ind] << std::endl;
		}
	}
	*/
	vector_linear_interp1(bg_in, chig_in, bu, chig, step, last);
	//vector_linear_interp1(bg_in, etag_in, bu, etag, step, last);
	vector_linear_interp1(bg_in, dbdchig_in, bu, dbdchig, step, last);
	//direct evaluation of delta eta
	for (unsigned j=0; j<bu.size()/last; ++j)
	{
		for (int i=0; i<last; i+=step)
		{
			delta_eta[j*last+i] = calc_eta( m_pot_u, m_pot_g, m_reduced_mass, bu[j*last+i], energy, m_tolerance, m_interp );
		}
	}

	/*
	for (unsigned j=0; j<bg.size()/last; ++j)
	{
		for (int i=0; i<last; i += step)
		{
			unsigned ind = j*last + i;
			std::cout << bg[ind] << "\t" << chig[ind] << "\t" << dbdchig[ind] << std::endl;
		}
	}
	*/

	for (int i=0; i<last;i+=step)
	{

		for (int j=0;j<s;++j)
		{
			//gerade contribution
			DCS=0.0;
			//DCS_odd=0.0;
			if ( dbdchig[j*last+i] != 0)
			{
				//determine chi_u and chi_l
				if (i==0)
				{
					chi_u= (chig[j*last+i+step] + chig[j*last+i] )/2.0;
					chi_l= chig[j*last+i];
					//dchi = std::abs(chig_integral[j*last+i+step]-chig_integral[j*last+i] ) / 2.0;
				}
				else if (i==last-1)
				{
					chi_l= (chig[j*last+i] + chig[j*last+i-step] )/2.0;
					chi_u= chig[j*last+i];
					//dchi = std::abs( chig_integral[j*last+i] - chig_integral[j*last+i-step] ) / 2.0;
				}
				else
				{
					chi_u= (chig[j*last+i+step] + chig[j*last+i] )/2.0;
					chi_l= (chig[j*last+i] + chig[j*last+i-step] )/2.0;
					//dchi = std::abs( chig_integral[j*last+i+step] - chig_integral[j*last+i-step]) / 2.0;
				}
				chi = chig[j*last+i];
			}
			else	{
				chi_l=0.0;
				chi_u=0.0;
				chi = 0.0;
				dchi = 0.0;
			}

			if (chi_l > chi_u)
				std::swap(chi_u,chi_l);

			dchi=chi_u-chi_l;
			lower = std::round(chi_l * rad_to_deg / dchi_DCS);
			upper = std::round(chi_u * rad_to_deg / dchi_DCS);

			if (dchi>0.0 && std::abs(chig[j*last+i])>0.0 && std::isinf(bg[j*last+i])==false )
			{
				//2* pi * sin(chi) *b *db/(dchi * sin(chi)) = 2 * pi b *db/dchi
				/*
				if (i==0)
					dbdchi_temp = (bg[j*last+i+step] - bg[j*last+i]) / dchi / 2.0;
				else if (i==last-step)
					dbdchi_temp = (bg[j*last+i] - bg[j*last+i-step]) / dchi / 2.0;
				else
					dbdchi_temp = (bg[j*last+i+step] - bg[j*last+i-step]) / dchi / 2.0;

				if (std::isnan(dbdchi_temp)==true)
					dbdchi_temp = 0.0;

				constant=2.0 * Constant::pi * std::abs(bg[j*last+i] * dbdchi_temp);
				*/
				constant=2.0 * Constant::pi * std::abs(bg[j*last+i] * dbdchig[j*last+i]);
			}
			else	{
				constant=0.0;
				//dbdchi_temp = 0.0;
			}
			//*
			if (std::isnan(delta_eta[j*last+i])==0 )	{
				odd_factor= (1.0 - 2.0*pow(sin(delta_eta[j*last+i]),2 ) )/2.0;
				quantum_factor = sin(2.0*delta_eta[j*last+i])/4.0;
			}
			else	{
				odd_factor=0.0;
				quantum_factor=0.0;
			}
			// */
			//calculate resonant charge exchange cross section
			//DCS_RCE_term = constant * std::pow(sin(delta_eta[j*last+i]),2.0);
			double RCE = 2.0 * pow(sin(delta_eta[j*last+i]),2.0);

			int index;
			double test = 0.0;
			int count = lower;

			while (count <= upper && dchi>0.0)
			{
				//get correct index
				index = std::abs(count) % int_360;
				if (index> (int) DCS_new[0].DCS.size()-1 )
					index = std::abs(index-int_360);
				local_dchi=(DCS_new[0].DCS[index].bnd_up()-DCS_new[0].DCS[index].bnd_low())*deg_to_rad;
				if ( count == lower)
				{
					if (lower==upper) {
						dchi_step=dchi;
						//method = 1;
					}
					else {
						dchi_step = (lower + 0.5) * dchi_DCS * deg_to_rad - chi_l;
						//method = 2;
					}
				}
				else if (count == upper)
				{
					dchi_step = chi_u - (upper - 0.5) * dchi_DCS * deg_to_rad;
					//method = 3;
				}
				else
				{
					//for example: from 179.5 to 180.0 and back from 180.0 to 179.5
					if (index==0 || index==(int)DCS_new[0].size_DCS()-1)	{
						dchi_step=2.0*local_dchi;
						//method=4;
					}
					else	{
						dchi_step=local_dchi;
						//method =5;
					}
				}

				DCS=constant*dchi_step/local_dchi;
				DCS_odd=constant*odd_factor*dchi_step/local_dchi;
				DCS_quantum = constant * quantum_factor * dchi_step / local_dchi;

				for (unsigned k=0;k<DCS_new.size();++k)
				{
					if ((k+1) % 2 == 1)	{
						//*
						//odd, starts from 0
						for (unsigned jj=0;jj<DCS_new[k].coef.size();++jj)
						{
							//eq(7) Viehland 1986, gerade terms
							DCS_new[k].DCS[index].add(DCS_new[k].coef[jj]*(DCS_odd*(1.0 - cos( (2.0*jj+1.0)*chi ))/2.0 +
							DCS_quantum * sin((2.0*jj+1.0)*chi) ) );
						}
						//add resonant charge contribution
						DCS_new[k].DCS[index].add(DCS * RCE / 2.0);

						// */
						//classical expression
						//DCS_new[k].DCS[index].add( (1.0 - (1.0 - 2.0 * RCE) *(pow(cos(chi_u),k+1)+pow(cos(chi_l),k+1)/2.0 ) )* DCS / 2.0 );
						//DCS_new[k].DCS[index].add( (1.0 - (1.0 - RCE) * pow(cos(chi),k+1) ) * DCS / 2.0 );
					}
					else	{
						//*
						//gerade, starts from 1
						for (unsigned jj=1;jj<DCS_new[k].coef.size();++jj)
						{
							//eq(8) Viehland 1986, ungerade terms
							DCS_new[k].DCS[index].add(DCS*DCS_new[k].coef[jj]/2.0*pow(sin(jj*chi),2) );
						}
						// */
						//classical expression
						//DCS_new[k].DCS[index].add( DCS * ( 1.0 -( pow(cos(chi_u),k+1) + pow(cos(chi_l),k+1) )/2.0) / 2.0 );
						//DCS_new[k].DCS[index].add( DCS * ( 1.0 -pow(cos(chi),k+1) ) / 2.0 );
					}
				}

				//check integration step
				test += dchi_step;
				++ count;
			}
			//*
			if (dchi >0.0 && std::abs(test / dchi- 1.0) > tol_dchi)	{
				std:: cout << "Error in integration of dchi. Dchi is not conserved." << std::endl;
				std::cout << " Ratio: " << test / dchi << std::endl;
				std::cout << "Binned dchi " << test << std::endl;
				std::cout << "dchi " << dchi << std::endl;
				std::cout << "Ratio -1 " << test/dchi -1.0 << std::endl;
				throw std::runtime_error("Error in integration of dchi");
			}
			// */
			/*
			for (unsigned k=0;k<test_cs.size();++k)
			{
				if ( (k+1) % 2 == 1)
					test_cs[k] += (1.0 - (1.0 - 2.0 * RCE) * pow(cos(chi),k+1)) * constant * dchi / 2.0;
				else
					test_cs[k] += (1.0 - pow(cos(chi),k+1)) * constant * dchi / 2.0;
			}
			// */

			//ungerade contribution
			DCS=0.0;
			DCS_odd=0.0;
			if ( dbdchiu[j*last+i]!=0)
			{
				//determine dchi
				if (i==0)
				{
					chi_u= (chiu[j*last+i+step]+ chiu[j*last+i] )/2.0;
					chi_l= chiu[j*last+i];
					//dchi = std::abs( chiu_integral[j*last+i+step] - chiu_integral[j*last+i]) /2.0;
				}
				else if (i==last-1)
				{
					chi_l= (chiu[j*last+i]+ chiu[j*last+i-step] )/2.0;
					chi_u= chiu[j*last+i];
					//dchi = std::abs( chiu_integral[j*last+i] - chiu_integral[j*last+i-step]) / 2.0;
				}
				else
				{
					chi_u= (chiu[j*last+i+step]+ chiu[j*last+i] )/2.0;
					chi_l= (chiu[j*last+i] + chiu[j*last+i-step] )/2.0;
					//dchi = std::abs( chiu_integral[j*last+i+step] - chiu_integral[j*last+i-step]) / 2.0;
				}
				chi = chiu[j*last+i];
			}
			else	{
				chi_l = 0.0;
				chi_u = 0.0;
				chi = 0.0;
				dchi = 0.0;
			}

			if (chi_l > chi_u)
				std::swap(chi_u,chi_l);

			dchi=chi_u-chi_l;
			lower = std::round(chi_l * rad_to_deg / dchi_DCS);
			upper = std::round(chi_u * rad_to_deg / dchi_DCS);

			if (dchi>0.0 && chiu[j*last+i]!=0.0 && std::isinf(bu[j*last+i])==false )
			{
				//2* pi * sin(chi) *b *db/(dchi * sin(chi)) = 2 * pi b *db/dchi
				/*
				if (i==0)
					dbdchi_temp = (bu[j*last+i+step] - bu[j*last+i]) / dchi / 2.0;
				else if (i==last-step)
					dbdchi_temp = (bu[j*last+i] - bu[j*last+i-step]) / dchi / 2.0;
				else
					dbdchi_temp = (bu[j*last+i+step] - bu[j*last+i-step]) / dchi / 2.0;

				if (std::isnan(dbdchi_temp)==true)
					dbdchi_temp = 0.0;

				constant=2.0 * Constant::pi * std::abs(bu[j*last+i] * dbdchi_temp);
				*/
				constant=2.0 * Constant::pi * std::abs(bu[j*last+i] * dbdchiu[j*last+i]);
			}
			else	{
				constant = 0.0;
				//dbdchi_temp = 0.0;
			}
			//*
			if (std::isnan(delta_eta[j*last+i])==0)	{
				odd_factor= (1.0 - 2.0*pow(sin(delta_eta[j*last+i]),2 ) )/2.0 ;
				quantum_factor = sin(2.0*delta_eta[j*last+i])/4.0;
			}
			else	{
				odd_factor=0.0;
				quantum_factor = 0.0;
			}
			// */

			//calculate resonant charge exchange cross section
			//DCS_RCE_term = constant * std::pow(sin(delta_eta[j*last+i]),2.0);

			//determine first index
			test=0.0;
			count =lower;

			while (count <= upper && dchi>0.0)
			{
				//get correct index
				index = std::abs(count) % int_360;
				if (index> (int) DCS_new[0].DCS.size()-1 )
					index = std::abs(index-int_360);
				local_dchi = (DCS_new[0].DCS[index].bnd_up()-DCS_new[0].DCS[index].bnd_low() ) * deg_to_rad;
				if ( count == lower)
				{
					if (lower==upper) {
						dchi_step=dchi;
						//method = 1;
					}
					else {
						dchi_step = (lower + 0.5) * dchi_DCS * deg_to_rad - chi_l;
						//method = 2;
					}
				}
				else if (count == upper)
				{
					dchi_step = chi_u - (upper - 0.5) * dchi_DCS * deg_to_rad;
					//method = 3;
				}
				else
				{
					//for example: from 179.5 to 180.0 and back from 180.0 to 179.5
					if (index==0 || index==(int)DCS_new[0].size_DCS()-1)	{
						dchi_step=2.0*local_dchi;
						//method=4;
					}
					else	{
						dchi_step=local_dchi;
						//method =5;
					}
				}

				DCS=constant*dchi_step/local_dchi;
				DCS_odd=constant*odd_factor*dchi_step/local_dchi;
				DCS_quantum = constant * quantum_factor * dchi_step / local_dchi;

				for (unsigned k=0;k<DCS_new.size();++k)
				{
					if ((k+1) % 2 ==1)	{
						//*
						//odd, starts from 0
						for (unsigned jj=0;jj<DCS_new[k].coef.size();++jj)
						{
							//eq (7) Viehland 1986, ungerade terms (plus resonant charge exchange)
							DCS_new[k].DCS[index].add(DCS_new[k].coef[jj]*(DCS_odd*(1.0-cos( (2.0*jj+1.0)*chi ))/2.0 -
							DCS_quantum * sin((2.0*jj+1.0)*chi) ) );
						}
						//add resonant charge exchange
						DCS_new[k].DCS[index].add(DCS * RCE / 2.0);

						// */
						//classical expression
						//DCS_new[k].DCS[index].add( (1.0 - (1.0 - 2.0 * RCE) * (pow(cos(chi_u),k+1)+pow(cos(chi_l),k+1) )/2.0) * DCS /sin(chi) / 2.0);
						//DCS_new[k].DCS[index].add( (1.0 - (1.0 - RCE) * pow(cos(chi),k+1)) * DCS / 2.0);
					}
					else	{
						//*
						//gerade, starts from 1
						for (unsigned jj=1;jj<DCS_new[k].coef.size();++jj)
						{
							//eq(8) Viehland 1986, ungerade terms
							DCS_new[k].DCS[index].add(DCS*DCS_new[k].coef[jj]/2.0*pow(sin(jj*chi),2) );
						}
						// */
						//classical expression
						//DCS_new[k].DCS[index].add( DCS * ( 1.0 - (pow(cos(chi_u),k+1)+ pow(cos(chi_l),k+1)) /2.0 ) / sin(chi) /2.0 );
						//DCS_new[k].DCS[index].add( DCS * ( 1.0 - pow(cos(chi),k+1) ) / 2.0 );
					}
				}

				//check integration step
				test += dchi_step;
				++ count;
			}
			/*
			if (dchi >0.0 && std::abs(test / dchi- 1.0) > tol_dchi)	{
				std:: cout << "Error in integration of dchi. Dchi is not conserved." << std::endl;
				std::cout << " Ratio: " << test / dchi << std::endl;
				std::cout << "Binned dchi " << test << std::endl;
				std::cout << "dchi " << dchi << std::endl;
				std::cout << "Ratio -1 " << test/dchi -1.0 << std::endl;
				throw std::runtime_error("Error in integration of dchi");
			}
			// */
			/*
			for (unsigned k=0;k<test_cs.size();++k)
			{
				if ( (k+1) % 2 == 1)
					test_cs[k] += (1.0 - (1.0 - 2.0 * RCE) * pow(cos(chi),k+1)) * constant * dchi / 2.0;
				else
					test_cs[k] += (1.0 - pow(cos(chi),k+1)) * constant * dchi / 2.0;
			}
			// */
		/*	if (dchi>100 && index_cc==16)
				std:: cout << chi_l << "\t" << chi_u << std::endl;
		*/
		}
	}

	/*
	for (unsigned k=0;k<DCS_new.size();++k)
	{
		std::cout << std::endl << test_cs[k];
	}
	// */

	//calculate relative change
	double residue=0.0;
	/*
	//method 1:
	double old_g,old_u;
	//compare individual elements of the DCS with the previous DCS
	for (unsigned i=0;i<DCS_new.size();++i)
	{
		old_g=std::abs(DCS_new[i].DCS-DCS_old[i].DCS)/DCS_new[i].DCS;
		old_u=std::abs(DCS_new[i].DCS_odd-DCS_old[i].DCS_odd)/DCS_new[i].DCS_odd;
		if (DCS_new[i].DCS==0 || DCS_new[i].DCS_odd==0)
			residue=1.0;
		else
		{
			residue=std::max(residue,std::max(old_g,old_u));
		}

		//check intermediate results
	//	std::cout << DCS_new[i].chi() << "\t" << DCS_new[i].DCS << "\t" << DCS_old[i].DCS << "\t" << DCS_new[i].DCS_odd << "\t" << DCS_old[i].DCS_odd << "\t" << residue << std::endl;
	}
	// */

	//method 2:
	//compare calculated transport cross sections with the previous ones
	residue = RCEDCS_residue(DCS_new, DCS_old);
	return residue;
}

double calc_dcs_rce::RCEDCS_residue(const std::vector<QlRCE>& DCS_new, const std::vector<QlRCE>& DCS_old) const
{
	//write DCS to file
	std::vector<double> cs_new(m_l_max,0.0);
	std::vector<double> cs_old(m_l_max,0.0);

	const double deg_to_rad = Constant::pi / 180.0;
	for (unsigned j=0;j<DCS_new.size();++j)
	{
		for (unsigned i=0;i<DCS_new[j].size_DCS();++i)
		{
			const double dchi = (DCS_new[j].DCS[i].bnd_up() - DCS_new[j].DCS[i].bnd_low()) * deg_to_rad;

			cs_new[j] += DCS_new[j].DCS[i].dcs() * dchi;
			cs_old[j] += DCS_old[j].DCS[i].dcs() * dchi;

			//write output
//			std::cout << DCS_new[j].DCS[i].chi() << "\t" << DCS_new[j].DCS[i].DCS << std::endl;
		}
	}
	//calculate residue
	double residue=0.0;
	for (unsigned i=0; i<cs_new.size(); ++i)
	{
		residue = std::max(residue, std::abs( (cs_new[i] - cs_old[i])/cs_new[i]) );
	}
	std::cout << "\tresidue\t" << residue;
	for (unsigned i=0; i<DCS_new.size();++i)
	{
		std::cout << "\t" << cs_new[i];
	}
	std::cout << std::endl;
	return residue;
}

double calc_dcs_rce::verify_Ql(const std::vector<double>& bg, const std::vector<double>& chig, const std::vector<double>& bu, const std::vector<double>&chiu,
	const std::vector<double>& delta_eta, int l,int index_cc) const
{
	double Ql=0.0;
	int step=std::pow(2,m_integrator.upper()-index_cc);
	unsigned n=std::pow(2,m_integrator.upper())+1;
	unsigned sg=bg.size()/n;
	unsigned su=bu.size()/n;
	int ind;
	double db;
	if (l % 2 == 0)
	{
		//gerade
		for (unsigned i=0;i<n;i += step)
		{
			for (unsigned j=0; j<sg; ++j)
			{
				ind = i + j*n;
				if (chig[ind]!= 0.0)
				{
					if (i==0)
						db = (bg[ind+step]-bg[ind]) / 2.0;
					else if (i==n-1)
						db = (bg[ind] - bg[ind-step]) / 2.0;
					else
						db = (bg[ind+step]-bg[ind-step])/ 2.0;

					Ql += std::abs(2.0 * Constant::pi * bg[ind] * db * (1.0 - pow(cos(chig[ind]),l) ))/2.0;
				}
			}
		}
		//ungerade
		for (unsigned i=0;i<n;i += step)
		{
			for (unsigned j=0; j<su; ++j)
			{
				ind = i + j*n;
				if (chiu[ind]!= 0.0)
				{
					if (i==0)
						db = (bu[ind+step]-bu[ind]) / 2.0;
					else if (i==n-1)
						db = (bu[ind] - bu[ind-step]) / 2.0;
					else
						db = (bu[ind+step]-bu[ind-step])/ 2.0;

					Ql += std::abs(2.0 * Constant::pi * bu[ind] * db * (1.0 - pow(cos(chiu[ind]),l) ))/2.0;
				}
			}
		}

	}
	else
	{
		//interpolate ungerade phaseshift, scattering angle to gerade grid (bg)
		std::vector<double> chi_g(bu.size(),0.0);
		vector_linear_interp1(bg, chig, bu, chi_g, step, n);

		//gerade+ungerade using ungerade grid
		for (unsigned j=0; j<su; ++j)
		{
			for (unsigned i=0;i<n;i += step)
			{
				ind = i + j*n;

				if (i==0)
					db = (bu[ind+step]-bu[ind]) / 2.0;
				else if (i==n-1)
					db = (bu[ind] - bu[ind-step]) / 2.0;
				else
					db = (bu[ind+step]-bu[ind-step])/ 2.0;

				const double RCE = pow(sin(delta_eta[ind]),2);

				if (chiu[ind] != 0.0)	{
					//ungerade
					Ql += std::abs(2.0 * Constant::pi * bu[ind] * db * (1.0 - (1.0 - 2.0 * RCE) * pow(cos(chiu[ind]),l) ))/2.0;
				}
				if (chi_g[ind] != 0.0)	{
					//gerade
					Ql += std::abs(2.0 * Constant::pi * bu[ind] * db * (1.0 - (1.0 - 2.0 * RCE) * pow(cos(chi_g[ind]),l) ))/2.0;
				}
			}
		}
	}
	return Ql;
}

double calc_dcs_rce::verify_Ql_DCS(const std::vector<double>& b, const std::vector<double>& chi, const std::vector<double>& dbdchi, int l,int index_cc) const
{
	double Ql=0.0;
	int step=std::pow(2,m_integrator.upper()-index_cc);
	unsigned n=std::pow(2,m_integrator.upper())+1;
	unsigned s=b.size()/n;
	int ind;
	double dchi;
	for (unsigned i=0;i<n;i += step)
	{
		for (unsigned j=0; j<s; ++j)
		{
			ind = i + j*n;
			if (chi[ind]!= 0.0)
			{
				if (i==0)
					dchi = (chi[ind+step]-chi[ind]);
				else /* if (i==n-1) // */
					dchi = (chi[ind] - chi[ind-step]);
				/* else
					dchi = ( chi[ind-step] + chi[ind+step] ) / 2.0; // */
				Ql += std::abs(2.0 * Constant::pi * b[ind] * dbdchi[ind] * dchi * (1.0 - pow(cos(chi[ind]),l) ) );
			}
		}
	}
	return Ql;
}

} // namespace magnumpi
