#include <string>
#include <iostream>
#include <stdexcept>
#include <iomanip>
#include <chrono>
#include "magnumpi/json.h"
#include "magnumpi/consts.h"
#include "magnumpi/potential.h"
#include "magnumpi/utility.h"
#include "ideas/dcs/dcs.h"

int main(int argc, char *argv[])
{
	try {
		using namespace magnumpi;
		//set output to scientific
		std::cout << std::scientific;
		//std::setprecision(16);
		const std::chrono::time_point<std::chrono::system_clock> start(std::chrono::system_clock::now());

		if (argc!=2)
		{
			throw std::runtime_error("Usage: calc_dcs_rce <inputfile>");
		}
		const json_type cnf(magnumpi::read_json_from_file(argv[1]));

		const std::unique_ptr<const potential> pot_g(potential::create(cnf.at("interaction").at("potential_g")));
		const std::unique_ptr<const potential> pot_u(potential::create(cnf.at("interaction").at("potential_u")));

		//consider these clenshaw curtis quadratures
		const int lower_cc=cnf.at("cc").at("lower");
		const int upper_cc=cnf.at("cc").at("upper");

		const double reduced_mass = in_si(cnf.at("interaction").at("reduced_mass"),units::mass);

		//set range for calculating cross section as a function of energy [log]
		/// \todo pass around the energy itself, in SI units, around internally
		const double logE_low=std::log10(in_si(cnf.at("energy_range").at("min"),units::energy));
		const double logE_high=std::log10(in_si(cnf.at("energy_range").at("max"),units::energy));
		const double E_points_decade=cnf.at("energy_range").at("points_per_decade");

		// max l for which the cross section is calculated
		const int l_max = cnf.at("l_max");

		double rmin=in_si(cnf.at("r").at("min"),units::length);
		double rmax=in_si(cnf.at("r").at("max"),units::length);
		const double dr=in_si(cnf.at("r").at("del"),units::length);

		// adjust rmin,rmax if necessary
		const double Vmin=in_si(cnf.at("r").at("Vmin"),units::energy);
		const double Vmax=in_si(cnf.at("r").at("Vmax"),units::energy);
		rmin_input_evaluation(rmin,pot_g.get(),Vmin);
		rmin_input_evaluation(rmin,pot_u.get(),Vmin);
		rmax_input_evaluation(rmax,pot_g.get(),Vmax);
		rmax_input_evaluation(rmax,pot_u.get(),Vmax);

		//determine whether scat_Colonna or scat_Viehland is used for estimating scattering angles
		const int use_scat_Colonna=1;
		//set tolerance and interpolation order
		const int interp=3;
		const double tolerance = cnf.at("tolerance");

		//make DCS object
		calc_dcs_rce DCS_RCE( lower_cc, upper_cc, std::pow(10,logE_low), l_max, rmin, rmax, dr, use_scat_Colonna, interp, tolerance, reduced_mass, pot_g.get(),pot_u.get());
		//set the number of angular points.
		double num_chi = 181;
		//calculate DCS
		const std::vector<double> energy_vals = make_samples_log(logE_low,logE_high,E_points_decade);
		std::vector<data_set> dcs_data = DCS_RCE.DCS_RCE(energy_vals,num_chi);
		dcs_data[0].write("cs_rce_from_dcs.dat");
		for (unsigned l=1; l!=dcs_data.size(); ++l)
		{
			std::stringstream ss;
			ss << l;
			dcs_data[l].write("dcs_l_" + ss.str() + ".dat");
		}

		//print the elapsed time
		const std::chrono::time_point<std::chrono::system_clock> end(std::chrono::system_clock::now());
		const std::chrono::duration<double> dt(end - start);
		std::cout << "Run time\t" << dt.count() << "s" << std::endl;
	}
	catch (const std::exception& exc)
	{
		std::cerr << "Error: " << exc.what() << std::endl;
		return 1;
	}
	return 0;
}
