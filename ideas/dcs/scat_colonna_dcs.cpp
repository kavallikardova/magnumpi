#include "ideas/dcs/scat_colonna_dcs.h"
#include "magnumpi/potential.h"
#include "magnumpi/consts.h"

#include <cmath>
#include <algorithm>
#include <iostream>

namespace magnumpi {

//DCS version that also calculates phase shift and dchi/db
void scat_Colonna_DCS(double& scat,
		double& dchi_db,
		double& eta,
		const potential* V,
		double b,
		double eps,
		double tolerance,
		int n,
		double reduced_mass)
{
	const double r_max = 25e-9;
	double r_high = (b > r_max) ? b + 1e-9 : r_max;
	// These will be properly initialized in the for loop below.
	double pot_high = std::numeric_limits<double>::quiet_NaN();
	double integral = std::numeric_limits<double>::quiet_NaN();
	double int_dchidb = std::numeric_limits<double>::quiet_NaN();

	/// \todo Find out what this local_tolerance is about: it does not appear in scat_colonna.cpp
	double local_tolerance = Constant::pi / 180.0	* tolerance;
	for (;;)
	{
		pot_high = V->value(r_high);
		// eq 15, multiplied with b
		const double bI1 = std::asin(b/r_high/std::sqrt(1.0-pot_high/eps));
		const double bI2 = std::asin(b/r_high);
		// eq 13
		const double I3 = 1.0 / std::sqrt(1.0 - std::pow(b/r_high, 2) - pot_high / eps) / (r_high * r_high);
		const double I4 = 1.0 / std::sqrt(1.0 - std::pow(b/r_high, 2)) / (r_high * r_high);
		const double error = std::max( (b==0 ? 0 : std::abs(bI1 - bI2) / bI1), std::abs(I3 - I4) / I3);
		if (error <= local_tolerance && 1.0 - std::pow(b/r_high, 2) - pot_high / eps > 0)
		{
			//choose an underestimate of the long range scattering angle
			integral = pot_high<0 ? bI1 : bI2;
			int_dchidb = pot_high < 0 ? I3 : I4;
			break;
		}
		r_high *= 2;
	}

	//store r_high for the calculation of the phaseshift
	double r_high_first = r_high;
	double r_high_previous = r_high;

	/// \todo Explain --- why is there no long-range contribution to int_I6?
	double int_I6 = 0.0;

	double delta_r = 1e-12;
	double sqrt_high = std::sqrt(1.0 - std::pow(b/r_high, 2) - pot_high / eps);
	double F_high = std::pow(r_high, -2) / sqrt_high;
	double F_high_2 = std::pow(r_high, -2) * std::pow(sqrt_high, -3);

	double r_low = std::numeric_limits<double>::quiet_NaN();
	double pot_low = std::numeric_limits<double>::quiet_NaN();
	//loop for the integration from r_high to a value close to r0 (eq 1)
	for (;;)
	{
		//obtain integral contribution of one segment
		// These will be properly initialized in the for loop below.
		double I2 = std::numeric_limits<double>::quiet_NaN();
		double I4 = std::numeric_limits<double>::quiet_NaN();
		double I6 = std::numeric_limits<double>::quiet_NaN();
		for (;;)
		{
			//calculate low points
			r_low = r_high - delta_r;
			pot_low = V->value(r_low);

			//make sure the denominator is still positive, otherwise reduce step size
			while (1.0 - std::pow(b/r_low, 2) - pot_low / eps < 0 || r_low < 0.0)
			{
				delta_r = delta_r / 2.0;
				r_low = r_high - delta_r;
				pot_low = V->value(r_low);
			}

			//calculate mid points
			const double r_mid = (r_high + r_low) / 2.0;
			const double pot_mid = V->value(r_mid);

			//calculate integrand function
			const double sqrt_low = std::sqrt(1.0 - std::pow(b/r_low, 2) - pot_low / eps);
			const double F_low = std::pow(r_low, -2) / sqrt_low;
			const double F_low_2 = std::pow(r_low, -2) * std::pow(sqrt_low, -3);
			const double sqrt_mid = std::sqrt(1.0 - std::pow(b / r_mid, 2) - pot_mid / eps);
			const double F_mid = std::pow(r_mid, -2) / sqrt_mid;
			const double F_mid_2 = std::pow(r_mid, -2) * std::pow(sqrt_mid, -3);

			//calculate integral contributions (eq 7,8,9)
			// multiplied with b
			//const double I_trapz = delta_r * (F_high + F_low) / 2.0;
			//const double I_midp  = delta_r * F_mid;
			const double I1 = delta_r * (F_high + F_low) / 2.0;
			I2 = delta_r / 2.0 * (F_low / 2.0 + F_high / 2.0 + F_mid);
			const double I3 = delta_r / 2.0 * (F_high_2 + F_low_2);
			I4 = delta_r / 2.0 * (F_low_2 / 2.0 + F_high_2 / 2.0 + F_mid_2);

			//error scattering angle
			double error = std::max(std::abs(I2 - I1) / I2, std::abs(I4 - I3) / I4);

			//calculate contributions phase shift
			const double I5 = delta_r / 2.0 * ( sqrt_low + sqrt_high);
			I6 = delta_r / 2.0 * ( sqrt_low / 2.0 + sqrt_high / 2.0 + sqrt_mid );

			//error phase shift
			error = std::max(error, std::abs(I6 - I5) / I6);

			//evaluate error
			if (error < local_tolerance)
			{
				r_high = r_low;
				pot_high = pot_low;
				F_high = F_low;
				F_high_2 = F_low_2;
				sqrt_high = sqrt_low;
	//			std::cout << "r0: " << r_high << ", delta_r: " << delta_r << std::endl;
				// we found and handled a suitable dr. (Maybe) adjust
				// delta_r, then leave the inner loop
				if (error < local_tolerance / 4.0)
				{
					//1: accept result increase delta_r for next step
					//eq 4, but 2 is used instead of 1.5 to clip q
					const double q = std::min(2.0, 0.9 * std::pow(local_tolerance / error, n + 1));
					delta_r = delta_r * q;
				}
				break;
			}
			else
			{
				//2: reject result decrease delta_r for next step
				const double q = std::max(0.5, 0.9 * std::pow(local_tolerance / error, n + 1));
				delta_r = delta_r * q;
	//			std::cout << "BAD: delta_r/r0/eps: " << delta_r/r_high/std::numeric_limits<double>::epsilon() << std::endl;
			}
		}
		//perform integration of scattering angle
		integral += b*I2;

		//perform integration of dchidb (second term)
		int_dchidb += I4;

		//perform integration of phase shift
		int_I6 += I6;

		/** \todo Check why the stop criterium here is different
		 *        from in scat_colonna.cpp.
		 */
		//check if r0 is reached
		if (r_high - r_high_previous == 0.0)
		{
	//		std::cout << "FINISHED" << std::endl;
			//end outer while loop
			break;
		}
		else
		{
	//		std::cout << "drH: " << (r_high - r_high_previous) << std::endl;
			r_high_previous = r_high;
		}
	}
	// now r_low is r0

	//eq 1: return scattering angle
	scat = Constant::pi - 2*integral;
	if (std::abs(scat/Constant::pi)<tolerance)
	{
		//results can oscillate for various b's at a constant E,
		//because a better accuracy than tolerance can not be expected
		scat = 0.0;
		//also allow no phase shift
		eta = 0.0;
		//also set derivative to zero
		dchi_db = 0.0;
	}
	else
	{
		scat = Constant::pi - 2.0 * integral;
		double dr0db = 2.0 * b / ( 2.0 * r_low * (1.0 - pot_low / eps) - std::pow(r_low, 2) / eps * V->dVdr(r_low) );
		dchi_db = - 2.0 * int_dchidb + 2.0 * b / std::pow(r_low, 2) / std::sqrt(1.0 - std::pow(b / r_low, 2) - pot_low / eps) * dr0db;
		double int_no_disturb = r_high_first * std::sqrt(1.0 - std::pow(b / r_high_first, 2)) + b * (std::asin(b / r_high_first) - Constant::pi / 2.0 );
		eta = std::sqrt(2.0 * reduced_mass * eps) / Constant::hbar * (int_I6 - int_no_disturb );
	}

	if ( std::isnan(scat) == 1 )
	{
		std::cout << eps << "\t" << b << "\t" << integral << "\t" << int_I6 << std::endl;
	}
	if (std::isnan(eta) == 1)
	{
		std::cout << r_low << "\t" << b << "\t" << eps << "\t" << int_I6 << "\t" << eta << std::endl;
	}

}

//calculate phase shift
double calc_eta(const potential* Vu, const potential* Vg, double reduced_mass, double b, double eps, double tolerance, int n)
{
	double int_g = 0.0;
	double int_u = 0.0;
	double r_high, r_max = 25e-9;
	double r_low, r_mid, r0 = 0.0;
	double pot_low_u, pot_mid_u, pot_high_u = 0;
	double pot_low_g, pot_mid_g, pot_high_g = 0;
	double F_low_u, F_mid_u, F_high_u;
	double F_low_g, F_mid_g, F_high_g;
	double delta_r = 1e-12;
	double I1 = 0.0, I2 = 0.0, I3, I4, error;
	double rel_test = 1e-5;
	double rel_stop = 1e-12;
	double local_tolerance = tolerance / 10.0;
	double temp;

	r_high = (b > r_max) ? b + 1e-9 : r_max;

	//estimate error of setting integration limit to r_high instead of Infinite
	//adjust r_high if necessary, (eq 15-17)
	error = 1.0;
	double mult = 2.0;
	r_high = r_high / mult;
	while (	error > local_tolerance || pot_high_u >= eps || pot_high_g >= eps ||
		1.0 - std::pow(b / r_high, 2) - pot_high_u / eps <= 0.0 || 1.0 - std::pow(b / r_high, 2) - pot_high_g / eps <= 0.0)
	{
		r_high *= mult;
		pot_high_u = Vu->value(r_high);
		pot_high_g = Vg->value(r_high);
		//eq 15
		if (b > 1e-12)
		{
			I1 = 1.0 / b * std::asin(b / r_high / std::sqrt(1.0 - pot_high_u / eps));
			I2 = 1.0 / b * std::asin(b / r_high);
			I3 = 1.0 / b * std::asin(b / r_high / std::sqrt(1.0 - pot_high_g / eps));
			I4 = 1.0 / b * std::asin(b / r_high);
		}
		else
		{
			//b=0, use taylor series 1/b*std::asin(b*c)=1/b*b*c=c
			I1 = 1.0 / r_high / std::sqrt(1.0 - pot_high_u / eps);
			I2 = 1.0 / r_high;
			I3 = 1.0 / r_high / std::sqrt(1.0 - pot_high_g / eps);
			I4 = 1.0 / r_high;
		}
		error = std::max(std::abs(I1 - I2) / I1, std::abs(I3 - I4) / I3);
	}
	double r_high_first = r_high;

	temp = 1.0 - std::pow(b / r_high, 2) - pot_high_u / eps;
	F_high_u = (temp >= 0) ? std::sqrt(temp) : 0.0;

	temp = 1.0 - std::pow(b / r_high, 2) - pot_high_g / eps;
	F_high_g = (temp >= 0) ? std::sqrt(temp) : 0.0;


	//gerade
	//////////////////////////////
	//loop for the integration from r_high to a value close to r0 (eq 1)
	for(;;)
	{
		//obtain integral contribution of one segment
		for(;;)
		{
			//calculate low points
			r_low = r_high - delta_r;
			pot_low_g = Vg->value(r_low);

			//make sure the denominator is still positive, otherwise reduce step size
			while (1.0 - std::pow(b / r_low, 2) - pot_low_g / eps < 0 || r_low < 0.0)
			{
				delta_r = delta_r / 2.0;
				r_low = r_high - delta_r;
				pot_low_g = Vg->value(r_low);
			}

			//calculate mid points
			r_mid = (r_high + r_low) / 2.0;
			pot_mid_g = Vg->value(r_mid);

			//calculate integrand function
			temp = 1.0 - std::pow(b / r_low, 2) - pot_low_g / eps;
			F_low_g = (temp < 0.0) ? 0.0 : std::sqrt(temp);

			temp = 1.0 - std::pow(b / r_mid, 2) - pot_mid_g / eps;
			F_mid_g = (temp < 0.0) ? 0.0 : std::sqrt(temp);

			//calculate integral contributions (eq 7,8,9)
			I3 = delta_r / 2.0 * (F_high_g + F_low_g);
			I4 = delta_r / 2.0 * (F_low_g / 2.0 + F_high_g / 2.0 + F_mid_g);

			//error scattering angle
			error = std::abs(I4 - I3) / I4;

			//evaluate error
			if (error < local_tolerance / 4.0)
			{
				//1: accept result increase delta_r for next step
				//eq 4
				double q = std::min(2.0, 0.9 * std::pow(local_tolerance / error, n + 1));
				delta_r = delta_r * q;
				//pass on data from r_high to r_low
				r_high = r_low;
				pot_high_g = pot_low_g;
				F_high_g = F_low_g;
				break;
			}
			else if (error > local_tolerance)
			{
				//2: reject result decrease delta_r for next step
				double q = std::max(0.5, 0.9 * std::pow(local_tolerance / error, n + 1));
				delta_r = delta_r * q;
			}
			else
			{
				//3: error in correct order of magnitude, use same delta_r for next step
				r_high = r_low;
				pot_high_g = pot_low_g;
				F_high_g = F_low_g;
				break;
			}
		}
		//perform integration of phase shift
		int_g += I4;

		//check size of r_step
		//in the paper the contribution of the last section (closest to r0) is extrapolated with an analytical function
		//is that a better way to cutoff the integration?
		if (r0 > 0.0 && delta_r / r_high <= rel_stop)
		{
			//end outer while loop
			break;
		}
		else if (delta_r / r_high <= rel_test) // && (delta_r/r_high)<dr_rel)
		{
			r0 = r_high;
		}
	}

	//ungerade
	//loop for the integration from r_high to a value close to r0 (eq 1)
	delta_r = 1e-12;
	r_high = r_high_first;
	while (true)
	{
		//obtain integral contribution of one segment
		while (true)
		{
			//calculate low points
			r_low = r_high - delta_r;
			pot_low_u = Vu->value(r_low);

			//make sure the denominator is still positive, otherwise reduce step size
			while (1.0 - std::pow(b / r_low, 2) - pot_low_u / eps < 0 || r_low < 0.0)
			{
				delta_r = delta_r / 2.0;
				r_low = r_high - delta_r;
				pot_low_u = Vu->value(r_low);
			}

			//calculate mid points
			r_mid = (r_high + r_low) / 2.0;
			pot_mid_u = Vu->value(r_mid);

			//calculate integrand function
			temp = 1.0 - std::pow(b / r_low, 2) - pot_low_u / eps;
			F_low_u = (temp < 0.0) ? 0.0 : std::sqrt(temp);

			temp = 1.0 - std::pow(b / r_mid, 2) - pot_mid_u / eps;
			F_mid_u = (temp < 0.0) ? 0.0 : std::sqrt(temp);

			//calculate integral contributions (eq 7,8,9)
			I1 = delta_r / 2.0 * (F_high_u + F_low_u);
			I2 = delta_r / 2.0 * (F_low_u / 2.0 + F_high_u / 2.0 + F_mid_u);

			//error scattering angle
			error = std::abs(I2 - I1) / I2;

			//evaluate error
			if (error < local_tolerance / 4.0)
			{
				//1: accept result increase delta_r for next step
				//eq 4
				double q = std::min(2.0, 0.9 * std::pow(local_tolerance / error, n + 1));
				delta_r = delta_r * q;
				//pass on data from r_high to r_low
				r_high = r_low;
				pot_high_u = pot_low_u;
				F_high_u = F_low_u;
				break;
			}
			else if (error > local_tolerance)
			{
				//2: reject result decrease delta_r for next step
				double q = std::max(0.5, 0.9 * std::pow(local_tolerance / error, n + 1));
				delta_r = delta_r * q;
			}
			else
			{
				//3: error in correct order of magnitude, use same delta_r for next step
				r_high = r_low;
				pot_high_u = pot_low_u;
				F_high_u = F_low_u;
				break;
			}
		}
		//perform integration of phase shift
		int_u += I2;

		//check size of r_step
		//in the paper the contribution of the last section (closest to r0) is extrapolated with an analytical function
		//is that a better way to cutoff the integration?
		if (r0 > 0.0 && delta_r / r_high <= rel_stop)
		{
			//end outer while loop
			break;
		}
		else if (delta_r / r_high <= rel_test) // && (delta_r/r_high)<dr_rel)
		{
			r0 = r_high;
		}
	}
	return	std::sqrt(2.0 * reduced_mass * eps) / Constant::hbar * (int_g - int_u );
}

} // namespace magnumpi
