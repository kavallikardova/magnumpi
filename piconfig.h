/** \file
 *  A wrapper file for the various compiler-environment header files.
 *  This files tries to determine your system type on the basis of 
 *  the symbols which are defined when this file is precompiled. If no
 *  appropriate configuration could be determined, you get an #error.
 *
 */

#ifndef H_PICONFIG_H
#define H_PICONFIG_H

// environment-specific configuration

#if defined HAVE_CONFIG_H	// we use the GNU build tools ...
# include "magnumpi-config.h"
#else
# error No appropriate configuration file found: see plconfig.h.
#endif

// set up the plEXP macro for exporting functions, classes and data
// (This stems from the time that PLASIMO was compiled with VisualStudio,
// in 2000 or so.)

// libplparser dll exports
#  if defined(LIBPLPARSER_EXPORTS)
#	 define plEXP __declspec( dllexport )
#  elif defined(PLDLL)
#	 define plEXP __declspec( dllimport )
#  else
#	 define plEXP
#  endif

// libplgeneric dll export
#  if defined(LIBPLGENERIC_EXPORTS)
#	 define genEXP __declspec( dllexport )
#  elif defined(PLDLL)
#	 define genEXP __declspec( dllimport )
#  else
#	 define genEXP
#  endif

// libplgrid dll export
#  if defined(LIBPLGRID_EXPORTS)
#	 define grdEXP __declspec( dllexport )
#  elif defined(PLDLL)
#	 define grdEXP __declspec( dllimport )
#  else
#	 define grdEXP
#  endif

// libplem dll export
#  if defined(LIBPLEM_EXPORTS)
#	 define emEXP __declspec( dllexport )
#  elif defined(PLDLL)
#	 define emEXP __declspec( dllimport )
#  else
#	 define emEXP
#  endif

// libplmath dll export
#  if defined(LIBPLMATH_EXPORTS)
#	 define mthEXP __declspec( dllexport )
#  elif defined(PLDLL)
#	 define mthEXP __declspec( dllimport )
#  else
#	 define mthEXP
#  endif

// libplspecies dll export
#  if defined(LIBPLSPECIES_EXPORTS)
#	 define spcEXP __declspec( dllexport )
#  elif defined(PLDLL)
#	 define spcEXP __declspec( dllimport )
#  else
#	 define spcEXP
#  endif

// libpltranseqns dll export
#  if defined(LIBPLTRANSEQNS_EXPORTS)
#	 define treEXP __declspec( dllexport )
#  elif defined(PLDLL)
#	 define treEXP __declspec( dllimport )
#  else
#	 define treEXP
#  endif

// libmd2d dll exports
#  if defined(LIBMD2D_EXPORTS)
#        define mdEXP __declspec( dllexport )
#  elif defined(PLDLL)
#        define mdEXP __declspec( dllimport )
#  else
#        define mdEXP
#  endif

// libmc dll exports
#  if defined(LIBMC_EXPORTS)
#        define mcEXP __declspec( dllexport )
#  elif defined(PLDLL)
#        define mcEXP __declspec( dllimport )
#  else
#        define mcEXP
#  endif

// some wx stuff

// libwxparser dll export 
#  if defined(LIBWXPARSER_EXPORTS)
#	 define plxEXP __declspec( dllexport )
#  elif defined(PLDLL)
#	 define plxEXP __declspec( dllimport )
#  else
#	 define plxEXP
#  endif

// libplwxgrid dll export
#  if defined(LIBWXGRID_EXPORTS)
#	 define grdxEXP __declspec( dllexport )
#  elif defined(PLDLL)
#	 define grdxEXP __declspec( dllimport )
#  else
#	 define grdxEXP
#  endif

// libplot dll export
#  if defined(LIBWXGENERIC_EXPORTS)
#	 define genxEXP __declspec( dllexport )
#  elif defined(PLDLL)
#	 define genxEXP __declspec( dllimport )
#  else
#	 define genxEXP
#  endif

/* Introduce same bad-ass optimization macros  -  BH */
#if defined(PL_HAVE_BUILTIN_EXPECT)
# define pl_likely(expr)   __builtin_expect(!!(expr),1)
# define pl_unlikely(expr) __builtin_expect(!!(expr),0)
#else
# define pl_likely(expr)   (expr)
# define pl_unlikely(expr) (expr)
#endif

#endif	// H_PICONFIG_H
