#include <string>
#include <iostream>
#include <stdexcept>
#include <chrono>
#include "../../../magnumpi/json.h"
#include "../../../magnumpi/consts.h"
#include "../../../magnumpi/potential.h"
#include "../../../magnumpi/scat_colonna.h"


int main(int argc, char *argv[])
{
	try {
        std::cout << "Content-type: application/json" << std::endl << std::endl;
		using namespace magnumpi;
		//set output to scientific
		std::cout << std::scientific;
		//std::setprecision(16);
		const std::chrono::time_point<std::chrono::system_clock> start(std::chrono::system_clock::now());

        const json_type cnf(magnumpi::read_json_from_stream(std::cin));

		const std::unique_ptr<const potential> pot(potential::create(cnf.at("interaction").at("potential")));

		//set tolerance and interpolation order
		const int interpol=3;
		const double tolerance = cnf.at("tolerance");

		const double b_low=in_si(cnf.at("b_range").at("min"),units::length);
		const double b_high=in_si(cnf.at("b_range").at("max"),units::length);
		const int num_b=cnf.at("b_range").at("points");

		const double logE_low=std::log10(in_si(cnf.at("energy_range").at("min"),units::energy));
		const double logE_high=std::log10(in_si(cnf.at("energy_range").at("max"),units::energy));
		const int E_pdecade=cnf.at("energy_range").at("points_per_decade");

		data_set chi_b_E_data = 
			get_scat_vs_b_vs_E(b_low, b_high, num_b, logE_low, logE_high, E_pdecade,interpol,tolerance,pot.get());

  		std::cout << chi_b_E_data.write_json() << std::endl;
	}
	catch (const std::exception& exc)
	{
		std::cerr << "Error: " << exc.what() << std::endl;
		return 1;
	}
	return 0;
}
