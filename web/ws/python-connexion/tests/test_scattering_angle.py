from magnumpi_webservice.api.scattering_angle import post


def test_post():
    request = {
        "interaction": {
            "potential": {
                "re": {
                    "value": 2e-10,
                    "unit": "m"
                },
                "type": "HardSphere"
            }
        },
        "b_range": {
            "min": {
                "value": 0,
                "unit": "m"
            },
            "max": {
                "value": 3e-10,
                "unit": "m"
            },
            "points": 31
        },
        "energy_range": {
            "max": {
                "value": 1e+5,
                "unit": "eV"
            },
            "min": {
                "value": 1e-4,
                "unit": "eV"
            },
            "points_per_decade": 5
        },
        "tolerance": 1e-3
    }

    response = post(request)

    assert response['title'] == 'chi(b,E)'
    assert len(response['columns']) == 31
    assert len(response['columns'][0]) == 46
