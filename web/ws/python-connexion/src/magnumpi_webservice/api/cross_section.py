from connexion import ProblemException
from flask import url_for

from ..tasks import calc_cross_section, get_result, delete_calculation


def post(body):
    job = calc_cross_section.delay(body)
    url = url_for('.magnumpi_webservice_api_cross_section_get', jobid=job.id)
    return {}, 201, {'Location': url}


def get(jobid):
    try:
        return get_result(jobid)
    except Exception as e:
        raise ProblemException(500, 'Error', str(e))


def delete(jobid):
    delete_calculation(jobid)
