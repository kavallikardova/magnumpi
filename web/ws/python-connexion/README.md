# MagnumPI web service

Web service which exposes endpoints to run MagnumPI calculations.

* OpenAPI specification at /openapi.json
* OpenAPI UI Console at /ui
* Quick calculations run directly.
* Slower calculations run using [celery job queue](https://docs.celeryproject.org/en/stable/).

## Install & run

```shell
# Install magnumpi Python package in current Python environment by following steps in ../../pymagnumpi/DEVELOP.md
# Install other deps
pip install .
# Star Task queue
docker run -d -p 6379:6379 redis
# Start task worker
export MAGNUMPI_INPUTDATA_DIR=$PWD/../../..
celery -A magnumpi_webservice.tasks worker
# Start webservice in another terminal
gunicorn -w 4 -b 0.0.0.0:8888 magnumpi_webservice.serve:app
```

Will run web service on http://localhost:8888, with the swagger ui on http://localhost:8888/ui

Run with docker-compose from root of repo with

```shell
docker-compose -f web/ws/python-connexion/docker-compose.yml up
```

Which will run web service on http://localhost:8888

## Development

### Install dependencies

```shell
pip install -e .[dev]
```

### Run webservice in debug mode

```shell
python -m magnumpi_webservice.serve
```

### Tests

Test can be run with

```shell
pytest
```

### Lint

Lint with pycodestyle

```shell
pycodestyle src/magnumpi_webservice
```
