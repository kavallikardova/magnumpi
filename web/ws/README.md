[OpenAPI](https://www.openapis.org/) specification of MagnumPI web service.

For each calc_* executable maps config file to a request and output files to a response.

# Generated server stubs

First install
```bash
npm install @openapitools/openapi-generator-cli 
```

## C++

```
npx openapi-generator generate -i openapi.yml -g cpp-pistache-server -o cpp-pistache-server \
--invoker-package magnumpi.ws --api-package magnumpi.ws.api \
--model-package magnumpi.ws.model -additional-properties=helpersPackage=magnumpi.ws.helpers
```

## Python

Instead of using generator, make use of https://connexion.readthedocs.io/ directly.
Also generator does not handle potential oneof

```
# npx openapi-generator generate -i openapi.yml -g python-flask -o python-flask
cookiecutter https://github.com/nlesc/python-template.git 
```

## PHP

```
npx openapi-generator generate -i openapi.yml -g php-lumen -o php-lumen
```

## NodeJS

Nodejs server generator is currently broken
```
npx openapi-generator generate -i openapi.yml -g nodejs-server -o nodejs-server
```
