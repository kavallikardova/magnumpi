# Web assembly of MagnumPI

The JavaScript npm package of the MagnumPI C++ library.
MagnumPI can be used to calculate scattering angles and several types of cross-sections between 2 particles.

This directory contains a [emscripten bindings](https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html) file (`src/wa.cpp`) to make C++ functions available to JS using a [WebAssembly](https://webassembly.org/) file.

## Installation

Install using

```shell
npm install @magnumpi/wasm
```

Or for [yarn](https://yarnpkg.com/) users

```shell
yarn add @magnumpi/wasm
```

## Run

The library can be run in [NodeJS](https://nodejs.org/) and [Web Worker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API) environments. See below for a number of examples.

### Using CommonJS require and promises

```javascript
const magnumpicalc = require('@magnumpi/wasm')();
// Wait for wasm to be loaded
magnumpicalc.then((magnumpi) => {
    console.log(magnumpi.eV);

    // Convert eV to Joule
    const joule = magnumpi.in_si('{"value": 1e+5, "unit": "eV"}', magnumpi.dimensions.energy);
    console.log(joule);
});
```

### Using dynamic import and await

```shell
node --experimental-repl-await
```

```js
// Using dynamic import and ES modules
const magnumpicalc = await import('@magnumpi/wasm');
const magnumpi = await magnumpicalc.default();
magnumpi.eV
```

### Using ES module file

Create test file called `ev.mjs` with

```js
import magnumpicalc from '@magnumpi/wasm';
const magnumpi = await magnumpicalc();
magnumpi.eV
```

Run with

```shell
node ev.mjs
```

### Use in web application web worker

See [../magnumpi-app/](https://gitlab.com/magnumpi/magnumpi/-/tree/master/web/magnumpi-app/README.md) how to use the library in a web worker of a web application.

### Example calculation

In the [src/magnumpi.test.js](https://gitlab.com/magnumpi/magnumpi/-/tree/master/web/assembly/src/magnumpi.test.js) file there is a test that calculates the cross section of a FiniteWell potential, the test shows how the available methods can be connected together.

## Development

### Requirements

Requires

* emscripten SDK see https://kripken.github.io/emscripten-site/docs/getting_started/downloads.html#sdk-download-and-install
* make

The `emcc` executable of the emscripten SDK should be in your PATH.

### Dependencies

Install development dependencies with

```bash
npm install
```

### Build

To build `dist/` folder with wasm file and js wrappers use

```bash
npm run build
```

To clean `dist/` folder use

```shell
npm run clean
```

## Tests

There are also unit tests written in [jest](https://jestjs.io/) test framework which can be run with

```shell
npm run test
```
