const magnumpicalc = require('../dist/magnumpicalc.cjs')

describe('magnumpi', () => {
    let magnumpi;
    beforeAll(async () => {
        magnumpi = await magnumpicalc();
    });

    it('eV', () => {
        const value = magnumpi.eV;

        const expected = 1.6021766208e-19;
        expect(value).toBeCloseTo(expected);
    });

    describe('in_si()', () => {
        it('eV -> joule', () => {
            const value = 1e20;
            const unit = 'eV';

            const result = magnumpi.in_si(
                JSON.stringify({value, unit}),
                magnumpi.dimensions.energy
            );

            const expected = 16.02177;
            expect(result).toBeCloseTo(expected);
        })
    })

    it('Calculate cross section of a FiniteWell potential()', () => {
        const pot = magnumpi.json2potential(JSON.stringify({
            type: 'FiniteWell',
            r_s: {
                value: 2.5e-10,
                unit: 'm'
            },
            V_s: {
                value: 0.1,
                unit: 'eV'
            }
        }));

        const lower_cc = 6;
        const upper_cc = 14;

        const reduced_mass = magnumpi.in_si(JSON.stringify({ value: 2.0013, unit: 'amu' }), magnumpi.dimensions.mass);
        const logE_low = Math.log10(magnumpi.in_si(JSON.stringify({ value: 1e-4, unit: 'eV' }), magnumpi.dimensions.energy));
        const logE_high = Math.log10(magnumpi.in_si(JSON.stringify({ value: 1e+5, unit: 'eV' }), magnumpi.dimensions.energy));

        const E_points_decade = 20;

        // max l for which the cross section is calculated
        const l_max = 2;

        let rmin = magnumpi.in_si(JSON.stringify({ value: 2e-10, unit: 'm' }), magnumpi.dimensions.length_);
        let rmax = magnumpi.in_si(JSON.stringify({ value: 1e-9, unit: 'm' }), magnumpi.dimensions.length_);
        const dr = magnumpi.in_si(JSON.stringify({ value: 1e-14, unit: 'm' }), magnumpi.dimensions.length_);

        // adjust rmin,rmax if necessary
        const Vmin = magnumpi.in_si(JSON.stringify({ value: 1e-3, unit: 'eV' }), magnumpi.dimensions.energy);
        const Vmax = magnumpi.in_si(JSON.stringify({ value: 20, unit: 'eV' }), magnumpi.dimensions.energy);
        rmin = magnumpi.rmin_input_evaluation(rmin, pot, Vmin);
        rmax = magnumpi.rmax_input_evaluation(rmax, pot, Vmax);

        // determine whether scat_Colonna or scat_Viehland is used for estimating scattering angles
        const use_scat_Colonna = 1;
        // set tolerance and interpolation order
        const interp = 3;
        const tolerance = 1e-3;

        const csc = new magnumpi.CrossSectionCalculator(
            lower_cc, upper_cc,
            logE_low,
            l_max, rmin, rmax, dr,
            use_scat_Colonna, interp,
            tolerance, pot
        );
        const cs_data = csc.integrate(
            logE_low, logE_high, E_points_decade
        );
        pot.delete();
        csc.delete();


        function ds2json(ds) {
            const row_axis = ds.row_axis();
            const col_axis = ds.col_axis();
            const nr = row_axis.size();
            const nc = col_axis.size();
            const rows = [];
            const dname = ds.name();
            const cname = col_axis.name();
            const rname = row_axis.name();
            for (var r = 0; r < nr; r++) {
                const rval = row_axis.value(r);
                for (var c = 0; c < nc; c++) {
                    const cval = col_axis.label_str(c);
                    const cell = {};
                    cell[rname] = rval;
                    cell[cname] = cval;
                    cell[dname] = ds.cell(r, c);
                    rows.push(cell);
                }
            }
            row_axis.delete();
            col_axis.delete();
            return rows;
        }

        // Check cs_data
        const cs_data2 = ds2json(cs_data);
        expect(cs_data2.length).toBeGreaterThan(100);
        const last_cs_data = cs_data2[cs_data2.length-1];
        expect(last_cs_data.epsilon).toBeCloseTo(4.515547240933656e-21);
        expect(last_cs_data.l).toEqual("2");
        expect(last_cs_data['sigma_l(eps)']).toBeCloseTo(4.515547240933656e-21);

        // Calculate collision integral
        const s_max = 5;
        const ci_lower_cc = 6;
        const ci_upper_cc = 14;
        const Tmin = magnumpi.in_si(JSON.stringify({ value: 100, unit: 'K' }), magnumpi.dimensions.temperature);
        const Tmax = magnumpi.in_si(JSON.stringify({ value: 50000, unit: 'K' }), magnumpi.dimensions.temperature);
        const Tdel = magnumpi.in_si(JSON.stringify({ value: 100, unit: 'K' }), magnumpi.dimensions.temperature);

        const q = new magnumpi.CrossSectionFromDatasetLog(cs_data);
        const ls_pairs = magnumpi.construct_ls_pairs(l_max, s_max);
        const oc = new magnumpi.OmegaCalculator(
            q,
            reduced_mass,
            ls_pairs,
            ci_lower_cc,
            ci_upper_cc,
            tolerance
        );
        const omega_data = oc.integrate(Tmin, Tmax, Tdel);
        cs_data.delete();
        oc.delete();

        // Check omega data
        const omega_data2 = ds2json(omega_data);
        omega_data.delete();
        expect(omega_data2.length).toBeGreaterThan(100);
        const first_omega_data = omega_data2[0];
        console.log(omega_data)
        console.log(omega_data2)
        expect(first_omega_data.T).toEqual(100);
        expect(first_omega_data['(l,s)']).toEqual("(1,1)");
        expect(first_omega_data['Omega_ls(T)']).toBeCloseTo(5.0476408092036636e-17);
    })
});
