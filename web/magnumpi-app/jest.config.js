export default {
    preset: 'vite-jest',

    setupFilesAfterEnv: ['<rootDir>/src/setupTests.js'],
    testMatch: [
      '<rootDir>/src/**/__tests__/**/*.{js,jsx,ts,tsx}',
      '<rootDir>/src/**/*.{spec,test}.{js,jsx,ts,tsx}',
    ],
    testEnvironment: 'jest-environment-jsdom',
    moduleNameMapper: {
        // "magnumpiwasm": "<rootDir>/src/__mocks__/calculator.worker.js"
        "calculator.worker.js": "<rootDir>/src/__mocks__/calculator.worker.js"
      }
  }