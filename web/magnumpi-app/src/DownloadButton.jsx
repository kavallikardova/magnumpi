import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import Button from "react-bootstrap/lib/Button";
import Glyphicon from "react-bootstrap/lib/Glyphicon";

import { createObjectURL } from "./writers";

export class DownloadButton extends PureComponent {
  constructor(props) {
    super(props);
    this.objectUrl = "";
  }

  componentWillUnmount() {
    if (this.objectUrl) {
      URL.revokeObjectURL(this.objectUrl);
    }
  }

  render() {
    if (this.objectUrl) {
      // free old objectUrl
      URL.revokeObjectURL(this.objectUrl);
    }
    this.objectUrl = createObjectURL(this.props.blob, this.props.mime_type);
    return (
      <Button href={this.objectUrl} download={this.props.filename}>
        <Glyphicon glyph="download" /> Download
      </Button>
    );
  }
}
DownloadButton.propTypes = {
  mime_type: PropTypes.string.isRequired,
  filename: PropTypes.string.isRequired,
  blob: PropTypes.any.isRequired,
};
