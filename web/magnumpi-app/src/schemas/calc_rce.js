import definitions from "./definitions.json";
import LXCatJSONSchema from "./interaction-potential.json";

export const schema = {
  $schema: "http://json-schema.org/draft-07/schema#",
  $id: "http://magnumpi.org/schemas/calc_rce.json",
  definitions: {
    ...definitions,
    ...LXCatJSONSchema.definitions,
  },
  properties: {
    geradePotential: {
      title: "Gerade potential",
      $ref: "#/definitions/Potential",
    },
    ungeradePotential: {
      title: "Ungerade potential",
      $ref: "#/definitions/Potential",
    },
    reducedMass: {
      $ref: "#/definitions/ReducedMass",
    },
    clenshawCurtisQuadrature: {
      $ref: "#/definitions/ClenshawCurtisQuadrature",
    },
    energyRange: {
      $ref: "#/definitions/EnergyRange",
    },
    l_max: {
      $ref: "#/definitions/LMax",
    },
    r: {
      $ref: "#/definitions/R",
    },
    tolerance: {
      $ref: "#/definitions/Tolerance",
    },
    ci: {
      $ref: "#/definitions/CollisionIntegral",
    },
  },
  required: [
    "geradePotential",
    "ungeradePotential",
    "reducedMass",
    "clenshawCurtisQuadrature",
    "energyRange",
    "l_max",
    "r",
    "tolerance",
    "ci",
  ],
  additionalProperties: false,
  type: "object",
};

schema.definitions.LXCatJSONInline.properties.record = LXCatJSONSchema;
