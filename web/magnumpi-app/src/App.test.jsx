/* eslint-env node, jest */
import { render } from "@testing-library/react";
import React from "react";

import App from "./App";

it("renders without crashing", () => {
  const app = render(<App />);
  expect(app.getByText("Magnum PI")).toBeTruthy();
});
