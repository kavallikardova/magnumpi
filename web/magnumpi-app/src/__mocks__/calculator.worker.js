export default class Worker {
  constructor() {
    this.messages = [];
  }

  postMessage(message) {
    this.messages.push(message);
  }
}
