/**
 * Create ObjectURL from a long string
 *
 * The returned url must be freed with URL.revokeObjectURL when no longer needed
 *
 */
export function createObjectURL(blobParts, mime_type) {
  const blob = new Blob([blobParts], { type: mime_type });
  const url = URL.createObjectURL(blob);
  return url;
}

export function rows2tsv(data, cols) {
  const header = cols.join("\t") + "\n";
  const rows = data
    .map((row) => {
      return cols.map((col) => row[col]).join("\t");
    })
    .join("\n");
  return header + rows;
}

export function cs2tsv(data) {
  const cols = ["epsilon", "sigma_l(eps)", "l"];
  return rows2tsv(data, cols);
}

export function dcs2tsv(data) {
  const cols = ["chi", "dcs", "energy"];
  return rows2tsv(data, cols);
}

export function tcs2tsv(data) {
  const cols = ["chi", "dcs", "energy"];
  return rows2tsv(data, cols);
}

export function sa2tsv(data) {
  const cols = ["b", "E", "chi(b,E)"];
  return rows2tsv(data, cols);
}

export function omega2tsv(data) {
  const cols = ["T", "Omega_ls(T)", "(l,s)"];
  return rows2tsv(data, cols);
}
